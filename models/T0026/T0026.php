<?php

/**************************************************************************/
/* Criado em: 10/11/2012 por USER                              */
/* Descrição: Classe para executar as Querys do Programa T0012            */

/**************************************************************************/

class models_T0026 extends models
{

    public function __construct()
    {
        $conn = "";
        parent::__construct($conn);
    }

    public function inserir($tabela, $campos)
    {
        $insere = $this->exec($this->insere($tabela, $campos));

        //Mensagem
        if ($insere) {
            $this->mostraMensagem("", "Gravado com Sucesso!");
        } else {
            $this->mostraMensagem("e", "Não foi possível gravar!");
        }

        return $insere;
    }

    public function atualizar($tabela, $campos, $delim)
    {
        $conn = "";

        $altera = $this->exec($this->atualiza($tabela, $campos, $delim));

        //Mensagem
        if ($altera) {
            $this->mostraMensagem("", "Alterado com Sucesso!");
        } else {
            $this->mostraMensagem("e", "Não foi possível alterar!");
        }

        return $altera;
    }

    public function excluir($tabela, $delim)
    {
        $exclui = $this->exec($this->exclui($tabela, $delim));

        return $exclui;
    }

    public function retornaDados(
        $codigoManutencao,
        $codigoCliente,
        $dataEntInicio,
        $dataEntFim,
        $dataRetInicio,
        $dataRetFim,
        $statusManutencao,
        $limit
    ) {
        $dataEntInicio = $this->formataData($dataEntInicio);
        $dataEntFim = $this->formataData($dataEntFim);

        $dataRetInicio = $this->formataData($dataRetInicio);
        $dataRetFim = $this->formataData($dataRetFim);

        $sql = "  SELECT T16.T016_codigo          CodigoManutencao
                                , T16.T002_codigo          CodigoCliente
                                , T02.T002_nome            NomeCliente
                                , T16.T016_dt_entrada      DataEntManutencao
                                , T16.T016_dt_envio        DataEnvioManutencao
                                , T16.T016_dt_recebimento  DataRecManutencao
                                , T16.T016_dt_devolucao    DataDevManutencao
                                , T16.T016_dt_cancelamento DataCancManutencao
                                , T16.T016_historico       HistoricoManutencao
                                , CASE T16.T016_status
                                   WHEN 1 THEN '001-AGUARDANDO ENVIO'
                                   WHEN 2 THEN '002-AGUARDANDO MANUTENÇÃO'
                                   WHEN 3 THEN '003-AGUARDANDO CLIENTE'
                                   WHEN 4 THEN '004-ENTREGUE CLIENTE'
                                   WHEN 5 THEN '005-CANCELADO'
                                  END StatusManutencao
                             FROM t016_manutencao T16
                             JOIN t002_cliente T02 ON T02.T002_codigo = T16.T002_codigo";

        if (!empty($codigoManutencao)) {
            $sql .= " AND T16.T016_codigo= $codigoManutencao";
        }
        if (!empty($codigoCliente)) {
            $sql .= " AND T16.T002_codigo= $codigoCliente";
        }
        if (($dataEntInicio)) {
            $sql .= " AND T16.T016_dt_entrada  >= '$dataEntInicio 00:00:00'";
        }
        if (!empty($dataEntFim)) {
            $sql .= " AND T16.T016_dt_entrada <= '$dataEntFim 23:59:59'";
        }

        if (!empty($dataRetInicio)) {
            $sql .= " AND T16.T016_dt_devolucao  >= '$dataRetInicio 00:00:00'";
        }
        if (!empty($dataRetFim)) {
            $sql .= " AND T16.T016_dt_devolucao <= '$dataRetFim 23:59:59'";
        }

        if (!empty($statusManutencao)) {
            $sql .= " AND T16.T016_status= $statusManutencao";
        }

        $sql .= " ORDER BY 5,1";

        if (!isset($limit)) {
            $sql .= " LIMIT " . $this->limiteLinhas();
        } else {
            if (is_numeric($limit)) {
                $sql .= " LIMIT " . $limit;
            }
        }

        return $this->query($sql);
    }

    public function retornaManutencaoDetalhe($codigoManutencao)
    {
        $sql = "  SELECT T17.T016_codigo      CodigoManutencao
                                , T17.T003_codigo      CodigoProduto
                                , T03.T003_descricao   DescricaoProduto
                                , T17.T017_qtde        QtdeProduto
                                , T17.T017_obs         ObservacaoProduto
                             FROM t017_manutencao_detalhe T17
                             JOIN t003_produto T03 ON T17.T003_codigo = T03.T003_codigo
                            WHERE T17.T016_codigo   =   $codigoManutencao";

        return $this->query($sql);
    }

    public function retornaDadosCliente($codigoCliente)
    {
        $sql = "  SELECT T02.T002_codigo   CodigoCliente
                                , T02.T002_nome     NomeCliente
                             FROM t002_cliente T02
                            WHERE T02.T002_codigo  = $codigoCliente";

        return $this->query($sql);
    }

    public function retornaDadosManutencaoImpressao($codigoManutencao)
    {
        $sql = "  SELECT T17.T016_codigo      CodigoManutencao
                                , T17.T003_codigo      CodigoProduto
                                , T03.T003_descricao   DescricaoProduto
                                , T02.T002_codigo      CodigoCliente
                                , T02.T002_nome        NomeCliente
                             FROM t017_manutencao_detalhe T17
                             JOIN t016_manutencao T16  ON T16.T016_codigo = T17.T016_codigo
                             JOIN t002_cliente T02     ON T02.T002_codigo = T16.T002_codigo
                             JOIN t003_produto T03     ON T17.T003_codigo = T03.T003_codigo
                            WHERE 1 =    1";

        $sql .= "     AND T17.T016_codigo   =   $codigoManutencao";

        return $this->query($sql);
    }

}

?>