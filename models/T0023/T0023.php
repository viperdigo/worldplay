<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 10/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0012            */    
    /**************************************************************************/    

    class models_T0023 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }         
        
        public function retornaDados($codigoDevolucao,$codigoCliente,$dataInicio, $dataFim, $statusDevolucao, $limit)
        {
            $dataInicio =   $this->formataData($dataInicio);                    
            $dataFim    =   $this->formataData($dataFim);                    
            
            $sql    =   "  SELECT T15.T015_codigo      CodigoDevolucao
                                , T15.T002_codigo      CodigoCliente
                                , T02.T002_nome        NomeCliente
                                , T15.T015_data        DataDevolucao
                                , CASE T15.T015_status
                                  WHEN 1 THEN '001-PENDENTE'
                                  WHEN 2 THEN '002-APROVADO'
                                  WHEN 3 THEN '003-CANCELADO'
                                  END StatusDevolucao
                             FROM t015_devolucao T15
                             JOIN t002_cliente T02 ON T02.T002_codigo = T15.T002_codigo
                            WHERE 1 = 1";
            
            if(!empty($codigoDevolucao))
                $sql    .=  " AND T15.T015_codigo = $codigoDevolucao";
            if(!empty($codigoCliente))
                $sql    .=  " AND T15.T002_codigo = $codigoCliente";
            if(!empty($dataInicio))
                $sql    .=  " AND T15.T015_data >= '$dataInicio'";
            if(!empty($dataFim))
                $sql    .=  " AND T15.T015_data <= '$dataFim'";
            if(!empty($statusTroca))
                $sql    .=  " AND T15.T015_status = $statusTroca";
            
            $sql    .=  " ORDER BY 4 DESC";
            
            if (empty($limit))
                $sql    .=  " LIMIT ".$this->limiteLinhas();
            else if (is_numeric($limit))
                $sql    .=  " LIMIT ".$limit;                        
            
            return $this->query($sql);
        }
        
        public function verificaContasProxVencimento()
        {
            $sql    =   " SELECT T07.T007_valor
                            FROM t007_parametro T07
                           WHERE T007_codigo = 2";
            
            $parametroDias  =   (int)$this->query($sql)->fetchColumn();
            
            $dataParametro  =   date('Y-m-d', strtotime("+$parametroDias days"));
            
            $sql    =   " SELECT *
                            FROM t008_contas_pagar T08
                           WHERE T008_data_venc <= '$dataParametro'";
            
            return $this->query($sql)->rowCount();
        }


    }                                                                               
?>