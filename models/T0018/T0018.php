<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 23/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0018            */    
    /**************************************************************************/    

    class models_T0018 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }  
        
        public function retornaDados($usuario)
        {
            $sql    =   "  SELECT T01.t001_login    LoginUsuario
                                , T01.T001_nome     NomeUsuario
                             FROM t001_usuario T01
                            WHERE 1 = 1";
            if (!empty($usuario))
                $sql    .=  " AND T01.t001_login = '$usuario'";
            
            return $this->query($sql);
            
        }
        
        public function retornaDadosUsuario($user)
        {
            $sql    =   "  SELECT T01.t001_login        LoginUsuario
                                , T01.T001_nome         NomeUsuario
                             FROM t001_usuario T01
                            WHERE T01.t001_login   =   '$user'";
            
            return $this->query($sql);
            
        }
        
        public function retornaAssociados($user)
        {
            $sql    =   "  SELECT T0001.t001_login      LoginUsuario
                                , T01.T001_nome         NomeUsuario
                                , T0001.t000_codigo     CodigoEstrutura
                                , T00.t000_nome         NomeEstrutura
                             FROM t000_t001 T0001
                             JOIN t000_estrutura T00 ON T00.t000_codigo = T0001.t000_codigo
                             JOIN t001_usuario T01 ON T01.t001_login = T0001.t001_login
                            WHERE T0001.t001_login   =   '$user'";
            
            return $this->query($sql);
            
        }


    }                                                                               
?>