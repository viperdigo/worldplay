<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 10/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0012            */    
    /**************************************************************************/    

    class models_T0012 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }         
        
        public function retornaDados($user, $codigoContaP, $codigoFornecedor, $dataInicio, $dataFim, $status, $limit )
        {
            $dataInicio =   $this->formataData($dataInicio);                    
            $dataFim    =   $this->formataData($dataFim);                    
            
            $sql    =   "  SELECT T08.T008_codigo       CodigoConta
                                , T08.T004_codigo       CodigoFornecedor
                                , T04.T004_razao_social NomeFornecedor
                                , T08.T008_descricao    DescricaoConta
                                , T08.T008_data_venc    DataVencConta
                                , T08.T008_data_pagto   DataPgtoConta
                                , T08.T008_valor        ValorConta
                                , CASE T08.T008_status
                                  WHEN 1 THEN '001-PENDENTE'
                                  WHEN 2 THEN '002-PAGO'
                                  WHEN 3 THEN '003-CANCELADO'
                                  END StatusConta
                             FROM t008_contas_pagar T08
                             JOIN t004_fornecedor  T04 ON T04.T004_codigo = T08.T004_codigo
                             JOIN t001_t004 T001004 ON T001004.T004_codigo = T04.T004_codigo
                            WHERE 1 = 1
                              AND T001004.t001_login = '$user'";
            
            if(!empty($status))
                $sql    .=  " AND T08.T008_status = $status";
            if(!empty($codigoFornecedor))
                $sql    .=  " AND T08.T004_codigo = $codigoFornecedor";
            if(!empty($codigoContaP))
                $sql    .=  " AND T08.T008_codigo = $codigoContaP";
            if(!empty($dataInicio))
                $sql    .=  " AND T08.T008_data_venc >= '$dataInicio'";
            if(!empty($dataFim))
                $sql    .=  " AND T08.T008_data_venc <= '$dataFim'";
            
            $sql    .=  " ORDER BY 5,1 DESC";   
            
            if (empty($limit))
                $sql    .=  " LIMIT ".$this->limiteLinhas();
            else if (is_numeric($limit))
                $sql    .=  " LIMIT ".$limit;  
            
            return $this->query($sql);
        }
        
        public function verificaContasProxVencimento()
        {
            $sql    =   " SELECT T07.T007_valor
                            FROM t007_parametro T07
                           WHERE T007_codigo = 2";
            
            $parametroDias  =   (int)$this->query($sql)->fetchColumn();
            
            $dataParametro  =   date('Y-m-d', strtotime("+$parametroDias days"));
            
            $sql    =   " SELECT *
                            FROM t008_contas_pagar T08
                           WHERE T008_data_venc <= '$dataParametro'
                             AND T008_status    =   1";
            
            return $this->query($sql)->rowCount();
        }


    }                                                                               
?>