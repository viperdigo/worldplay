<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 27/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0019            */    
    /**************************************************************************/    

    class models_T0020 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }  
        
        public function retornaDados($codigoProduto)
        {
            $sql    =   "  SELECT T03.T003_codigo           CodigoProduto
                                , T03.T003_descricao        DescricaoProduto
                                , T03.T003_qtd_estoque      EstoqueAtual
                                , FNWPINT_RetornaTrocasProduto(T03.T003_codigo)         Troca
                                , FNWPINT_RetornaDevolucoesProduto(T03.T003_codigo)     Devolucao
                             FROM t003_produto T03
                            WHERE 1 = 1";
            
            if (!empty($codigoProduto))
                $sql    .=  " AND T03.T003_codigo       = $codigoProduto";

            $sql    .=  " GROUP BY T03.T003_codigo, T03.T003_descricao";
            
            return $this->query($sql);
        }


    }                                                                               
?>