<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 10/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0010            */    
    /**************************************************************************/    

    class models_T0031 extends models
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }  
        
        public function retornaDados($codigoPedido, $codigoCliente, $dataInicio, $dataFim, $limit)
        {
            $dataInicio =   $this->formataData($dataInicio);
            $dataFim    =   $this->formataData($dataFim);
                        
            $sql    =   "  SELECT T20.T020_pedido  CodigoPedido
                                , T20.T002_codigo  CodigoCliente
                                , T02.T002_nome    NomeCliente
                                , T20.T020_data    DataPedido
                                , T20.T020_valor   ValorPedido
                             FROM t020_pedido_fornecedor T20
                             JOIN t002_cliente T02 ON T02.T002_codigo = T20.T002_codigo
                            WHERE 1  = 1";
            
            if (!empty($codigoPedido))
                $sql    .=  " AND T20.T020_pedido   =   $codigoPedido";
            if (!empty($codigoCliente))
                $sql    .=  " AND T20.T002_codigo   =   $codigoCliente";
            if (!empty($dataInicio))
                $sql    .=  " AND T20.T020_data   >=   '$dataInicio'";
            if (!empty($dataFim))
                $sql    .=  " AND T20.T020_data   <=   '$dataFim'";
            
            $sql    .=  " ORDER BY 4 DESC";
            
            if (empty($limit))
                $sql    .=  " LIMIT ".$this->limiteLinhas();
            else if (is_numeric($limit))
                $sql    .=  " LIMIT ".$limit;  
            
            return $this->query($sql);
        }
        
        public function retornaProdutosPedido($codigoPedido)
        {
            $sql    =   "  SELECT T21.T020_pedido        CodigoPedido
                                , T21.T002_codigo        CodigoCliente
                                , T02.T002_nome          NomeCliente
                                , T21.T003_codigo        CodigoProduto
                                , T03.T003_descricao     DescricaoProduto
                                , T21.T021_valor_venda   VlVendaProduto     
                                , T21.T021_qtde          QtdeProduto
                                , T21.T021_subtotal      SubtotalProduto
                             FROM t021_pedido_fornecedor_detalhe T21
                             JOIN t003_produto T03 ON T03.T003_codigo = T21.T003_codigo
                             JOIN t002_cliente T02 ON T21.T002_codigo = T02.T002_codigo
                            WHERE T21.T020_pedido  = $codigoPedido";
            
            return $this->query($sql);
        }
        
        public function recalculaSubtotalPedido($codigoPedido)
        {
            $sql    =   " SELECT sum(T21.T021_subtotal)
                            FROM t021_pedido_fornecedor_detalhe T21
                           WHERE T21.T020_pedido    =   $codigoPedido";
            
            $total  =   $this->query($sql)->fetchColumn();    
            $total  =   $this->formataMoeda($total);
            $total  =   str_replace("R$ ", "", $total);
            
            $tabela =   "t020_pedido_fornecedor";
            
            $campo  =   array("T020_valor" => $total);
            $delim  =   " T020_pedido    =   $codigoPedido";
            $altera =   $this->atualizar($tabela, $campo, $delim);
            
            return $total;
                                           
        }
        
        public function retornaTipoProduto($codigoProduto)
        {
            $sql    =   " SELECT T03.T003_tipo      TipoProduto
                            FROM t003_produto T03
                           WHERE T03.T003_codigo = $codigoProduto";
            
            $tipo   =   $this->query($sql)->fetchAll(PDO::FETCH_COLUMN);
            
            return $tipo[0];
        }
        
        public function retornaComponentes($codigoProdutoReceita)
        {
            $sql    =   "  SELECT T13.T003_codigo          CodigoReceita
                                , T13.T003_componente      CodigoComponente
                                , T13.T013_qtd_componente  QtdeComponente
                             FROM t013_componente T13
                            WHERE T13.T003_codigo  = $codigoProdutoReceita";
            
            return $this->query($sql);
        }
        
        public function retornaDadosPedidoImpressao($codigoPedido)
        {
            $sql    =   "  SELECT T20.T020_pedido          CodigoPedido
                                , T20.T020_data            DataPedido
                                , T20.T002_codigo          CodigoCliente
                                , T02.T002_nome            NomeCliente
                                , T21.T003_codigo          CodigoProduto
                                , T03.T003_descricao       DescricaoProduto
                                , T21.T021_qtde            QtdeProduto
                                , T21.T021_valor_venda     VlVendaProduto
                                , T21.T021_subtotal        SubtotalProduto
                             FROM t021_pedido_fornecedor_detalhe T21
                             JOIN t020_pedido_fornecedor T20 ON T21.T020_pedido = T20.T020_pedido
                             JOIN t002_cliente T02 ON T02.T002_codigo = T20.T002_codigo
                             JOIN t003_produto T03 ON T03.T003_codigo = T21.T003_codigo
                            WHERE T20.T020_pedido = $codigoPedido";
            
            return $this->query($sql);
        }
        
        public function retornaDadosProduto($codigoProduto)
        {
            $sql    =   "  SELECT T03.T003_codigo      CodigoProduto
                                , T03.T003_descricao   DescricaoProduto
                             FROM t003_produto T03
                            WHERE T03.T003_codigo  = $codigoProduto";
            
            return $this->query($sql);
            
        }
        
        public function retornaDadosCliente($codigoCliente)
        {
            $sql    =   "  SELECT T02.T002_codigo   CodigoCliente
                                , T02.T002_nome     NomeCliente
                             FROM t002_cliente T02
                            WHERE T02.T002_codigo  = $codigoCliente";
            
            return $this->query($sql);            
        }
     
    }                                                                               
?>