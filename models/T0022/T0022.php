<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 10/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0012            */    
    /**************************************************************************/    

    class models_T0022 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }         
        
        public function retornaDados($codigoTroca,$codigoCliente,$dataInicio, $dataFim, $statusTroca, $limit)
        {
            $dataInicio =   $this->formataData($dataInicio);                    
            $dataFim    =   $this->formataData($dataFim);                    
            
            $sql    =   "  SELECT T14.T014_codigo    CodigoTroca
                                , T14.T002_codigo    CodigoCliente
                                , T02.T002_nome      NomeCliente
                                , T14.T014_data      DataTroca
                                , CASE T14.T014_status
                                  WHEN 0 THEN '000-PENDENTE'
                                  WHEN 1 THEN '001-GARANTIA'
                                  WHEN 2 THEN '002-TROCA'
                                  WHEN 3 THEN '003-CANCELADO'
                                  END StatusTroca
                             FROM t014_troca T14
                             JOIN t002_cliente T02 ON T02.T002_codigo = T14.T002_codigo
                            WHERE 1 = 1";

            if(!empty($codigoTroca))
                $sql    .=  " AND T14.T014_codigo = $codigoTroca";
            if(!empty($codigoCliente))
                $sql    .=  " AND T14.T002_codigo = $codigoCliente";
            if(!empty($dataInicio))
                $sql    .=  " AND T14.T014_data >= '$dataInicio'";
            if(!empty($dataFim))
                $sql    .=  " AND T14.T014_data <= '$dataFim'";
            if(!empty($statusTroca))
                $sql    .=  " AND T14.T014_status = $statusTroca";
            
            $sql    .=  " ORDER BY 4 DESC";
            
            if (empty($limit))
                $sql    .=  " LIMIT ".$this->limiteLinhas();
            else if (is_numeric($limit))
                $sql    .=  " LIMIT ".$limit;              
            
            return $this->query($sql);
        }
        
        public function verificaContasProxVencimento()
        {
            $sql    =   " SELECT T07.T007_valor
                            FROM t007_parametro T07
                           WHERE T007_codigo = 2";
            
            $parametroDias  =   (int)$this->query($sql)->fetchColumn();
            
            $dataParametro  =   date('Y-m-d', strtotime("+$parametroDias days"));
            
            $sql    =   " SELECT *
                            FROM t008_contas_pagar T08
                           WHERE T008_data_venc <= '$dataParametro'";
            
            return $this->query($sql)->rowCount();
        }


    }                                                                               
?>