<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 27/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0019            */    
    /**************************************************************************/    

    class models_T0019 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }  
        
        public function retornaDados()
        {
            $sql    =   " SELECT DISTINCT T02.T002_codigo                                       CodigoCliente
                                        , T02.T002_nome                                         NomeCliente
                                        , T02.T002_saldo                                        SaldoCliente
                                        , FNWPINT_RetornaUltimoPagtoCliente(T02.T002_codigo)    UltimoPagamento
                                     FROM t002_cliente T02
                                LEFT JOIN t012_contas_receber T12 ON T02.T002_codigo = T12.T002_codigo
                                    WHERE T02.T002_status = 1";

            return $this->query($sql);
        }


    }                                                                               
?>