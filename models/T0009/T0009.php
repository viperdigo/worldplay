<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 10/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0009            */    
    /**************************************************************************/    

    class models_T0009 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }  
        
        public function retornaDados($codigoProduto = null, $descricaoProduto = null, $tipo = null , $limit =   null)
        {
            $sql    =   "  SELECT T03.T003_codigo        CodigoProduto
                                , T03.T004_codigo        CodigoFornecedor
                                , T04.T004_razao_social  RazaoFornecedor
                                , T03.T003_descricao     DescricaoProduto
                                , T03.T003_vl_unit       VlVendaProduto
                                , T03.T003_vl_custo      VlCustoProduto
                                , T03.T003_qtd_estoque   EstoqueProduto
                                , CASE T03.T003_tipo
                                  WHEN 1 THEN 'NORMAL'
                                  WHEN 2 THEN 'RECEITA'
                                  WHEN 3 THEN 'GABINETE'
                                  END                    TipoProduto
                             FROM t003_produto T03
                        LEFT JOIN t004_fornecedor T04 ON T04.T004_codigo = T03.T004_codigo
                            WHERE 1  = 1";
            
            if ($tipo == 3)
                $sql    .=  " AND T03.T003_tipo         =   3";
            if ($tipo == 2)
                $sql    .=  " AND T03.T003_tipo         =   2";
            if ($tipo == 1)
                $sql    .=  " AND T03.T003_tipo         =   1"; 
            if (!empty($codigoProduto))
                $sql    .=  " AND T03.T003_codigo       =   $codigoProduto";
            if (!empty($descricaoProduto))
                $sql    .=  " AND T03.T003_descricao LIKE '%$descricaoProduto%'";                        
            
            if (empty($limit))
                $sql    .=  " LIMIT ".$this->limiteLinhas();
            else if (is_numeric($limit))
                $sql    .=  " LIMIT ".$limit;
            
//            echo $sql;
            
            return $this->query($sql);
            
        }
        
        public function retornaComponentes($codigoProduto)
        {
            $sql    =   "      SELECT T13.T003_componente      CodigoComponente
                                    , T03.T003_descricao       DescricaoProduto
                                    , T13.T013_qtd_componente  QtdeComponente
                                 FROM t013_componente T13
                                 JOIN t003_produto T03 ON T03.T003_codigo = T13.T003_componente
                                WHERE T13.T003_codigo = $codigoProduto";

            return $this->query($sql);
        }
        
        public function verifiqueDescricao($descricaoProduto)
        {
            $sql    =   " SELECT T03.T003_descricao DescricaoProduto
                            FROM t003_produto T03
                           WHERE T03.T003_descricao LIKE '$descricaoProduto'";
            
            return $this->query($sql);
        }


    }                                                                               
?>