<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 27/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0019            */    
    /**************************************************************************/    

    class models_T0016 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }
        
        public function retornaCliente($codigoCliente)
        {
            $sql    =   "  SELECT T02.T002_codigo    CodigoCliente
                                , T02.T002_nome      NomeCliente
                             FROM t002_cliente T02
                            WHERE T02.T002_codigo = $codigoCliente";
            
            return $this->query($sql);            
        }
        
        public function retornaDados($codigoCliente, $dataInicio, $dataFim)
        {
            $dataInicio =   $this->formataData($dataInicio);
            $dataFim    =   $this->formataData($dataFim);            
            
            $sql    =   "      SELECT S1.CodigoPedido                   CodigoPedido
                                    , S1.Data                           Data
                                    , S1.Valor                          Valor
                                    , S1.Tipo                           Tipo
                                    , S1.CodigoCliente                  CodigoCliente
                                    , S1.NomeCliente                    NomeCliente
                                    , S1.ObsCR                          ObsCR
                                 FROM (
                                       SELECT T10.T010_pedido    CodigoPedido
                                            , DATE_FORMAT(T10.T010_data,'%Y-%m-%d')      Data
                                            , T10.T010_valor     Valor
                                            , '2'                Tipo  /* Pedido */
                                            , T02.T002_codigo    CodigoCliente
                                            , T02.T002_nome      NomeCliente
                                            , ''                 ObsCR
                                            , T02.T002_status StatusCliente
                                         FROM t010_pedido T10    
                                         JOIN t002_cliente T02 
                                           ON T02.T002_codigo = T10.T002_codigo
                                UNION
                                       SELECT T12.T012_codigo CodigoPedido
                                            , DATE_FORMAT(T12.T012_data,'%Y-%m-%d')  Data
                                            , T012_valor     Valor
                                            , '1'            Tipo    /* Pagto  */
                                            , T02.T002_codigo    CodigoCliente
                                            , T02.T002_nome      NomeCliente 
                                            , T12.T012_observacao ObsCR
                                            , T02.T002_status StatusCliente
                                         FROM t012_contas_receber T12
                                         JOIN t002_cliente T02 
                                           ON T02.T002_codigo = T12.T002_codigo    
                                UNION
                                       SELECT T20.T020_pedido    CodigoPedido
                                            , DATE_FORMAT(T20.T020_data,'%Y-%m-%d')      Data
                                            , T20.T020_valor     Valor
                                            , '3'                Tipo  /* Pedido Forn. */
                                            , T02.T002_codigo    CodigoCliente
                                            , T02.T002_nome      NomeCliente
                                            , ''                 ObsCR
                                            , T02.T002_status StatusCliente
                                         FROM t020_pedido_fornecedor T20    
                                         JOIN t002_cliente T02 
                                           ON T02.T002_codigo = T20.T002_codigo                   
                                       ) S1
                                WHERE S1.StatusCliente = 1";
            
            if (!empty($codigoCliente))
                $sql    .=  " AND S1.CodigoCliente =    $codigoCliente";
            if (!empty($dataInicio))
                $sql    .=  " AND S1.Data         >=    '$dataInicio 00:00:00'";
            if (!empty($dataFim))
                $sql    .=  " AND S1.Data         <=    '$dataFim 23:59:59'";

           $sql .=  " ORDER BY 2 ASC, 4 DESC";
           
//           echo $sql;
           
            return $this->query($sql);
        }
        
        public function retornaDadosContaCliente($codigoCliente, $dataInicio, $dataFim)
        {
            $dataInicio =   $this->formataData($dataInicio);
            $dataFim    =   $this->formataData($dataFim); 
            
            $sql    =   "  SELECT T02.T002_codigo          CodigoCliente
                                , T02.T002_saldo           SaldoCliente
                                , SUM(T12.T012_valor)      RecebidoCliente
                             FROM t012_contas_receber T12 
                             JOIN t002_cliente T02 ON T02.T002_codigo = T12.T002_codigo
                            WHERE T02.T002_codigo = $codigoCliente";
            
//            if(!empty($dataInicio))
//                $sql .= "     AND T12.T012_data >= '$dataInicio'";
//            if(!empty($dataFim))
//                $sql .= "     AND T12.T012_data <= '$dataFim'";
//                              
//
//            $sql .= "    GROUP BY T02.T002_codigo
//                                , T02.T002_saldo";
//            echo $sql;
            return $this->query($sql);
            
        }
        
        public function detalhePedido($codigoPedido)
        {
            $sql    =   "  SELECT T11.T010_pedido        CodigoPedido
                                , T11.T002_codigo        CodigoCliente
                                , T02.T002_nome          NomeCliente     
                                , T11.T003_codigo        CodigoProduto
                                , T03.T003_descricao     DescricaoProduto
                                , T11.T011_valor_venda   ValorProduto
                                , T11.T011_qtde          QtdeProduto
                                , T11.T011_subtotal      SubtotalProduto
                             FROM t011_pedido_detalhe T11
                             JOIN t003_produto T03 ON T03.T003_codigo = T11.T003_codigo
                             JOIN t002_cliente T02 ON T11.T002_codigo = T02.T002_codigo
                            WHERE T11.T010_pedido   =   $codigoPedido";

            return $this->query($sql);
        }

		/**
		 * @param $codigoPedido
		 * @return PDOStatement
		 */
		public function detalhePedidoFornecedor($codigoPedido)
		{
			$sql    =   "  SELECT T21.T020_pedido        CodigoPedido
                                , T21.T002_codigo        CodigoCliente
                                , T02.T002_nome          NomeCliente     
                                , T21.T003_codigo        CodigoProduto
                                , T03.T003_descricao     DescricaoProduto
                                , T21.T021_valor_venda   ValorProduto
                                , T21.T021_qtde          QtdeProduto
                                , T21.T021_subtotal      SubtotalProduto
                             FROM t021_pedido_fornecedor_detalhe T21
                             JOIN t003_produto T03 ON T03.T003_codigo = T21.T003_codigo
                             JOIN t002_cliente T02 ON T21.T002_codigo = T02.T002_codigo
                            WHERE T21.T020_pedido   =   $codigoPedido";

			return $this->query($sql);
		}


    }                                                                               
?>