<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 15/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0015            */    
    /**************************************************************************/    

    class models_T0015 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }                                                                           
        
        public function retornaDados($codigoParametro, $nomeParametro)
        {
            $sql    =   "  SELECT T07.T007_codigo      CodigoParametro
                                , T07.T007_nome        NomeParametro
                                , T07.T007_descricao   DescricaoParametro
                                , T07.T007_valor       ValorParametro
                             FROM t007_parametro T07
                            WHERE 1 = 1";
            if(!empty($codigoParametro))
                $sql    .=  " AND T07.T007_codigo   =   $codigoParametro";
            if(!empty($nomeParametro))
                $sql    .=  " AND T07.T007_nome     =   $nomeParametro";
            
            if (empty($limit))
                $sql    .=  " LIMIT ".$this->limiteLinhas();
            else if (is_numeric($limit))
                $sql    .=  " LIMIT ".$limit;  
            
            return $this->query($sql);
        }


    }                                                                               
?>