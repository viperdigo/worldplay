<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 10/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0010            */    
    /**************************************************************************/    

    class models_T0010 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }  

        public function retornaDados($codigoFornecedor, $nomeFornecedor, $limit)
        {
            $sql    =   "  SELECT T04.T004_codigo        CodigoFornecedor
                                , T04.T004_razao_social  RazaoFornecedor
                                , T04.T004_cnpj          CnpjFornecedor  
                                , T04.T004_fone          FoneFornecedor
                             FROM t004_fornecedor T04
                            WHERE 1 =   1";
            
            if (!empty($codigoFornecedor))
                $sql    .=  " AND T04.T004_codigo =   $codigoFornecedor";
            if (!empty($nomeFornecedor))
                $sql    .=  " AND T04.T004_razao_social   LIKE '%$nomeFornecedor%'";
            
            if (empty($limit))
                $sql    .=  " LIMIT ".$this->limiteLinhas();
            else if (is_numeric($limit))
                $sql    .=  " LIMIT ".$limit;  

                            
            return $this->query($sql);
        }    
        
        public function retornaDadosFornecedor($codigoFornecedor)
        {
            $sql    =   "  SELECT T04.T004_codigo           CodigoFornecedor
                                , T04.T004_razao_social     RazaoFornecedor
                             FROM t004_fornecedor T04
                            WHERE T04.T004_codigo = $codigoFornecedor";
            
            if (!isset($limit))
                $sql    .=  " LIMIT ".$this->limiteLinhas();
            else if (is_numeric($limit))
                $sql    .=  " LIMIT ".$limit;            
            
            return $this->query($sql);
            
        }
        
        public function retornaAssociados($codigoFornecedor)
        {
            $sql    =   "  SELECT T0104.t001_login       LoginUsuario
                                , T01.T001_nome          NomeUsuario
                                , T0104.T004_codigo      CodigoFornecedor
                                , T04.T004_razao_social  RazaoFornecedor
                             FROM t001_t004 T0104
                             JOIN t001_usuario T01 ON T01.t001_login = T0104.t001_login
                             JOIN t004_fornecedor T04 ON T04.T004_codigo = T0104.T004_codigo
                            WHERE T0104.T004_codigo  =   $codigoFornecedor";
            
            return $this->query($sql);
        }


    }                                                                               
?>