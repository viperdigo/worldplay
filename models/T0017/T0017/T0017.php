<?php                                                                               

    /**************************************************************************/    
    /* Criado em: 21/11/2012 por USER                              */    
    /* Descrição: Classe para executar as Querys do Programa T0017            */    
    /**************************************************************************/    

    class models_T0017 extends models                                               
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }   
        
        public function retornaDados($codigoConta, $codigoCliente, $dataInicio, $dataFim, $limit)
        {
            $dataInicio =   $this->formataData($dataInicio);
            $dataFim    =   $this->formataData($dataFim);            
            
            $sql    =   "  SELECT T12.T012_codigo      CodigoConta
                                , T12.T002_codigo      CodigoCliente
                                , T02.T002_nome        NomeCliente
                                , T12.t001_login       LoginUsuario
                                , T01.T001_nome        NomeUsuario
                                , T12.T012_data        DataConta
                                , T12.T012_valor       ValorConta
                                , T12.T012_observacao  ObservacaoConta
                             FROM t012_contas_receber T12 
                             JOIN t001_usuario T01 ON T01.t001_login = T12.t001_login
                             JOIN t002_cliente T02 ON T02.T002_codigo = T12.T002_codigo
                            WHERE 1 = 1";
            
            if(!empty($codigoConta))
                $sql    .=  " AND T12.T012_codigo  = $codigoConta"; 
            if(!empty($codigoCliente))
                $sql    .=  " AND T12.T002_codigo  = $codigoCliente";           
            if(!empty($dataInicio))
                $sql    .=  " AND T12.T012_data  >= '$dataInicio'";           
            if(!empty($dataFim))
                $sql    .=  " AND T12.T012_data  <= '$dataFim'";     
            
            if (empty($limit))
                $sql    .=  " LIMIT ".$this->limiteLinhas();
            else if (is_numeric($limit))
                $sql    .=  " LIMIT ".$limit;              
            
            return $this->query($sql);
        }


    }                                                                               
?>