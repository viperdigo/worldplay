<!-- Dialogo Excluir -->
<div id="dialogExcluir" title="Excluir" style="display: none">
    <p>Tem certeza que deseja excluir?</p>
</div>

<!-- Dialogo Status -->
<div id="dialogStatus" title="Alterar Status" style="display: none">
    <p>Tem certeza que deseja alterar o status?</p>
</div>

<!-- Dialogo Contas -->
<div id="dialogContas" title="Contas à Pagar!" style="display: none">
    <br>
    <p>Há conta(s) próxima(s) do vencimento, <a href="?router=T0012/home">acesse aqui.</a></p>
</div>

<!-- Dialogo Editar Pedido -->
<div id="dialogPedido"  style="display: none"></div>

<!-- Dialogo Editar Pedido -->
<div id="dialogManutencao"  style="display: none"></div>

<!-- Dialogo Aprovação de Item -->
<div id="dialogAprovar" title="Caixa de Aprovação" style="display: none">
    <p>Tem certeza que deseja Aprovar?</p>
</div>

<!-- Dialogo Alteração Status de Item -->
<div id="dialogStatus" title="Caixa de Alteração de Status" style="display: none">
    
    <div class="conteudo_16">
        
        <div class="grid_7">
            <p>Status Atual :<label id="lblDialogStatus"></label></p>
        </div>
        
        <div class="clear"></div>
        
        <div class="grid_7">
            <p>Alterar Status para :<label id="lbl2DialogStatus"></label></p>
        </div>
        
        <div class="clear"></div>
        
        <div class="grid_7">
            <p>Histórico</p>
            <textarea rows="10" cols="50" id="txtDialogStatus"></textarea>            
        </div>
        
    </div>
   
</div>

<!-- Dialogo Cancelamento de item -->
<div id="dialogCancelar" title="Caixa de Cancelamento" style="display: none">
    <p>Tem certeza que deseja Cancelar?</p>
</div>

<!-- Dialogo Cancelamento de item -->
<div id="dialogImpressao" title="Confirma Impressão?" style="display: none">
    <p>Tem certeza que deseja imprimir?</p>
</div>

<!-- Dialogo Garantia -->
<div id="dialogGarantia" title="Caixa de Cancelamento" style="display: none">
    <p>Deseja realizar a troca por Garantia?</p>
</div>

<!-- Dialogo Componentes Produto -->
<div id="dialogComponentes" style="display: none"></div>