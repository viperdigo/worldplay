<?php

$obj    =   new models();

$user   =   $_SESSION['user'];

$titulo =   $obj->retornaTitulo(PROGRAMA);

if ($_POST['logoff']=="1")
{
    unset($_SESSION['user']);
    header("Location:index.php");
}

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!--        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7, IE=9"> -->
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
        <title>
            <?php echo NOME_SISTEMA?>
        </title>
        <!-- INICIO CSS  -->
        <link rel="stylesheet" type="text/css" href="template/css/geral.css" />
        <!-- FIM    CSS  -->

        <!--INICIO jQuery UI-->
        <link type="text/css" href="template/jQueryUI/css/custom-theme/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="template/jQueryUI/js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="template/jQueryUI/js/jquery-ui-1.8.20.custom.min.js"></script>
        <!--FIM jQuery UI-->

        <!-- INICIO IMPORT JS   -->
        <script type="text/javascript" src="template/js/mask.js">               </script><!-- Mascaramento Campos                               -->
        <script type="text/javascript" src="template/js/menu.js">               </script><!-- Menu Principal                                    -->
        <script type="text/javascript" src="template/js/tablesorter.js">        </script><!-- Ordernar Tabelas Dinamicamente                    -->
        <script type="text/javascript" src="template/js/quicksearch.js">        </script><!-- Filtrar em Tabelas Dinamicamente                  -->
        <script type="text/javascript" src="template/js/validationLanguage.js"> </script><!-- Mensagens do Validador de Formulário              -->
        <script type="text/javascript" src="template/js/validationEngine.js">   </script><!-- Validador de Formulário                           -->
        <script type="text/javascript" src="template/js/mensagem.js">           </script><!-- Mensagens, Notificações                           -->
        <script type="text/javascript" src="template/js/priceformat.js">        </script><!-- Formatação de Preço                               -->
        <script type="text/javascript" src="template/js/calculation.js">        </script><!-- Calculo de campos dinamico                        -->
        <!--<script type="text/javascript" src="template/js/fileUpload.js">         </script><!-- Upload de Arquivos                                -->
        <script type="text/javascript" src="template/js/funcoesGerais.js">      </script><!-- Funções Gerais sistema, atribuições nas classes   -->
        <!-- FIM    IMPORT JS   -->


        <!--   IMPORTA JS NO PROGRAMA CASO EXISTA     -->
        <?php
        $arquivoJs  =   "template/jsViews/".PROGRAMA.".js";
        if (file_exists($arquivoJs))
            echo "<script src='$arquivoJs'></script>";
        ?>


        <!-- Limpa Logo Apycom   -->
        <div style="visibility:hidden; display: none">
            <a href="http://apycom.com/">Apycom jQuery Menus</a>
        </div>

        <script>
        $(function(){
            $("#logoff").click(function(event){
                event.preventDefault();
                $("#form-logoff").submit();
            })
        })

        </script>

        <!-- Impressora MP-4200 TH Não Fiscal da Bematech-->
        <object id="BemaPrinter1" height="14" width="14" classid="clsid:310DBDAC-85FF-4008-82A8-E22A09F9460B"  viewastext></object>

        <!-- Impressora LabelWriter 450 Etiquetas-->
        <script type="text/javascript" src="template/js/dymoLabelPrinter.js">      </script><!-- Funções Gerais sistema, atribuições nas classes   -->

    </head>
    <?php

    if(PRD_BD=="worldplay_qas")
        echo "<body style='background-color: floralwhite'>";
    else
        echo "<body>";

    ?>
    <body>

        <div id="principal">
            <div class="conteudo_16">
                <div class="grid_2 cabecalho-logo">
                    <img src="template/css/images/logo.png" alt="DZone" class="floatleft" />
                </div>

                <div class="grid_11 cabecalho-titulo">
                    <label><?php echo $titulo; $color = "red"; if(PRD_BD=="worldplay_qas") echo "<strong style='color:$color'> [TESTE]</strong>"?></label>
                </div>

                <form action="" method="post" id="form-logoff">

                    <input type="hidden" value="1" name="logoff">

                    <div class="grid_3 cabecalho-login">
                        <label>Usuário: <?php echo $user; ?> | <a href="#" id="logoff">Sair</a></label>
                    </div>

                </form>


            </div>

        
        
