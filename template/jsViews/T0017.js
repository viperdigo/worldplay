$(function () {

    $('li.excluirConta').click(function (event) {
        event.preventDefault();
        var item = $(this).data('id');
        var linha = $(this).parents(".linhaAdd");

        $('#dialogExcluir').dialog({
            draggable: false,
            resizable: false,
            modal: true,
            width: 300,
            buttons: {
                "Ok": function () {
                    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('/');    //captura url
                    var url = "?" + hashes[0] + "/js.excluir";                                                    //url js.excluirAssociar
                    $.post(url, {item: item}, function (dados) {
                        if (dados == 1) {
                            linha.remove();
                            $('#dialogExcluir').dialog("close");
                            mensagem("", "Excluído com Sucesso!");
                        }
                        else {
                            $('#dialogExcluir').dialog("close");
                            mensagem("e", "Não foi possível excluir!")
                        }
                        ;
                    });

                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

    });

    $(".btnImprimir").live("click", function(e){
        e.preventDefault();
        var $this          =   $(this);
        var codigoConta    =   $this.parents("tr").find(".id").text();
        var valor          =   $this.parents("tr").find(".valor").text();

        //Caixa de Dialogo
        $('#dialogImpressao').dialog({
            draggable: false,
            resizable: false,
            modal: true,
            width: 300,
            buttons: {
                "Ok": function() {
                    $(this).dialog("close");

                    //Imprimi Cabec
                    $.getJSON("?router=T0017/js.impressao",{codigoConta:codigoConta}, function(dados){
                        var strPedido   =   "CONTA Nro. : "+codigoConta;
                        var strCliente  =   "CLIENTE    : ";
                        var strData     =   "DATA       : ";
                        var strValor    =   "VALOR      : ";
                        var strObs      =   "OBSERVACAO : ";

                        $.each(dados, function(cp,vl){
                            if(cp=="CLIENTE")
                                strCliente  =   strCliente+vl;
                            if(cp=="DATA")
                                strData     =   strData+vl;
                            if(cp=="VALOR")
                                strValor    =   strValor+vl;
                            if(cp=="OBSERVACAO")
                                strObs      =   strObs+vl;

                        });

                        //Imprime Linha Pedido
                        Imp_ImprimeString(strPedido,1);

                        //Imprime Linha Cliente
                        Imp_ImprimeString(strCliente,1);

                        //Imprime Linha Data
                        Imp_ImprimeString(strData,1);

                        //Imprime Linha Valor
                        Imp_ImprimeString(strValor,1);

                        //Imprime Linha Obs
                        Imp_ImprimeString(strObs,1);

                    });

                },
                "Cancel": function() {
                    $(this).dialog("close");
                }
            }
        });
    });

});
