$(function(){
    
    $(".btnAprovar").live("click", function(){
        var $this               =   $(this);
        var codigoConta         =   $this.parents("tr").find(".id").html();
        var campoStatus         =   $this.parents("tr").find(".campoStatus");
        var campoDtRetirado     =   $this.parents("tr").find(".campoDtPagto");
        var acoes               =   $this.parents("tr").find(".acoes");
        
        var hoje = new Date()
        var dia = hoje.getDate()
        var mes = hoje.getMonth()
        var ano = hoje.getFullYear()
        
        if (dia < 10)
        dia = "0" + dia
        if (ano < 2000)
        ano = "19" + ano
      
        var dtHoje  =   dia+"/"+(mes+1)+"/"+ano;
        
        $("#dialogAprovar").dialog
        ({
            resizable: false,
            autoopen: true,
            width:300,
            modal: true,
            draggable: false,
            buttons: 
                { 
                    "Aprovar":function(){
                        $.get("?router=T0012/js.aprovaCP",{codigoConta:codigoConta}, function(dados){
                            if (dados==1)
                                {
                                    campoStatus.text("002-PAGO");
                                    campoDtRetirado.text(dtHoje);
                                    acoes.html("");
                                    mensagem('','Conta Aprovada com Sucesso');                                    
                                }else
                                    {
                                        mensagem('e','Não foi possível aprovar a Conta');
                                    }
                        });
                        
                        $(this).dialog("close"); 
                    }
                    ,
                    "Fechar": function(){ 
                        $(this).dialog("close"); 
                        } 
                    } 
        });  
        

    });
    
    $(".btnCancelar").live("click", function(){
        var $this               =   $(this);
        var codigoConta         =   $this.parents("tr").find(".id").html();
        var campoStatus         =   $this.parents("tr").find(".campoStatus");
        var acoes               =   $this.parents("tr").find(".acoes");
        
        $("#dialogCancelar").dialog
        ({
            resizable: false,
            autoopen: true,
            width:300,
            modal: true,
            draggable: false,
            buttons: 
                { 
                    "Cancelar":function(){
                        $.get("?router=T0012/js.cancelarCP",{codigoConta:codigoConta}, function(dados){
                            if (dados==1)
                                {
                                    campoStatus.text("003-CANCELADO");
                                    acoes.html("");
                                    mensagem('','Conta Cancelada com Sucesso');                                    
                                }else
                                    {
                                        mensagem('e','Não foi possível cancelar a Conta');
                                    }
                        });
                        
                        $(this).dialog("close"); 
                    }
                    ,
                    "Fechar": function(){ 
                        $(this).dialog("close"); 
                        } 
                    } 
        });  
        

    });    
    
});
