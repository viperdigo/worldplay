$(function(){
    
    //Copia HTML da Linha Principal
    var linhaHtml =   $("#linhaPrincipal").html();        
    
    //Cria Linha Dinamica
    $(".campoObservacao").live("blur", function(){
        $("#linhaPrincipal").append(linhaHtml);
        $(".campoProdutos:last").focus();
        
        //AutoComplete Para Campos de Produtos
        $( ".campoProdutos" ).autocomplete({
                source: "?router=T0009/js.autoComplete",
                minLength: 1
        });           
    }); 
    
    $(".btnAprovar").live("click", function(){
        var $this               =   $(this);
        var codigoDevolucao     =   $this.parents("tr").find(".id").html();
        var campoStatus         =   $this.parents("tr").find(".campoStatus");
        var acoes               =   $this.parents("tr").find(".acoes");
        
        $("#dialogAprovar").dialog
        ({
            resizable: false,
            autoopen: true,
            width:300,
            modal: true,
            draggable: false,
            buttons: 
                { 
                    "Aprovar":function(){
                        $.get("?router=T0023/js.aprovaDevolucao",{codigoDevolucao:codigoDevolucao}, function(dados){
                            if (dados==1)
                                {
                                    campoStatus.text("002-APROVADO");
                                    acoes.html("");
                                    mensagem('','Devolução aprovada!');                                    
                                }else
                                    {
                                        mensagem('e','Não foi possivel');
                                    }
                        });
                        
                        $(this).dialog("close"); 
                    }
                    ,
                    "Fechar": function(){ 
                        $(this).dialog("close"); 
                        } 
                    } 
        });  
        

    });
        
    $(".btnCancelar").live("click", function(){
        var $this               =   $(this);
        var codigoDevolucao     =   $this.parents("tr").find(".id").html();
        var campoStatus         =   $this.parents("tr").find(".campoStatus");
        var acoes               =   $this.parents("tr").find(".acoes");
        
        $("#dialogCancelar").dialog
        ({
            resizable: false,
            autoopen: true,
            width:300,
            modal: true,
            draggable: false,
            buttons: 
                { 
                    "Cancelar":function(){
                        $.get("?router=T0023/js.cancelarDevolucao",{codigoDevolucao:codigoDevolucao}, function(dados){
                            if (dados==1)
                                {
                                    campoStatus.text("003-CANCELADO");
                                    acoes.html("");
                                    mensagem('','Devolução Cancelada!');                                    
                                }else
                                    {
                                        mensagem('e','Não foi possível cancelar a Devolução');
                                    }
                        });
                        
                        $(this).dialog("close"); 
                    }
                    ,
                    "Fechar": function(){ 
                        $(this).dialog("close"); 
                        } 
                    } 
        });  
        

    });
    
});
