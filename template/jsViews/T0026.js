$(function(){
    
    //Copia HTML da Linha Principal
    var linhaHtml =   $("#linhaPrincipal").html();        
    
    //Cria Linha Dinamica
//    $(".campoObservacao").live("blur", function(){
//        $("#linhaPrincipal").append(linhaHtml);
////        $(".campoProdutos:last").focus();
//        
//        //AutoComplete Para Campos de Produtos
//        $( ".campoProdutos" ).autocomplete({
//                source: "?router=T0009/js.autoComplete",
//                minLength: 1
//        });           
//    }); 
    
    $(".btnAlterarStatus").live("click", function(){
        var $this               =   $(this);
        var codigoManutencao    =   $this.parents("tr").find(".id").html()          ;
        var campoStatus         =   $this.parents("tr").find(".campoStatus")        ;
        
        var btnAlterarStatus    =   $this.parents("tr").find(".liAlterar")          ;
        var btnCancelar         =   $this.parents("tr").find(".liCancelar")         ;
        
        var campoDtEntrada      =   $this.parents("tr").find(".campoDtEntrada")     ;
        var campoDtEnvio        =   $this.parents("tr").find(".campoDtEnvio")       ;
        var campoDtRec          =   $this.parents("tr").find(".campoDtRec")         ;
        var campoDtDev          =   $this.parents("tr").find(".campoDtDev")         ;
        var acoes               =   $this.parents("tr").find(".acoes")              ;
        
        var StatusManutencao    =   "";
        var HistoricoManutencao =   "";
        var ProxStatusManutencao=   "";
        
        var hoje = new Date()
        var dia = hoje.getDate()
        var mes = hoje.getMonth()
        var ano = hoje.getFullYear()
        
        if (dia < 10)
        dia = "0" + dia
        if (ano < 2000)
        ano = "19" + ano
      
        var dtHoje  =   dia+"/"+(mes+1)+"/"+ano;
        
        $.getJSON("?router=T0026/js.statusManutencao", {codigoManutencao:codigoManutencao},function(retorno){
            $.each(retorno, function(campo,valor){
                if(campo=='Status')
                    {
                        StatusManutencao    =   valor;
                    }if (campo == 'Historico')
                        {
                            HistoricoManutencao =   valor;
                        }if (campo == 'Proximo')
                            {
                                ProxStatusManutencao    =   valor;
                            }
            });
            
        $("#lblDialogStatus").text(StatusManutencao);
        $("#lbl2DialogStatus").text(ProxStatusManutencao);
        $("#txtDialogStatus").val(HistoricoManutencao);            

        $("#dialogStatus").dialog
        ({
            resizable: false,
            autoopen: true,
            width:430,
            height: 350,
            modal: true,
            draggable: false,
            buttons: 
                { 
                    "Alterar Status":function(){
                        //Novo valor campo histórico
                        var vlCpHistorico   =   $("#txtDialogStatus").val();
                        
                        $.get("?router=T0026/js.alteraStatus",{codigoManutencao:codigoManutencao
                                                             , statusManutencao:StatusManutencao
                                                             , historicoManutencao:vlCpHistorico}, function(dados){
                            if (dados == 2)
                            {
                                campoStatus.text("002-AGUARDANDO MANUTENÇÃO");
                                campoDtEnvio.text(dtHoje);

                            }else if (dados == 3)
                            {
                                campoStatus.text("003-AGUARDANDO CLIENTE");
                                campoDtRec.text(dtHoje);

                            }else if (dados == 4)
                            {
                                campoStatus.text("004-ENTREGUE CLIENTE");
                                campoDtDev.text(dtHoje);
                                btnAlterarStatus.remove();
                                btnCancelar.remove();
                                
                                var strManutencao   =   "";
                                var strCliente      =   "";
                                var strData         =   "";
                                var strCabec        =   "";
                                var strItem         =   "";
                                var strLinha        =   "";
                                var strAssinatura   =   "";
                                
                                $.getJSON("?router=T0026/js.impressaoManutencao",{codigoManutencao:codigoManutencao}, function(retorno){
                                    $.each(retorno, function(campo,valor){
                                        if ( campo  ==  "MANUTENCAO")
                                            strManutencao   =   valor;
                                        else if ( campo == "CLIENTE")
                                            strCliente      =   valor;
                                        else if ( campo == "DATA")
                                            strData         =   valor;
                                        else if ( campo == "ITEM")
                                            strItem         =   valor;
                                        else if ( campo == "LINHA")
                                            strLinha        =   valor;
                                        else if ( campo == "ASSINATURA")
                                            strAssinatura   =   valor;                                        
                                    });
                                    
                                strManutencao   =   "MANUTENÇÃO   :"+strManutencao  ;
                                strCliente      =   "CLIENTE      :"+strCliente     ;
                                strData         =   "DATA         :"+strData        ;
                                strCabec        =   "CÓDIGO       PRODUTO"          ;
                                strItem         =   strItem                         ;
                                strLinha        =   strLinha                        ;
                                strAssinatura   =   strAssinatura                   ;   
                                    
                                Imp_ImprimeString(strManutencao             ,1);                                
                                Imp_ImprimeString(strCliente                ,1);                                
                                Imp_ImprimeString(strData                   ,2);                                
                                Imp_ImprimeString(strCabec                  ,2);                                
                                Imp_ImprimeString(strItem                   ,2);                                
                                Imp_ImprimeString(strLinha                  ,1);                                
                                Imp_ImprimeString(strAssinatura             ,4);

                                Imp_CorteTotal();                                    
                                });                               

                            }else
                            {
                                mensagem('e','Não foi possível aprovar Manutenção');
                            }
                        });
                        
                        $(this).dialog("close"); 
                    }
                    ,
                    "Fechar": function(){ 
                        $(this).dialog("close"); 
                        } 
                    } 
        });  


        });   
                    
    });   
    
    $(".btnCancelar").live("click", function(){
        var $this               =   $(this);
        var codigoManutencao    =   $this.parents("tr").find(".id").html();
        var campoStatus         =   $this.parents("tr").find(".campoStatus");
        var campoDtCanc         =   $this.parents("tr").find(".campoDtCanc");
        var acoes               =   $this.parents("tr").find(".acoes");
        
        $("#dialogCancelar").dialog
        ({
            resizable: false,
            autoopen: true,
            width:300,
            modal: true,
            draggable: false,
            buttons: 
                { 
                    "Cancelar":function(){
                        $.get("?router=T0026/js.cancelarManutencao",{codigoManutencao:codigoManutencao}, function(dados){
                            if (dados==1)
                                {
                                    var hoje = new Date()
                                    var dia = hoje.getDate()
                                    var mes = hoje.getMonth()
                                    var ano = hoje.getFullYear()

                                    if (dia < 10)
                                    dia = "0" + dia
                                    if (ano < 2000)
                                    ano = "19" + ano

                                    var dtHoje  =   dia+"/"+(mes+1)+"/"+ano;
                                    
                                    campoDtCanc.text(dtHoje)
                                    campoStatus.text("005-CANCELADO");
                                    acoes.html("");
                                    mensagem('','Manutenção Cancelada com Sucesso');                                    
                                }else
                                    {
                                        mensagem('e','Não foi possível cancelar a Manutenção');
                                    }
                        });
                        
                        $(this).dialog("close"); 
                    }
                    ,
                    "Fechar": function(){ 
                        $(this).dialog("close"); 
                        } 
                    } 
        });  
        

    });    
    
    $(".btnItens").live("click", function(e){ 
        e.preventDefault();
        var codigoManutencao    =   $(this).parents("tr").find(".id").text();
        
        $.post("?router=T0026/js.produtosManutencao",{codigoManutencao:codigoManutencao}, function(dados){
           if(dados!=0)
               {
                   $("#dialogManutencao").html(dados);                   
               }
        });
        
        $("#dialogManutencao").dialog
        ({
            resizable: false,
            autoopen: true,
            height:200,
            width:700,
            modal: true,
            draggable: false,        
            title: "Manutencao Nº:"+codigoManutencao,
            buttons: 
            {    
                "Fechar": function(){ 
                                    $("#dialogManutencao").html("");
                                    $(this).dialog("close"); 
                }
            } 
        });        
       
    });

});
