$(function(){

    $(".campoDescricao").blur(function(){
       var descricaoProduto     =   $(this);
       var hashes               =   window.location.href.slice(window.location.href.indexOf('?') +1).split('/');    //captura url
       
       if (hashes[1]=="novo")
           {
                $.post("?router=T0009/js.verificaDescricao",{descricaoProduto:descricaoProduto.val()}, function(retorno){
                    if(retorno==1)
                    {
                        descricaoProduto.val("");
                        descricaoProduto.focus();
                        mensagem("i","Já existe um produto com essa descrição!");
                    }           
                })
           }
    });
    
    $('li.excluirComponente').click(function(event){
        event.preventDefault();
        var linha           =   $(this).parents(".linhaAdd")                                ;   
        var componente      =   $(this).parents(".linhaAdd").find(".campoProdutosNormal")   ;
        var item            =   $('#codigoProduto').val()                                   ;
        var arrProduto      =   componente.val().split("-")                                 ;
        var item2           =   arrProduto[0]                                               ;

        //Caixa de Dialogo
        $('#dialogExcluir').dialog({
                draggable: false,
                resizable: false,
                modal: true,
                width: 300,
                buttons: {
                        "Ok": function() {
                            var hashes      =   window.location.href.slice(window.location.href.indexOf('?') +1).split('/');    //captura url
                            var url         =   "?"+hashes[0]+"/js.excluirComponente";                                                    //url js.excluirAssociar  
                            $.post(url,{item:item,item2:item2},function(dados){
                                if(dados==1)
                                    {
                                        linha.remove();                                        
                                        $('#dialogExcluir').dialog("close");                                                                                 
                                        mensagem("","Excluído com Sucesso!");
                                    }
                                    else
                                        {
                                            $('#dialogExcluir').dialog("close"); 
                                            mensagem("e","Não foi possível excluir!")
                                        }
                                        ;
                            });

                        },
                        "Cancel": function() {
                            $(this).dialog("close");
                        }
                }
        });

    });        

});
