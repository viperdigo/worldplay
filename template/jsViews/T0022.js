$(function(){
    
    //Copia HTML da Linha Principal
    var linhaHtml =   $("#linhaPrincipal").html();        
    
    //Cria Linha Dinamica
    $(".campoObservacao").live("blur", function(){
        $("#linhaPrincipal").append(linhaHtml);
        $(".campoProdutos:last").focus();
        
        //AutoComplete Para Campos de Produtos
        $( ".campoProdutos" ).autocomplete({
                source: "?router=T0009/js.autoComplete",
                minLength: 1
        });           
    }); 
    
    $(".btnGarantia").live("click", function(){
        var $this               =   $(this);
        var codigoTroca         =   $this.parents("tr").find(".id").html();
        var campoStatus         =   $this.parents("tr").find(".campoStatus");
        var acoes               =   $this.parents("tr").find(".acoes");
        
        $("#dialogGarantia").dialog
        ({
            resizable: false,
            autoopen: true,
            width:300,
            modal: true,
            draggable: false,
            buttons: 
                { 
                    "Ok":function(){
                        $.get("?router=T0022/js.garantiaTroca",{codigoTroca:codigoTroca}, function(dados){
                            if (dados==1)
                                {
                                    campoStatus.text("001-GARANTIA");
                                    acoes.html("");
                                    mensagem('','Troca realizada com sucesso!');                                    
                                }else
                                    {
                                        mensagem('e','Não foi possível realizar a troca');
                                    }
                        });
                        
                        $(this).dialog("close"); 
                    }
                    ,
                    "Fechar": function(){ 
                        $(this).dialog("close"); 
                        } 
                    } 
        });  
        

    });
        
    $(".btnCancelar").live("click", function(){
        var $this               =   $(this);
        var codigoTroca         =   $this.parents("tr").find(".id").html();
        var campoStatus         =   $this.parents("tr").find(".campoStatus");
        var acoes               =   $this.parents("tr").find(".acoes");
        
        $("#dialogCancelar").dialog
        ({
            resizable: false,
            autoopen: true,
            width:300,
            modal: true,
            draggable: false,
            buttons: 
                { 
                    "Cancelar":function(){
                        $.get("?router=T0022/js.cancelarTroca",{codigoTroca:codigoTroca}, function(dados){
                            if (dados==1)
                                {
                                    campoStatus.text("003-CANCELADO");
                                    acoes.html("");
                                    mensagem('','Troca Cancelada!');                                    
                                }else
                                    {
                                        mensagem('e','Não foi possível cancelar a Manutenção');
                                    }
                        });
                        
                        $(this).dialog("close"); 
                    }
                    ,
                    "Fechar": function(){ 
                        $(this).dialog("close"); 
                        } 
                    } 
        });  
        

    });
    
});
