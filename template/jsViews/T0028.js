$(function () {

    function soNums(e) {

        //teclas adicionais permitidas (tab,delete,backspace,setas direita e esquerda)
        keyCodesPermitidos = new Array(8, 9, 37, 39, 46);

        //numeros e 0 a 9 do teclado alfanumerico
        for (x = 48; x <= 57; x++) {
            keyCodesPermitidos.push(x);
        }

        //numeros e 0 a 9 do teclado numerico
        for (x = 96; x <= 105; x++) {
            keyCodesPermitidos.push(x);
        }

        //Pega a tecla digitada
        keyCode = e.which;

        //Verifica se a tecla digitada é permitida
        if ($.inArray(keyCode, keyCodesPermitidos) != -1) {
            return true;
        }

        if (e.which == 107) {
            if (!this.form.tabOrder) {
                var els = this.form.elements,
                    ti = [],
                    rest = [];
                for (var i = 0, il = els.length; i < il; i++) {
                    if (els[i].tabIndex > 0 && !els[i].disabled && !els[i].hidden && !els[i].readOnly &&
                        els[i].type !== 'hidden') {
                        ti.push(els[i]);
                    }
                }
                ti.sort(function (a, b) {
                    return a.tabIndex - b.tabIndex;
                });
                for (i = 0, il = els.length; i < il; i++) {
                    if (els[i].tabIndex == 0 && !els[i].disabled && !els[i].hidden && !els[i].readOnly &&
                        els[i].type !== 'hidden') {
                        rest.push(els[i]);
                    }
                }
                this.form.tabOrder = ti.concat(rest);
            }
            for (var j = 0, jl = this.form.tabOrder.length - 1; j < jl; j++) {
                if (this === this.form.tabOrder[j]) {
                    if (j + 1 < jl) {
                        $(this.form.tabOrder[j + 1]).focus();
                    }
                }
            }

            return false;
        }

        if (e.which == 109) {
            if (!this.form.tabOrder) {
                var els = this.form.elements,
                    ti = [],
                    rest = [];
                for (var i = 0, il = els.length; i < il; i++) {
                    if (els[i].tabIndex > 0 && !els[i].disabled && !els[i].hidden && !els[i].readOnly &&
                        els[i].type !== 'hidden') {
                        ti.push(els[i]);
                    }
                }
                ti.sort(function (a, b) {
                    return a.tabIndex - b.tabIndex;
                });
                for (i = 0, il = els.length; i < il; i++) {
                    if (els[i].tabIndex == 0 && !els[i].disabled && !els[i].hidden && !els[i].readOnly &&
                        els[i].type !== 'hidden') {
                        rest.push(els[i]);
                    }
                }
                this.form.tabOrder = ti.concat(rest);
            }
            for (var j = 0, jl = this.form.tabOrder.length; j < jl; j++) {
                if (this === this.form.tabOrder[j]) {
                    if (j + 1 < jl) {
                        $(this.form.tabOrder[j - 1]).focus();
                    } else {
                        $(this).blur();
                    }
                }
            }

            return false
        }

        if (e.which == 13) {


            return true;
        }

        return false;
    }

    $(".cabecalho-logo").remove();

    $(".cabecalho-titulo").css('text-align', 'left');

    $('.qtde').on('keydown', soNums);

    $(".qtde:first").focus();

    $('#botao').css('display', 'none');

    var idleTime = 0;
    $(document).ready(function () {
        //Increment the idle time counter every minute.
        var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

        //Zero the idle timer on mouse movement.
        $(this).mousemove(function (e) {
            idleTime = 0;
        });
        $(this).keypress(function (e) {
            idleTime = 0;
        });
    });

    function timerIncrement() {
        idleTime = idleTime + 1;
        if (idleTime >= 2) { // 2 minutos de inatividade
//            window.location.reload();
            $.post('?router=T0028/js.logoff',{logoff:1}, function(data){
                if(data==1){
                    location.reload();
                }
            });
        }
    }

})
;
