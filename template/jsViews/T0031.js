$(function(){
    
    //Retira enter do formulário
    $('input').live("keypress",function (event) {
         var keyCode = ('which' in event) ? event.which : event.keyCode;              
         return (keyCode == 13) ? false : true;
    });      
    
    //Carrega Valor Unitário do Produto ao sair do campo
    $('.campoProdutos').live("blur",function(){
        //Variáveis e Campos
        var $this               =   $(this)                                         ;
        var strProduto          =   $this.val()                                     ;
        var arrProduto          =   strProduto.split("-")                           ;
        var codigoProduto       =   arrProduto[0]                                   ;
        var cpVlrunit           =   $this.parents(".linhaAdd").find(".campoVlrUnit");
        var lblVlrunit          =   $this.parents(".linhaAdd").find(".lblVlrUnit")  ;
        var cpQtde              =   $this.parents(".linhaAdd").find(".campoQtde")   ;

        //Faz a busca do Valor
        $.get("?router=T0009/js.buscaValor",{codigoProduto:codigoProduto},function(vlrProduto){
            cpVlrunit.val(vlrProduto);
            cpVlrunit.priceFormat({
                prefix: 'R$ ',
                centsSeparator: ',',
                thousandsSeparator: '.'
            });  

            lblVlrunit.text(cpVlrunit.val());
        });                
    });  
    
    //Formatação campo Qtde
    $('.campoQtde').live("focus",function(){
        $(this).priceFormat({
            prefix: '',
            centsSeparator: '',
            thousandsSeparator: ''
        });         
    });    
        
    function adicionaLinha($this, campoVlrVda, campoVlrUnt, campoSubtotal, calculo, oVenda, linha)
    {
        if ((campoVlrVda.val()=="R$ 0,00") ||(campoVlrVda.val()==""))
            {
                oVenda    =   campoVlrUnt;
            }
            else
                {
                    oVenda    =   campoVlrVda;                    
                }

        oVenda      =   oVenda.unmask()        ;
        calculo     =   $this.val() *   oVenda ;

        campoSubtotal.val(calculo)                  ;

        campoSubtotal.priceFormat({
            prefix: 'R$ ',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });    

        var soma    =   $('.campoSubtotal').sum();   

        $('.campoTotal').val(soma);
        
        $('.campoTotal').priceFormat({
            prefix: 'R$ ',
            centsSeparator: ',',
            thousandsSeparator: '.'
        }); 
        
        $("#lblTotal").text($('.campoTotal').val());
        $("#lblTotal2").text($('.campoTotal').val());

        if(linha)
            $("#linhaPrincipal").append(linhaHtml);
        
        $(".campoProdutos:last").focus();

        //AutoComplete Para Campos de Produtos
        $( ".campoProdutos" ).autocomplete({
            source: "?router=T0009/js.autoComplete",
            minLength: 1
        }); 

        $('.campoQtde').live("focus",function(){
            $(this).priceFormat({
                prefix: '',
                centsSeparator: '',
                thousandsSeparator: ''
            });         
        }); 

        $('.campoVlrVenda').priceFormat({
            prefix: 'R$ ',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });          
    }
         
    var linhaHtml   =   $('#linhaPrincipal').html();
        
    $('#botaoGravar').live("click",function(){                                                 
        $('#formPedido').submit();        
    });   
        
    $('.campoQtde').live("blur",function(){
        var $this           =   $(this);
        var campoVlrVda     =   $this.parents('.linhaAdd').find('.campoVlrVenda')   ;         
        var campoVlrUnt     =   $this.parents('.linhaAdd').find('.campoVlrUnit')    ;         
        var campoSubtotal   =   $this.parents('.linhaAdd').find('.campoSubtotal')   ;          
        var lblSubtotal     =   $this.parents('.linhaAdd').find('.lblSubtotal')     ;          
        var calculo         =   0   ;   
        var oVenda          =   0   ;   
        
        if (($this.val()!="000") && ($this.val()!=""))
            adicionaLinha($this, campoVlrVda, campoVlrUnt, campoSubtotal, calculo, oVenda);
        else
            $this.focus();
        
        lblSubtotal.text(campoSubtotal.val());
        
    });
        
    $('.campoQtde').live("keypress",function (e) {
        var keyCode         =   ('which' in event) ? event.which : event.keyCode;
        var $this           =   $(this);
        var campoVlrVda     =   $this.parents('.linhaAdd').find('.campoVlrVenda')   ;         
        var campoVlrUnt     =   $this.parents('.linhaAdd').find('.campoVlrUnit')    ;         
        var campoSubtotal   =   $this.parents('.linhaAdd').find('.campoSubtotal')   ;          
        var lblSubtotal     =   $this.parents('.linhaAdd').find('.lblSubtotal')     ;          
        var calculo         =   0   ;   
        var oVenda          =   0   ;
        
        if ((keyCode == 13) && ($this.val()!="000") && ($this.val()!=""))
            adicionaLinha($this, campoVlrVda, campoVlrUnt, campoSubtotal, calculo, oVenda, 1)                
        else
            $this.focus();
        
        lblSubtotal.text(campoSubtotal.val());
        
        
    });   
        
    
//    $(".linhaAdd").keypress(function(event) {
//      if ( event.which == 13 ) {
//         event.preventDefault();
//       }
//       xTriggered++;
//       var msg = "Handler for .keypress() called " + xTriggered + " time(s).";
//      alert( msg, "html" );
//      alert( event );
//    });    
    
    $(".conteudo_16 .linha").live("mouseover", function(){
        $(this).css("background-color","#EEE9E9");
        
    });
    
    $(".conteudo_16 .linha").live("mouseout", function(){
        $(this).css("background-color","");
    });
    
    $(".conteudo_16 .linha").live("click", function(){
        var $this           =   $(this);
        var codigoRomaneio  =   $this.find(".codigoRomaneio").text();
        var codigoCliente   =   $("#cpCliente").val();
        
        $.getJSON("?router=T0008/js.detalhesModal",{codigoRomaneio:codigoRomaneio,codigoCliente:codigoCliente},function(dados){
            var cliente =   $('#cpCliente option:selected').html();
            var data    =   $this.find('#lData').text();
            var romaneio=   $this.find('#lRomaneio').text();
            var total   =   $this.find('#lTotal').text();
            
            $('#pCliente').text(cliente);
            $('#pData').text(data);
            $('#pRomaneio').text(romaneio);
            $('#l1Total').text(total);
            $('.dados').html(dados);           
            
        });
        
        $("#detalhesRomaneio").dialog
        ({
            resizable: false,
            autoopen: true,
            height:600,
            width:790,
            modal: true,
            draggable: false,
            buttons: 
                { "Fechar": function() { $(this).dialog("close"); } } 
        })        
        
        
    });
    
    $(".vlrVendaDialog").live("blur",function(){
        var $this           =   $(this);
        var subtotalDialog  =   $this.parents(".linhaDialog").find(".subTotalDialog");
        var qtdeDialog      =   $this.parents(".linhaDialog").find(".qtdeDialog");
        var calculo         =   0;
        var vlrVendaDialog  =   $this.unmask();           
        
        if(vlrVendaDialog!=0)
        {
            calculo =  parseInt(qtdeDialog.text()) * vlrVendaDialog ;        
            calculo =   calculo/100;
            subtotalDialog.text(calculo.toFixed(2));
        }
    });
    
    $(".alterarPedido").live("click",function(e){
        e.preventDefault()
        var $this           =   $(this);
        var codigoCliente   =   $this.parents("tr").find(".codigoCliente").text();
        var codigoPedido    =   $this.parents("tr").find("td:first").text();
        var valorPedido     =   $this.parents("tr").find(".valorPedido");
        
        $.post("?router=T0031/js.alterarProdutos",{codigoPedido:codigoPedido},function(dados){
            $("#dialogPedido").html(dados);
            //Inseri mascara R$
            $("#dialogPedido").find('input[name$="T021_valor_venda"]').priceFormat({
                                                                        prefix: 'R$ ',
                                                                        centsSeparator: ',',
                                                                        thousandsSeparator: '.'
                                                                    });                                                                  
        });

        $("#dialogPedido").dialog
        ({
            resizable: false,
            autoopen: true,
            height:550,
            width:850,
            modal: true,
            draggable: false,        
            title: "Pedido Nº:"+codigoPedido,
            buttons: 
            {   "Atualizar Valores": function(){
                                    var arrCodigo   =   [];
                                    var arrVlrVda   =   [];
                                    var arrSubTotal =   [];

                                    $('.linhaDialog').each(function(){
                                        var codigoProduto =   $(this).find(".produtoDialog").text();
                                        var vlrVenda      =   $(this).find(".vlrVendaDialog");
                                        var subtotal      =   $(this).find(".subTotalDialog").text();                                                                                
                                        
                                        if(vlrVenda.unmask()!=0)
                                            {
                                                arrCodigo.push(codigoProduto);
                                                arrVlrVda.push(vlrVenda.val());
                                                arrSubTotal.push(subtotal);                                                                                                                                                                           
                                            }
                                    });

                                    $.get("?router=T0031/js.atualizaPrecos",{  codigoCliente:codigoCliente
                                                                             , codigoPedido:codigoPedido
                                                                             , valorPedido:valorPedido.text()
                                                                             , arrCodigo:arrCodigo
                                                                             , arrVlrVda:arrVlrVda
                                                                             , arrSubTotal:arrSubTotal}
                                                                             , function(dados){
                                                                                if(dados!=0)
                                                                                    {
                                                                                        valorPedido.text("R$ "+dados)
                                                                                        mensagem("","Preços Alterados com Sucesso!");
                                                                                    }
                                                                                else
                                                                                    mensagem("e","Não foi possível alterar o preços!");                                                                                                                                                                               
                                    });  

                                    $("#dialogPedido").html("");
                                    $(this).dialog("close");
                                }, 
                "Cancelar": function(){ 
                                    $("#dialogPedido").html("");
                                    $(this).dialog("close"); 
                }
            } 
        });           
    });
    
    $(".btnImprimir").live("click", function(e){
        e.preventDefault();
        var $this           =   $(this);
        var codigoPedido    =   $this.parents("tr").find(".id").text();
        var valorPedido     =   $this.parents("tr").find(".valorPedido").text();
        
        //Caixa de Dialogo
        $('#dialogImpressao').dialog({
                draggable: false,
                resizable: false,
                modal: true,
                width: 300,
                buttons: {
                        "Ok": function() {
                            $(this).dialog("close");   
                            
                            //Imprimi Cabec
                            $.getJSON("?router=T0031/js.impressao",{codigoPedido:codigoPedido, tipo:1}, function(dados){
                                var strPedido   =   "PEDIDO Nro.: "+codigoPedido;
                                var strCliente  =   "CLIENTE    : ";
                                var strData     =   "DATA       : ";
                                                                
                                $.each(dados, function(cp,vl){
                                    if(cp=="CLIENTE")
                                        strCliente  =   strCliente+vl;
                                    if(cp=="DATA")
                                        strData     =   strData+vl;                                    
                                });
                                
                                //Imprime Linha Pedido
                                Imp_ImprimeString(strPedido,1);
                                
                                //Imprime Linha Cliente
                                Imp_ImprimeString(strCliente,1);
                                
                                //Imprime Linha Data
                                Imp_ImprimeString(strData,1);
                            
                            });
                            
                            //Imprimi Corpo
                            $.getJSON("?router=T0031/js.impressao",{codigoPedido:codigoPedido, tipo:2}, function(dados){
                                var strCabec    =   "Nro Descricao               Qtd VlUnit Subtotal";
                                var strProduto  =   "";
                                var strRodape   =   "             TOTAL GERAL "+valorPedido;

                                //Pula 2 Linhas
                                Imp_ImprimeString("", 2);   
                                
                                //Imprime Linha Cabecalho Produto
                                Imp_ImprimeString(strCabec, 2);                              
                                
                                $.each(dados, function(cp,vl){
                                    strProduto  =   "";
                                    strProduto  =   strProduto+vl;
                                    
                                    //Imprime Linha Produto
                                    Imp_ImprimeString(strProduto,1);
                                });
                                
                                //Pula 3 Linhas
                                Imp_ImprimeString("", 3);
                                
                                //Imprime subtotal
                                Imp_ImprimeString(strRodape, 3);
                                                                
                                //Corta Papel
                                Imp_CorteTotal();
                                
                            });
                            
                        },
                        "Cancel": function() {
                            $(this).dialog("close");
                        }
                }
        });                       
    });
    
    $(".excluirLinha").live("click", function(){
       var $this    =   $(this);
       $this.parents(".linhaAdd").remove();
    });
       
});
