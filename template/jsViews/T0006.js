$(function () {

    //Botão Alterar Status
    $('li.status').click(function (event) {
        event.preventDefault();
        var linha = $(this).parents('td').parents('tr');
        var item = linha.find('.id').text();
        var status = linha.find('.statusCliente').text();

        //Caixa de Dialogo
        $('#dialogStatus').dialog({
            draggable: false,
            resizable: false,
            modal: true,
            width: 300,
            buttons: {
                "Ok": function () {
                    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('/');    //captura url
                    var url = "?" + hashes[0] + "/js.alterarStatus";                                                    //url js.alterarStatus
                    $.post(url, {item: item, status: status}, function (dados) {
                        if (dados == 1) {
                            linha.remove();
                            $(this).dialog("close");
                            mensagem("", "Status alterado com Sucesso!");
                        }
                        else {
                            $(this).dialog("close");
                            mensagem("e", "Não foi possível alterar status!")
                        }
                        ;
                        location.reload();
                    });

                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

    });


});
