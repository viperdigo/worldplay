$(function(){
    
    //Copia HTML da Linha Principal, para realizar a cópia da dinamica em Componentes
    var linhaHtml =   $("#linhaPrincipal").html();
        
    //Mostra os Componentes do Produto Referencia
    function mostraComponentes(chk)
    {
        var strProduto      =   $(".campoProdutosReceita").val();
        var arrProduto      =   strProduto.split('-');
        var codigoProduto   =   arrProduto[0];
        
        $.post("?router=T0009/js.buscaComponentes", {codigoProduto:codigoProduto}, function(dados){            
            if(dados!=0)
            {
                if(chk==1)
                    {
                        $("#dialogComponentes").html(dados);                
                        $("#dialogComponentes").dialog
                        ({
                            resizable: false,
                            autoopen: true,
                            height:400,
                            width:450,
                            modal: true,
                            draggable: false,
                            title: "Componente(s) do produto "+$(".campoProdutosReceita").val(),
                            buttons: 
                                { "Fechar": function(){ 
                                    $(this).dialog("close"); 
                                    } 
                                } 

                        });
                    }
            }else
                {
                    mensagem("e","Produto Referencia incorreto!");
                    $(".campoProdutosReceita").val("");
                }
        });                                                           
    }
    
    //Cria Linha Dinamica
    $(".campoQtd").live("blur", function(){
        $("#linhaPrincipal").append(linhaHtml);
        $(".campoProdutosNormal:last").focus();
        
        //AutoComplete Para Campos de Produtos
        $( ".campoProdutosNormal" ).autocomplete({
                source: "?router=T0009/js.autoComplete&normal=1",
                minLength: 1
        });           
    });
    
    //Ao Clicar mostra os componentes do produto para o usuário em Dialogo    
    $("#btComponente").click(function(e){
        e.preventDefault();
        mostraComponentes(1);
    });
    
    $(".descReceita").focus(function(){
        mostraComponentes();
    });

    //Ao abrir a tela, inclui DISABLED em todos Inputs, para campos Receita
    $("#receita").find("input").attr('disabled', 'disabled');
    $("#receita").find("select").attr('disabled', 'disabled');
        
    //Botao Radio ( Normal - Receita )
    $(":checked").live("click", function(){
        var $this   =   $(this).val();
        if($this==1 || $this==3)
            {
                $("#receita").css('display', 'none');
                $("#normal").css('display', 'block');
                
                $("#normal").find("input").removeAttr('disabled','');
                $("#normal").find("select").removeAttr('disabled','');                
                
                $("#receita").find("input").attr('disabled', 'disabled');
                $("#receita").find("select").attr('disabled', 'disabled');
            }
            else if ($this==2)
                {
                    $("#receita").css('display', 'block');
                    $("#normal").css('display', 'none');  

                    $("#receita").find("input").removeAttr('disabled','');
                    $("#receita").find("select").removeAttr('disabled','');
                    
                    $("#normal").find("input").attr('disabled', 'disabled');
                    $("#normal").find("select").attr('disabled', 'disabled');                    
                }
    }); 
    
    $(".campoDescricao").blur(function(){
       var descricaoProduto     =   $(this);
       var hashes               =   window.location.href.slice(window.location.href.indexOf('?') +1).split('/');    //captura url
       
       if (hashes[1]=="novo")
           {
                $.post("?router=T0009/js.verificaDescricao",{descricaoProduto:descricaoProduto.val()}, function(retorno){
                    if(retorno==1)
                    {
                        descricaoProduto.val("");
                        descricaoProduto.focus();
                        mensagem("i","Já existe um produto com essa descrição!");
                    }           
                })
           }
    });
    
    $('li.excluirComponente').click(function(event){
        event.preventDefault();
        var linha           =   $(this).parents(".linhaAdd")                                ;   
        var componente      =   $(this).parents(".linhaAdd").find(".campoProdutosNormal")   ;
        var item            =   $('#codigoProduto').val()                                   ;
        var arrProduto      =   componente.val().split("-")                                 ;
        var item2           =   arrProduto[0]                                               ;

        //Caixa de Dialogo
        $('#dialogExcluir').dialog({
                draggable: false,
                resizable: false,
                modal: true,
                width: 300,
                buttons: {
                        "Ok": function() {
                            var hashes      =   window.location.href.slice(window.location.href.indexOf('?') +1).split('/');    //captura url
                            var url         =   "?"+hashes[0]+"/js.excluirComponente";                                                    //url js.excluirAssociar  
                            $.post(url,{item:item,item2:item2},function(dados){
                                if(dados==1)
                                    {
                                        linha.remove();                                        
                                        $('#dialogExcluir').dialog("close");                                                                                 
                                        mensagem("","Excluído com Sucesso!");
                                    }
                                    else
                                        {
                                            $('#dialogExcluir').dialog("close"); 
                                            mensagem("e","Não foi possível excluir!")
                                        }
                                        ;
                            });

                        },
                        "Cancel": function() {
                            $(this).dialog("close");
                        }
                }
        });

    });        
    
    $("#adicionaLinha").live("click",function(e){
        e.preventDefault();
        
        var html    =   "";
        
            html += '<div class="linhaAdd">';
            
            html += '   <div class="grid_7">';
            html += '       <input type="text" name="T003_componente[]"         class="campoProdutosNormal validate[required]"/>';
            html += '   </div>';

            html += '   <div class="grid_2">';
            html += '       <input type="text" name="T013_qtd_componente[]"     class="campoQtd validate[required]"/>';
            html += '   </div>';
            
            html += '</div>';
            
        $(html).appendTo("#linhas");
        
        $(".campoProdutosNormal:last").focus();
        
        //AutoComplete Para Campos de Produtos
        $( ".campoProdutosNormal" ).autocomplete({
                source: "?router=T0009/js.autoComplete&normal=1",
                minLength: 1
        });
        
        $('form.validar').validationEngine();
        
    });
    
});
