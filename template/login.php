<?php

    //Configuração Gerais
    require_once 'app/config.php';

    //Modelos/Métodos ja construidos e conexão com os BDs
    require_once 'app/models.php';

    $obj    =   new models();
    
    if (!empty($_POST))
    {
        $user   =   $_POST['usuario'];
        $senha  =   $_POST['senha'];

        $obj->autentica($user, $senha);
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=utf-8 />
        <title>Gerenciador Worldplay</title>
        <link rel="stylesheet" type="text/css" href="template/css/login.css">

        <link type="text/css" href="template/jQueryUI/css/custom-theme/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="template/jQueryUI/js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="template/jQueryUI/js/jquery-ui-1.8.20.custom.min.js"></script>

    </head> 
    <script type="text/javascript">
        function limpar(campo){
            if (campo.value == campo.defaultValue){
                campo.value = "";
            }
        }
        function escrever(campo){
            if (campo.value == ""){
                campo.value = campo.defaultValue;
            }
        }

        $(function(){

            $('input[name="usuario"]').keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    $('input[name="senha"]').focus();
                }
            });



        })

    </script>
    <body>
        <form action="" method="post">
            <div id="login-box">

                <div id="login-box-interno">

                    <div id="login-box-label">
                        Digite seu Login e Senha!
                    </div>

                    <div class="input-div" id="input-usuario">
                        <input type="text" name="usuario"   value="Usuário ou Email"    onfocus="limpar(this)" onblur="escrever(this)" autofocus/>
                    </div>

                    <div class="input-div" id="input-senha">
                        <input type="password" name="senha"     value="Senha"               onfocus="limpar(this)" onblur="escrever(this)"/>
                    </div>

                    <div id="botoes">

                        <div id="botao">
                            <input type="submit" value="Login"/>
                        </div>

                    </div>

                </div>

            </div>

        </form>  
    </body>
    
</html>