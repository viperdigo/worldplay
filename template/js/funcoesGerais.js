$(function () {

    //Tabs
    $(function () {
        $("#tabs").tabs();
    });

    //Radio
    $(function () {
        $("#radio").buttonset();
    });

    // Tabs
    $('#tabs').tabs();

    //AutoComplete Para Campos de Produtos
    $(".campoProdutos").autocomplete({
        source: "?router=T0009/js.autoComplete",
        minLength: 1
    });

    //AutoComplete Para Campos de Produtos Receita
    $(".campoProdutosReceita").autocomplete({
        source: "?router=T0009/js.autoComplete&receita=2",
        minLength: 1
    });

    //AutoComplete Para Campos de Produtos Normal
    $(".campoProdutosNormal").autocomplete({
        source: "?router=T0009/js.autoComplete&normal=1",
        minLength: 1
    });

    //AutoComplete Para Campos de Clientes
    $(".campoClientes").autocomplete({
        source: "?router=T0006/js.autoComplete",
        minLength: 1
    });

    $(".campoClientes").live("blur", function () {
        var $this = $(this);
        var codigoCliente = $this.val().split("-");
        $("#auxClientes").val(codigoCliente[0]);
    });

    // Datepicker
    $.datepicker.regional['pt-BR'] = {
        closeText: 'Fechar',
        prevText: '&#x3c;Anterior',
        nextText: 'Pr&oacute;ximo&#x3e;',
        currentText: 'Hoje',
        monthNames: ['Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
            'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
    $('.data').datepicker($.datepicker.regional['pt-BR']);

    //hover states on the static widgets
    $('#barra-botoes-links, ul#icons li, input').hover(
        function () {
            $(this).addClass('ui-state-hover');
        },
        function () {
            $(this).removeClass('ui-state-hover');
        }
    );

    //Mascaras
    $('.ddd').mask('(999)');
    $('.telefone').mask('9999-9999');
    $('.cnpj').mask('99.999.999/9999-99');
    $('.cpf').mask('999.999.999-99');
    $('.rg').mask('99.999.999-9');
    $('.cep').mask('99999-999');
    //$('.data').mask('99/99/9999');

    //Formata Valor
    $('.valor').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    //Ordenação de Tabela (Tablesorter)
//    $(".tablesorter").tablesorter({sortList:[[0,0],[2,1]], widgets: ['zebra']});
//    $(".tablesorter").tablesorter({sortList: [[0,0]], headers: {4:{sorter: false}}});

    //Input Filtro Dinamico (Quicksearch)
    $('input.filtroDinamico').quicksearch('table tbody tr');

    //Validação Formulário
    $('form.validar').validationEngine();

    //Abre e Fecha Filtros
    $('.abrirFiltros').live('click', function () {
        var $div_filtro = $('#barra-filtros').find('form');
        $div_filtro.toggle('fast');
    });

    //Botão Excluir
    $('li.excluir').click(function (event) {
        event.preventDefault();
        var linha = $(this).parents('td').parents('tr');
        var item = linha.find('.id').text();

        //Caixa de Dialogo
        $('#dialogExcluir').dialog({
            draggable: false,
            resizable: false,
            modal: true,
            width: 300,
            buttons: {
                "Ok": function () {
                    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('/');    //captura url
                    var url = "?" + hashes[0] + "/js.excluir";                                                    //url js.excluir
                    $.post(url, {item: item}, function (dados) {
                        if (dados == 1) {
                            linha.remove();
                            $('#dialogExcluir').dialog("close");
                            mensagem("", "Excluído com Sucesso!");
                        }
                        else {
                            $('#dialogExcluir').dialog("close");
                            mensagem("e", "Não foi possível excluir!")
                        }
                        ;
                    });

                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

    });

    //Botão Excluir
    $('li.excluirAssociar').click(function (event) {
        event.preventDefault();
        var linha = $(this).parents('td').parents('tr');
        var item = linha.find('.id').text();
        var item2 = $('#id').val();

        //Caixa de Dialogo
        $('#dialogExcluir').dialog({
            draggable: false,
            resizable: false,
            modal: true,
            width: 300,
            buttons: {
                "Ok": function () {
                    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('/');    //captura url
                    var url = "?" + hashes[0] + "/js.excluirAssociar";                                                    //url js.excluirAssociar
                    $.post(url, {item: item, item2: item2}, function (dados) {
                        if (dados == 1) {
                            linha.remove();
                            $('#dialogExcluir').dialog("close");
                            mensagem("", "Excluído com Sucesso!");
                        }
                        else {
                            $('#dialogExcluir').dialog("close");
                            mensagem("e", "Não foi possível excluir!")
                        }
                        ;
                    });

                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

    });

    //Verifica se há contas proxima a data expiração      
    var timeRefresh = 1000000;
    setInterval(function () {
        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();
        //Dialogo 10h
        if ((h == 10) && (m <= 15) || (h == 12) && (m <= 15) || (h == 14) && (m <= 15)) {
            $.post("?router=T0012/js.verificaContasPagar", function (dados) {
                if (dados == 1) {
                    $("#dialogContas").dialog
                    ({
                        resizable: false,
                        height: 150,
                        width: 450,
                        draggable: false,
                        modal: true,
                        buttons: {
                            Fechar: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            });
        }
    }, timeRefresh);
});

var stack_bar_bottom = {"dir1": "up", "dir2": "right", "spacing1": 0, "spacing2": 0};

function mensagem(tipo, texto) {
    var opts = {
        pnotify_title: "Mensagem!",
        pnotify_text: texto,
        pnotify_addclass: "stack-bar-bottom",
        pnotify_width: "30%",
        pnotify_stack: stack_bar_bottom
    };
    switch (tipo) {
        case 'e': //Erro
            opts.pnotify_title = "Erro!";
            opts.pnotify_type = "error";
            break;
        case 'i': //Informação
            opts.pnotify_title = "Informação!";
            opts.pnotify_type = "info";
            break;
    }
    $.pnotify(opts);
};


/*########### FUNÇÕES PARA IMPRESSORA MP-4200 TH ###############*/

function Imp_ImprimeString(strImp, pulaLinha) {
    //Abre Comunicação Impressora
    BemaPrinter1.IniciaPorta("COM3");

    //Configura modelo Impressora para MP-4200 TH = 7
    BemaPrinter1.ConfiguraModeloImpressora(7);

    //Imprime
    if (pulaLinha == 1)
        BemaPrinter1.FormataTX(strImp + "\n", 2, 0, 0, 0, 0);
    if (pulaLinha == 2)
        BemaPrinter1.FormataTX(strImp + "\n\n", 2, 0, 0, 0, 0);
    if (pulaLinha == 3)
        BemaPrinter1.FormataTX(strImp + "\n\n\n", 2, 0, 0, 0, 0);
    if (pulaLinha == 4)
        BemaPrinter1.FormataTX(strImp + "\n\n\n\n", 2, 0, 0, 0, 0);
    if (pulaLinha == 5)
        BemaPrinter1.FormataTX(strImp + "\n\n\n\n\n", 2, 0, 0, 0, 0);
    if (pulaLinha == 6)
        BemaPrinter1.FormataTX(strImp + "\n\n\n\n\n\n", 2, 0, 0, 0, 0);
    if (pulaLinha == 7)
        BemaPrinter1.FormataTX(strImp + "\n\n\n\n\n\n\n", 2, 0, 0, 0, 0);
    if (pulaLinha == 8)
        BemaPrinter1.FormataTX(strImp + "\n\n\n\n\n\n\n\n", 2, 0, 0, 0, 0);
    if (pulaLinha == 9)
        BemaPrinter1.FormataTX(strImp + "\n\n\n\n\n\n\n\n\n", 2, 0, 0, 0, 0);
    if (pulaLinha == 10)
        BemaPrinter1.FormataTX(strImp + "\n\n\n\n\n\n\n\n\n\n", 2, 0, 0, 0, 0);

    //Fecha Comunicação Impressora
    BemaPrinter1.FechaPorta();

}

function Imp_CorteParcial() {
    //Abre Comunicação Impressora
    BemaPrinter1.IniciaPorta("COM3");

    //Configura modelo Impressora para MP-4200 TH = 7
    BemaPrinter1.ConfiguraModeloImpressora(7);

    //Corte do Papel 1-Completo, 0-Parcial
    BemaPrinter1.AcionaGuilhotina(0);

    //Fecha Comunicação Impressora
    BemaPrinter1.FechaPorta();

}

function Imp_CorteTotal() {
    //Abre Comunicação Impressora
    BemaPrinter1.IniciaPorta("COM3");

    //Configura modelo Impressora para MP-4200 TH = 7
    BemaPrinter1.ConfiguraModeloImpressora(7);

    //Corte do Papel 1-Completo, 0-Parcial
    BemaPrinter1.AcionaGuilhotina(1);

    //Fecha Comunicação Impressora
    BemaPrinter1.FechaPorta();

}

function ImprimeLabel() {
    var textTextArea = document.getElementById('txtAreaPrtLbl');

    try {
        // open label
        var labelXml = '<?xml version="1.0" encoding="utf-8"?>\
    <DieCutLabel Version="8.0" Units="twips">\
        <PaperOrientation>Landscape</PaperOrientation>\
        <Id>Address</Id>\
        <PaperName>30252 Address</PaperName>\
        <DrawCommands/>\
        <ObjectInfo>\
            <TextObject>\
                <Name>Text</Name>\
                <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
                <BackColor Alpha="0" Red="255" Green="255" Blue="255" />\
                <LinkedObjectName></LinkedObjectName>\
                <Rotation>Rotation0</Rotation>\
                <IsMirrored>False</IsMirrored>\
                <IsVariable>True</IsVariable>\
                <HorizontalAlignment>Left</HorizontalAlignment>\
                <VerticalAlignment>Middle</VerticalAlignment>\
                <TextFitMode>ShrinkToFit</TextFitMode>\
                <UseFullFontHeight>True</UseFullFontHeight>\
                <Verticalized>False</Verticalized>\
                <StyledText/>\
            </TextObject>\
            <Bounds X="332" Y="150" Width="4455" Height="1260" />\
        </ObjectInfo>\
    </DieCutLabel>';
        var label = dymo.label.framework.openLabelXml(labelXml);

        // set label text
        label.setObjectText("Text", textTextArea.value);

        // select printer to print on
        // for simplicity sake just use the first LabelWriter printer
        var printers = dymo.label.framework.getPrinters();
        if (printers.length == 0)
            throw "No DYMO printers are installed. Install DYMO printers.";

        var printerName = "";
        for (var i = 0; i < printers.length; ++i) {
            var printer = printers[i];
            if (printer.printerType == "LabelWriterPrinter") {
                printerName = printer.name;
                break;
            }
        }

        if (printerName == "")
            throw "No LabelWriter printers found. Install LabelWriter printer";

        // finally print the label
        label.print(printerName);
    }
    catch (e) {
        alert(e.message || e);
    }
}
