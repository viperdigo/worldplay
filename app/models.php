<?php

/**************************************************************************/
/*                          DAVÓ SUPERMERCADOS                            */
/* Criado em: 21/03/2011 por Rodrigo Alfieri                              */
/* Descrição: Classe para realizar conexão com os BD's e
 *           funções utilizadas em todos os programas                     */

/**************************************************************************/

class models extends PDO
{

	var $usuario;
	var $senha;
	var $consulta = "";

	public function __construct()
	{

		parent::__construct('mysql:host=' . PRD_HOST . ';dbname=' . PRD_BD, PRD_USER, PRD_PASS);
		$this->exec("SET NAMES 'utf8'");
		$this->exec("SET character_set_connection=utf8");
		$this->exec("SET character_set_client=utf8");
		$this->exec("SET character_set_results=utf8");
	}

	public function autentica($user, $senha)
	{
		$sql = "  SELECT T01.t001_login Login
                            , T01.t001_senha Senha
                         FROM t001_usuario T01
                        WHERE T01.t001_login = '$user'
                          AND T01.t001_senha = '" . md5($senha) . "'";

		if ($this->query($sql)->rowCount()) {
			$_SESSION['user'] = $user;

			$sqlPerfil = "SELECT *
                            FROM t000_t001
                           WHERE t001_login = '" . $_SESSION['user'] . "'
                           LIMIT 1";

			foreach ($this->query($sqlPerfil) as $row) {

				if ($row['t000_codigo'] == 28) {
					header("Location:index.php?router=T0028/tb.home");
				} elseif ($row['t000_codigo'] == 29) {
					header("Location:index.php?router=T0029/tb.home");
				} else {
					header("Location:index.php");
				}
			}

		}

	}

	public
	function retornaTitulo(
		$programa
	)
	{
		if (($programa == 'home') || (empty($programa))) {
			return "Gerenciador WorldPlay";
		} else {
			$programa = str_replace("T", "", $programa);
			$sql = "SELECT T00.T000_titulo
                       FROM t000_estrutura T00
                      WHERE T00.t000_codigo  = $programa";

			$titulo = $this->query($sql)->fetchAll(PDO::FETCH_ASSOC);

			return $titulo[0]['T000_titulo'];
		}
	}

	public
	function seleciona(
		$tabela,
		$campos = "*",
		$delimitador = "",
		$quantidade = 1
	)
	{
		$sql = false;
		if (!empty($tabela)) {
			$sql = "SELECT " . $campos . " FROM " . $tabela;
			if ($delimitador != "") {
				$sql .= " WHERE " . $delimitador;
			}
		}

		return $sql;
	}

	public
	function insere(
		$tabela,
		$campos
	)
	{
		$sql = false;
		if (!empty($tabela)) {
			$sql = "INSERT INTO " . $tabela . " (";
			foreach ($campos as $nomes => $valores) {
				$sql_aux1 .= $nomes . ",";
				$sql_aux2 .= $this->formataValor($tabela, $nomes, $valores) . ",";
			}
			$sql_aux1 = substr($sql_aux1, 0, strlen($sql_aux1) - 1);
			$sql_aux2 = substr($sql_aux2, 0, strlen($sql_aux2) - 1);
//            echo $sql.$sql_aux1.") VALUES (".$sql_aux2.")";
//            echo "<br/>";die;
			return $sql . $sql_aux1 . ") VALUES (" . $sql_aux2 . ")";
		}
	}

	public
	function atualiza(
		$tabela,
		$campos,
		$delimitador
	)
	{
		$sql = false;
		if (!empty($tabela)) {
			$sql = "UPDATE " . $tabela . " SET ";
			foreach ($campos as $nomes => $valores) {
				$sql_aux .= $nomes . " = " . $this->formataValor($tabela, $nomes, $valores) . ",";
			}
			$sql_aux = substr($sql_aux, 0, (strlen($sql_aux) - 1));
//            echo  $sql.$sql_aux. " WHERE ".$delimitador;
//            echo "<br/>";
			return $sql . $sql_aux . " WHERE " . $delimitador;
		}
	}

	public
	function exclui(
		$tabela,
		$delimitador
	)
	{
		$sql = false;
		if (!empty($tabela)) {
			$sql = "DELETE FROM " . $tabela . " WHERE " . $delimitador;
//            echo  $sql;
		}

		return $sql;
	}

	public
	function formataValor(
		$tabela,
		$campo,
		$valor
	)
	{
		if (strpos($campo, "senha") === 0) {
			return "'" . md5($valor) . "'";

		} elseif ($this->verificaTipo($tabela, $campo) == "VAR_STRING"
			|| $this->verificaTipo($tabela, $campo) == "STRING"
			|| $this->verificaTipo($tabela, $campo) == "TIME"
			|| $this->verificaTipo($tabela, $campo) == "BLOB"
		) {
			$valor = strtoupper($valor);
			$valor = str_replace("'", "`", $valor);

			return "'" . $valor . "'";
		} elseif ($this->verificaTipo($tabela, $campo) == "DATE") {
			if (empty($valor)) {
				return "null";
			} else {
				return "'" . $this->formataData($valor) . "'";
			}
		} elseif ($this->verificaTipo($tabela, $campo) == "DATETIME") {
			if (empty($valor)) {
				return "null";
			} else {
				$valor = explode(" ", $valor);
				$valor[0] = $this->formataData($valor[0]);

				$valor = "'" . $valor[0] . " " . $valor[1] . "'";

				return $valor;
			}
		} elseif ($this->verificaTipo($tabela, $campo) == "LONG") {
			$valor = str_replace("R$", "", $valor);
			$valor = trim($valor);
			$valor = str_replace(".", "", $valor);
			$valor = str_replace(",", ".", $valor);

			return $valor;
		} else {
			$valor = str_replace("R$", "", $valor);
			$valor = trim($valor);
			$valor = str_replace(".", "", $valor);
			$valor = str_replace(",", ".", $valor);

			return $valor;
		}
	}

	private
	function validaHora(
		$tempo
	)
	{
		list($hora, $minuto) = explode(':', $tempo);

		if ($hora > -1 && $hora < 24 && $minuto > -1 && $minuto < 60) {
			return true;
		}
	}

	private
	function validaData(
		$data
	)
	{
		$data = split("[-,/]", $data);
		if (!checkdate($data[1], $data[0], $data[2]) and !checkdate($data[1], $data[2], $data[0])) {
			return false;
		}

		return true;
	}

	public
	function formataData(
		$data
	)
	{
		if (empty($data)) {
			return $data;
		}

		if ($this->validaData($data)) {
			return implode(
				!strstr($data, '/') ? "/" : "-",
				array_reverse(explode(!strstr($data, '/') ? "-" : "/", $data))
			);
		}
	}

//Formata data para as views sem Hora [Ex.: 01/10/2011]
	public
	function formataDataView(
		$data
	)
	{
		$data = explode(" ", $data);
		if ($this->validaData($data[0])) {
			return implode(
				!strstr($data[0], '/') ? "/" : "-",
				array_reverse(explode(!strstr($data[0], '/') ? "-" : "/", $data[0]))
			);
		}
	}

//Formata data para as views com Hora [Ex.: 01/10/2011 00:00:00]
	public
	function formataDataHoraView(
		$data
	)
	{
		$arrData = explode(" ", $data);
		$hora = $arrData[1];
		if ($this->validaData($arrData[0])) {
			$data = implode(
				!strstr($arrData[0], '/') ? "/" : "-",
				array_reverse(explode(!strstr($arrData[0], '/') ? "-" : "/", $arrData[0]))
			);
		}

		$strDataHora = $data . " " . $hora;

		return $strDataHora;
	}

	private
	function verificaTipo(
		$tabela,
		$campo
	)
	{
		if (array_key_exists($campo, $GLOBALS['arr'])) {
			return $GLOBALS['arr'][$campo];
		} else {
			$coluna = $this->query($this->seleciona($tabela, $campo, false, 1));
			if ($coluna !== false) {
				$resultado = $coluna->getColumnMeta(0);
				$GLOBALS['arr'][$campo] = $resultado["native_type"];

				return $resultado["native_type"];
			}
		}

//        $coluna = $this->query($this->seleciona($tabela,$campo,false,1));
//
//        if($coluna!==false)
//        {
//            $resultado = $coluna->getColumnMeta(0);
//            return $resultado["native_type"];
//        }
	}

	public
	function retiraMascara(
		$valor
	)
	{
		$valor = str_replace('/', "", $valor);
		$valor = str_replace('_', "", $valor);
		$valor = str_replace('-', "", $valor);
		$valor = str_replace('|', "", $valor);
		$valor = str_replace('.', "", $valor);
		$valor = str_replace(',', "", $valor);
		$valor = str_replace(':', "", $valor);
		$valor = str_replace(':', "", $valor);
		$valor = trim($valor);

		return $valor;
	}

	private
	function array_push_associative(
		&$arr
	)
	{
		$args = func_get_args();
		foreach ($args as $arg) {
			if (is_array($arg)) {
				foreach ($arg as $key => $value) {
					$arr[$key] = $value;
					$ret++;
				}
			} else {
				$arr[$arg] = "";
			}
		}

		return $ret;

	}

	public
	function retiraMascaraArray(
		$array,
		$tabela
	)
	{

		$retorno = array();

		foreach ($array as $campo => $valor) {

			if (($this->verificaTipo($tabela, $campo) != "DATETIME") &&
				($this->verificaTipo($tabela, $campo) != "DATE") &&
				($this->verificaTipo($tabela, $campo) != "NEWDECIMAL")
			) {

				$valor = str_replace('/', "", $valor);
				$valor = str_replace('_', "", $valor);
				$valor = str_replace('-', "", $valor);
				$valor = str_replace('|', "", $valor);
				$valor = str_replace('.', "", $valor);
				$valor = str_replace(',', "", $valor);
				$valor = str_replace(':', "", $valor);
				$valor = str_replace(':', "", $valor);
				$valor = str_replace(')', "", $valor);
				$valor = str_replace('(', "", $valor);
				$valor = trim($valor);
			}

			$arr = array($campo => $valor);

			$this->array_push_associative($retorno, $arr);

		}

		return $retorno;
	}


	/**************************************************************************
	 * Intranet - DAVÓ SUPERMERCADOS
	 * Criado em: 17/04/2012 por Rodrigo Alfieri
	 * Descrição: esta função recebe um valor numérico e retorna uma
	 *           string contendo o valor de entrada por extenso
	 * Entrada:  $valor (formato que a função number_format entenda :)
	 * Origens:  string com $valor por extenso
	 **************************************************************************
	 */

	function retornaValorPorExtenso($valor = 0)
	{

		$singular = array(
			"centavo"
			,
			"real"
			,
			"mil"
			,
			"milhão"
			,
			"bilhão"
			,
			"trilhão"
			,
			"quatrilhão",
		);

		$plural = array(
			"centavos"
			,
			"reais"
			,
			"mil"
			,
			"milhões"
			,
			"bilhões"
			,
			"trilhões"
			,
			"quatrilhões",
		);

		$c = array(
			""
			,
			"cem"
			,
			"duzentos"
			,
			"trezentos"
			,
			"quatrocentos"
			,
			"quinhentos"
			,
			"seiscentos"
			,
			"setecentos"
			,
			"oitocentos"
			,
			"novecentos",
		);

		$d = array(
			""
			,
			"dez"
			,
			"vinte"
			,
			"trinta"
			,
			"quarenta"
			,
			"cinquenta"
			,
			"sessenta"
			,
			"setenta"
			,
			"oitenta"
			,
			"noventa",
		);

		$d10 = array(
			"dez"
			,
			"onze"
			,
			"doze"
			,
			"treze"
			,
			"quatorze"
			,
			"quinze"
			,
			"dezesseis"
			,
			"dezesete"
			,
			"dezoito"
			,
			"dezenove",
		);

		$u = array(
			""
			,
			"um"
			,
			"dois"
			,
			"três"
			,
			"quatro"
			,
			"cinco"
			,
			"seis"
			,
			"sete"
			,
			"oito"
			,
			"nove",
		);

		$z = 0;

		$valor = str_replace(".", "", $valor);
		$valor = str_replace(",", ".", $valor);

		$valor = number_format($valor, 2, ".", ".");
		$inteiro = explode(".", $valor);
		for ($i = 0; $i < count($inteiro); $i++) {
			for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++) {
				$inteiro[$i] = "0" . $inteiro[$i];
			}
		}

		// $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
		$fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
		for ($i = 0; $i < count($inteiro); $i++) {
			$valor = $inteiro[$i];
			$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
			$rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
			$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

			$r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
			$t = count($inteiro) - 1 - $i;
			$r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
			if ($valor == "000") {
				$z++;
			} elseif ($z > 0) {
				$z--;
			}
			if (($t == 1) && ($z > 0) && ($inteiro[0] > 0)) {
				$r .= (($z > 1) ? " de " : "") . $plural[$t];
			}
			if ($r) {
				$rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
			}
		}

		return ($rt ? $rt : "zero");
	}

	public
	function dateDiff(
		$sDataInicial,
		$sDataFinal
	)
	{
		$sDataI = explode("-", $sDataInicial);
		$sDataF = explode("-", $sDataFinal);

		$nDataInicial = mktime(0, 0, 0, $sDataI[1], $sDataI[0], $sDataI[2]);
		$nDataFinal = mktime(0, 0, 0, $sDataF[1], $sDataF[0], $sDataF[2]);

		return ($nDataInicial > $nDataFinal) ?
			floor(($nDataInicial - $nDataFinal) / 86400) : floor(($nDataFinal - $nDataInicial) / 86400);
	}

	function calculaDigitoMod11($NumDado, $NumDig, $LimMult)
	{
		$Dado = $NumDado;
		for ($n = 1; $n <= $NumDig; $n++) {
			$Soma = 0;
			$Mult = 2;

			for ($i = strlen($Dado) - 1; $i >= 0; $i--) {
				$Soma += $Mult * intval(substr($Dado, $i, 1));
				if (++$Mult > $LimMult) {
					$Mult = 2;
				}
			}

			$Dado .= strval(fmod(fmod(($Soma * 10), 11), 10));
		}

		return substr($Dado, strlen($Dado) - $NumDig);
	}


//Gera $superArray para criar o menu
	public
	function menu()
	{
		$user = $_SESSION['user'];

		$superArray = array();
		$sql = "SELECT DISTINCT T1.T000_codigo
                               , T1.T000_pai
                               , T1.T000_nome
                            FROM t000_estrutura    T1
                            JOIN t000_t001 T0001 ON T0001.T000_codigo = T1.T000_codigo
                                                AND T0001.T001_login = '$user'
                           WHERE T000_pai          IS NULL";

		$menu = $this->query($sql);

		foreach ($menu as $campos => $valores) {

			$sql2 = "SELECT * FROM t000_estrutura T1 WHERE T000_pai = " . $valores['T000_codigo'];

			$this->query($sql2)->fetchAll(PDO::FETCH_ASSOC);

			$superArray[$valores['T000_nome']] = $this->tem_filho($valores['T000_codigo']);
		}

		return $superArray;
	}

//Função para verificar os sub-menus
	public
	function tem_filho(
		$id_menu
	)
	{

		$user = $_SESSION['user'];

		$superArray = array();
		$sql = "SELECT T1.*
                           FROM t000_estrutura T1
                           JOIN t000_t001 T0001 ON T0001.T000_codigo = T1.T000_codigo
                                               AND T0001.T001_login = '$user'
                          WHERE T000_pai = $id_menu";

		$menu = $this->query($sql)->fetchAll(PDO::FETCH_ASSOC);
		if ($menu) {
			foreach ($menu as $campos => $valores) {
				$superArray[$valores['T000_nome']] = $this->tem_filho($valores['T000_codigo']);
			}

			return $superArray;
		} else {
			return $id_menu;
		}
	}

	public
	function retornaFormatoCodigo(
		$codigo
	)
	{
		if (!empty($codigo)) {
			$codigo = str_pad($codigo, 3, "0", STR_PAD_LEFT);
		}

		$formato = $codigo;

		return $formato;
	}

	public
	function preencheBranco(
		$string,
		$qtdeCampo,
		$posicao
	)
	{
		if (!empty($string)) {
			if ($posicao == 0) {
				$string = str_pad($string, $qtdeCampo, " ", STR_PAD_LEFT);
			}
			if ($posicao == 1) {
				$string = str_pad($string, $qtdeCampo, " ", STR_PAD_RIGHT);
			}
		}

		$formato = $string;

		return $formato;
	}

	public
	function retornaFormatoCodigoNome(
		$codigo,
		$nome
	)
	{
		if (!empty($codigo)) {
			$separador = "-";
			$codigo = str_pad($codigo, 3, "0", STR_PAD_LEFT);
		}

		$formato = $codigo . $separador . $nome;

		return $formato;
	}

	public
	function mostraMensagem(
		$tipo,
		$texto
	)
	{

		$_SESSION['msg']['status'] = true;
		$_SESSION['msg']['tipo'] = $tipo;
		$_SESSION['msg']['texto'] = $texto;

	}

	public
	function criaArquivoControllers(
		$arquivo
	)
	{
		//NÃO MEXER NO INDENTAMENTO

		$pulaLinha = "\n";
		$conteudo = '
<?php
class ' . $arquivo . ' extends controllers
    {
        public function index($tipo)
            {
                home::execute($tipo);
            }
    }

?>';

		return $conteudo;
	}

	public
	function criaArquivoModels(
		$arquivo
	)
	{

		//NÃO MEXER NO INDENTAMENTO
		$data = date("d/m/Y");
		$pulaLinha = "\n";
		$conteudo =
			'<?php

                /**************************************************************************/
                /* Criado em: ' . $data . ' por ' . USER . '                              */
    /* Descrição: Classe para executar as Querys do Programa ' . $arquivo . '            */
    /**************************************************************************/    

    class models_' . $arquivo . ' extends models
    {                                                                               

        public function __construct()                                               
        {                                                                           
            $conn = "";                                                               
            parent::__construct($conn);                                             
        }                                                                           

        public function inserir($tabela,$campos)                                    
        {                                                                           
            $insere = $this->exec($this->insere($tabela, $campos));                     

            //Mensagem                                                              
            if($insere)                                                             
                $this->mostraMensagem("", "Gravado com Sucesso!");                  
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível gravar!");             

            return $insere;                                                         
        }                                                                           

        public function atualizar($tabela,$campos,$delim)                           
        {                                                                           
            $conn = "";                                                             

            $altera = $this->exec($this->atualiza($tabela, $campos, $delim));       

            //Mensagem                                                              
            if($altera)                                                             
                $this->mostraMensagem("", "Alterado com Sucesso!");                 
            else                                                                    
                $this->mostraMensagem("e", "Não foi possível alterar!");            

            return $altera;                                                         
        }                                                                           

        public function excluir($tabela, $delim)                                    
        {                                                                           
            $exclui = $this->exec($this->exclui($tabela, $delim));                      

            return $exclui;                                                         
        }                                                                           


    }                                                                               
?>';

		return $conteudo;
	}

	public
	function criaArquivoHomeViews()
	{
		$pulaLinha = "\n";
		$conteudo = "<?php echo 'Em Manutenção!!!'?>";

		return $conteudo;
	}

	public
	function retornaEstados()
	{
		$sql = "  SELECT T09.T009_codigo  CodigoEstado
                            , T09.T009_uf      UfEstado
                            , T09.T009_nome    NomeEstado
                         FROM t009_estado T09
                     ORDER BY 2";

		return $this->query($sql);
	}

	public
	function retornaTpPessoa()
	{
		$sql = "  SELECT T10.T010_codigo  CodigoTpPessoa
                            , T10.T010_tipo    TipoTpPessoa
                        FROM t010_tp_pessoa T10";

		return $this->query($sql);
	}

	public
	function retornaQtdeDados(
		$dados
	)
	{
		foreach ($dados as $campos => $valores) {
			return count($valores) ? true : false;
		}
	}

	public
	function retornaUltimoDiaMes(
		$data = ""
	)
	{
		if ($data) {
			list($dia, $mes, $ano) = explode("/", $data);
		} else {
			list($dia, $mes, $ano) = explode("/", date("d/m/Y"));
		}

		return date("t", mktime(0, 0, 0, $mes, $dia, $ano));
	}

	public function comboFornecedor(
		$codigoFornecedor,
		$r
	)
	{
		$sql = "  SELECT T04.T004_codigo           CodigoFornecedor
                            , T04.T004_razao_social     RazaoFornecedor
                         FROM t004_fornecedor T04
                        WHERE 1 = 1";

//        if(!empty($codigoFornecedor))
//            $sql    .=  " AND T04.T004_codigo = $codigoFornecedor";

		$dadosFornecedor = $this->query($sql);

		$strSelect1 = "";

		if (empty($codigoFornecedor)) {
			$strSelect1 = "selected";
		}

		$html = "";

		if ($r) {
			$required = "class='validate[required]'";
		} else {
			$required = "";
		}

		$html .= "    <select name='T004_codigo' $required>";
		$html .= "      <option value='null' $strSelect1>Selecione...</option>";
		foreach ($dadosFornecedor as $cp => $vl) {
			if ($vl['CodigoFornecedor'] == $codigoFornecedor) {
				$strSelect2 = "selected";
			} else {
				$strSelect2 = "";
			}
			$html .= "        <option value=" . $vl['CodigoFornecedor'] . " $strSelect2>" . $this->retornaFormatoCodigoNome(
					$vl['CodigoFornecedor'],
					$vl['RazaoFornecedor']
				) . "</option>";
		}
		$html .= "    </select>";

		return $html;
	}

//    public function comboCliente($c, $codigoCliente)
//    {
//        $sql    =   "  SELECT T02.T002_codigo       CodigoCliente
//                            , T02.T002_nome         NomeCliente
//                         FROM t002_cliente T02
//                        WHERE 1 = 1";
//        
////        if(!empty($codigoCliente))
////            $sql    .=  " AND T02.T002_codigo =  $codigoCliente";
//        
//        $strSelect1 =   "";
//        
//        if (empty($codigoCliente))
//            $strSelect1 =   "selected";    
//        
//        $dadosCliente   =   $this->query($sql);
//        
//        $html   =   "";
//                        
//        $html   .=   "    <select name='T002_codigo' class='validate[required]'>";
//        $html   .=   "      <option value='' $strSelect1>Selecione...</option>";
//        foreach($dadosCliente as $cp => $vl)            
//        {
//            if($vl['CodigoCliente']==$codigoCliente)
//                $strSelect2 =   "selected";
//            else
//                $strSelect2 =   "";
//            
//            if(!$c)
//                $html   .=   "        <option value=".$vl['CodigoCliente']." $strSelect2>".$this->retornaFormatoCodigoNome($vl['CodigoCliente'],$vl['NomeCliente'])."</option>";
//            else
//                $html   .=   "        <option value=".$vl['CodigoCliente']." $strSelect2>".$vl['NomeCliente']."</option>";
//        }
//        $html   .=   "    </select>";
//        
//        return $html;
//    }

	public
	function inputCliente(
		$codigoCliente,
		$c = false
	)
	{
		$sql = "  SELECT T02.T002_codigo       CodigoCliente
                            , T02.T002_nome         NomeCliente
                         FROM t002_cliente T02
                        WHERE T02.T002_codigo =  $codigoCliente
                          AND T02.T002_status =  1";

		$dadosCliente = $this->query($sql);

		$html = "";
		$input = "";

		if ($c) //Inclui Classe de Obrigatório
		{
			$c = "validate[required] text-input";
		}

		foreach ($dadosCliente as $cp => $vl) {
			$input .= "        <input type='hidden' name='T002_codigo'  value='" . $vl['CodigoCliente'] . "' id='auxClientes'/>";
			$input .= "        <input type='text' name='Clientes'  value='" . $this->retornaFormatoCodigoNome(
					$vl['CodigoCliente'],
					$vl['NomeCliente']
				) . "' class='campoClientes $c' onclick='this.select()' />";
		}

		if (empty($input)) {
			$html .= "   <input type='hidden' name='T002_codigo'  id='auxClientes'/>";
			$html .= "   <input type='text' name='Clientes'  class='campoClientes $c' onclick='this.select()'/>";
		} else {
			$html = $input;
		}

		return $html;
	}

	public
	function comboEstrutura(
		$codigoEstrutura
	)
	{
		$sql = "  SELECT T00.t000_codigo       CodigoEstrutura
                            , T00.t000_nome         NomeEstrutura
                         FROM t000_estrutura T00
                        WHERE 1 = 1";

		$strSelect1 = "";

		if (empty($codigoEstrutura)) {
			$strSelect1 = "selected";
		}

		$dadosEstrutura = $this->query($sql);

		$html = "";

		$html .= "    <select name='t000_codigo' class='validate[required]'>";
		$html .= "      <option value='' $strSelect1>Selecione...</option>";
		foreach ($dadosEstrutura as $cp => $vl) {
			if ($vl['CodigoEstrutura'] == $codigoEstrutura) {
				$strSelect2 = "selected";
			} else {
				$strSelect2 = "";
			}

			$html .= "        <option value=" . $vl['CodigoEstrutura'] . " $strSelect2>" . $this->retornaFormatoCodigoNome(
					$vl['CodigoEstrutura'],
					$vl['NomeEstrutura']
				) . "</option>";
		}
		$html .= "    </select>";

		return $html;
	}

	public
	function comboUsuario(
		$login
	)
	{
		$sql = "  SELECT T01.t001_login        LoginUsuario
                            , T01.T001_nome         NomeUsuario
                         FROM t001_usuario T01
                        WHERE 1 = 1";

		$strSelect1 = "";

		if (empty($login)) {
			$strSelect1 = "selected";
		}

		$dadosUsuario = $this->query($sql);

		$html = "";

		$html .= "    <select name='t001_login' class='validate[required]'>";
		$html .= "      <option value='' $strSelect1>Selecione...</option>";
		foreach ($dadosUsuario as $cp => $vl) {
			if ($vl['LoginUsuario'] == $login) {
				$strSelect2 = "selected";
			} else {
				$strSelect2 = "";
			}

			$html .= "        <option value=" . $vl['LoginUsuario'] . " $strSelect2>" . $this->retornaFormatoCodigoNome(
					$vl['LoginUsuario'],
					$vl['NomeUsuario']
				) . "</option>";
		}
		$html .= "    </select>";

		return $html;
	}

	public function debitaContaCliente(
		$codigoCliente,
		$valor
	)
	{
		$sql = " SELECT T02.T002_saldo SaldoCliente
                        FROM t002_cliente T02
                       WHERE T002_codigo = $codigoCliente";

		$arrSaldoAtual = $this->query($sql)->fetchAll(PDO::FETCH_COLUMN);

		$tabela = "t002_cliente";
		$saldoAtual = $arrSaldoAtual[0];

		$saldoFinal = (float)$saldoAtual - (float)$valor;

		$saldoFinal = $this->formataMoeda($saldoFinal);
		$campo = array("T002_saldo" => $saldoFinal);
		$delim = " T002_codigo =   $codigoCliente";
		$atualiza = $this->exec($this->atualiza($tabela, $campo, $delim));

		if ($atualiza) {
			$this->mostraMensagem("", "Saldo do Cliente Atualizado!");
		} else {
			$this->mostraMensagem("e", "Erro durante a Atualização de Saldo do Cliente!");
		}
	}

	public function creditaContaCliente(
		$codigoCliente,
		$valor
	)
	{
		$sql = " SELECT T02.T002_saldo SaldoCliente
                        FROM t002_cliente T02
                       WHERE T002_codigo = $codigoCliente";

		$arrSaldoAtual = $this->query($sql)->fetchAll(PDO::FETCH_COLUMN);

		$tabela = "t002_cliente";
		$saldoAtual = $arrSaldoAtual[0];
		$saldoFinal = $saldoAtual + $valor;

		$saldoFinal = $this->formataMoeda($saldoFinal);

		$campo = array("T002_saldo" => $saldoFinal);
		$delim = " T002_codigo =   $codigoCliente";
		$atualiza = $this->exec($this->atualiza($tabela, $campo, $delim));

		if ($atualiza) {
			$this->mostraMensagem("", "Saldo do Cliente Atualizado!");
		} else {
			$this->mostraMensagem("e", "Erro durante a Atualização de Saldo do Cliente!");
		}
	}

	public
	function verificaPermissaoUsuarioPrograma(
		$codigoPrograma,
		$user
	)
	{
		$sql = " SELECT *
                        FROM t000_t001 T0001
                       WHERE T0001.t001_login = '$user'
                         AND T0001.t000_codigo= $codigoPrograma";

		return $this->query($sql)->rowCount();

	}

	public
	function retornaValorNegativo(
		$valor
	)
	{
		$valor = str_replace("R$", "", $valor);
		$valor = trim($valor);
		$valor = str_replace(".", "", $valor);
		$valor = str_replace(",", ".", $valor);

		return -$valor;
	}

	public
	function formataMoeda(
		$valor
	)
	{
		return $valor = 'R$ ' . number_format($valor, 2, ',', '.'); // retorna R$100.000,50
	}

	private
	function retornaEstoqueAtualProduto(
		$codigoProduto
	)
	{
		$sql = " SELECT T03.T003_qtd_estoque
                        FROM t003_produto T03
                       WHERE T003_codigo = $codigoProduto";

		$estoqueAtual = $this->query($sql)->fetchAll(PDO::FETCH_COLUMN);

		return $estoqueAtual[0];
	}


	public
	function debitaEstoque(
		$codigoProduto,
		$qtde
	)
	{
		$estoqueProduto = $this->retornaEstoqueAtualProduto($codigoProduto);

		$estoqueAtual = $estoqueProduto - $qtde;

		$tabela = "t003_produto";

		$campos = array("T003_qtd_estoque" => $estoqueAtual);

		$delimitador = " T003_codigo   =   $codigoProduto";

		$this->exec($this->atualiza($tabela, $campos, $delimitador));
	}

	public
	function creditaEstoque(
		$codigoProduto,
		$qtde
	)
	{
		$estoqueProduto = $this->retornaEstoqueAtualProduto($codigoProduto);

		$estoqueAtual = $estoqueProduto + $qtde;

		$tabela = "t003_produto";

		$campos = array("T003_qtd_estoque" => $estoqueAtual);

		$delimitador = " T003_codigo   =   $codigoProduto";

		$this->exec($this->atualiza($tabela, $campos, $delimitador));
	}

	public
	function limiteLinhas()
	{
		$sql = " SELECT T07.T007_valor       ValorParametro
                        FROM t007_parametro T07
                       WHERE T07.T007_codigo = 3";

		$parametro = $this->query($sql)->fetchAll(PDO::FETCH_COLUMN);

		return (int)$parametro[0];
	}

	public
	function comboQtdeRegistros(
		$limit
	)
	{
		$html = "";
		$selected = "";

		if (empty($limit)) {
			$selected = "selected";
		} else {
			$selected = "";
		}

		$html .= "<select name='QtdeRegistros'>";

		$html .= "<option value='' $selected>...</option>";

		for ($i = 15; $i <= 60; $i = $i + 15) {
			if ($limit == $i) {
				$selected = "selected";
			} else {
				$selected = "";
			}

			$html .= "<option value='$i' $selected>$i</option>";
		}

		if ($limit == "*") {
			$selected = "selected";
		} else {
			$selected = "";
		}

		$html .= "<option value='*' $selected>Todos</option>";


		$html .= "</select>";

		return $html;
	}

	public
	function arquivoPedido(
		$codigoPedido
	)
	{
		$codigoPedido = 19;


		$codigoPedido = str_pad($codigoPedido, 4, "0", STR_PAD_LEFT);

		$tmpDir = "tmp"; //Diretório de Arquivos Temporários

		//Verifica a Existencia dos Diretórios
		if (!is_dir($tmpDir)) {
			mkdir($tmpDir);
		}

		$sql = "  SELECT T10.T010_pedido                                   CodigoPedido
                            , T10.T002_codigo                                   CodigoCliente    
                            , T02.T002_nome                                     NomeCliente
                            , date_format(T10.T010_data,'%d/%m/%Y %H:%i:%s')    DataPedido
                            , T10.T010_valor                                    ValorPedido
                         FROM t010_pedido T10
                         JOIN t002_cliente T02 ON T02.T002_codigo = T10.T002_codigo
                        WHERE T010_pedido = $codigoPedido";

		$dadosPedido = $this->query($sql);

		//Cabecalho Pedido
		foreach ($dadosPedido as $campos => $valores) {
			$cliente = $this->retornaFormatoCodigoNome($valores['CodigoCliente'], $valores['NomeCliente']);
			$dataPedido = $valores['DataPedido'];
			$valorPedido = $valores['ValorPedido'];

			file_put_contents("$tmpDir/PED_$codigoPedido.txt", "$codigoPedido|$dataPedido|$cliente|$valorPedido\n");
		}

		$sql = "  SELECT T11.T003_codigo      CodigoProduto
                            , T03.T003_descricao   DescricaoProduto
                            , T11.T011_qtde        QtdeProduto
                            , T11.T011_subtotal    SubtotalProduto
                         FROM t011_pedido_detalhe T11
                         JOIN t010_pedido T10 ON T10.T010_pedido = T11.T010_pedido
                         JOIN t002_cliente T02 ON T02.T002_codigo = T10.T002_codigo
                         JOIN t003_produto T03 ON T11.T003_codigo = T03.T003_codigo
                        WHERE T10.T010_pedido  = $codigoPedido";

		$produtosPedido = $this->query($sql);

		//Corpo
		foreach ($produtosPedido as $campos => $valores) {
			$produto = $this->retornaFormatoCodigoNome($valores['CodigoProduto'], $valores['DescricaoProduto']);
			$qtde = $valores['QtdeProduto'];
			$subtotal = $valores['SubtotalProduto'];

			file_put_contents("$tmpDir/P$codigoPedido.txt", "$produto|$qtde|$subtotal\n", FILE_APPEND);
		}

	}

	public
	function retornaStrImpressaoPedido(
		$codigoPedido
	)
	{
		$sql = "  SELECT T10.T010_pedido    CodigoPedido
                                , T10.T002_codigo    CodigoCliente
                                , T02.T002_nome      NomeCliente
                                , T10.T010_data      DataPedido
                                , T10.T010_valor     ValorPedido
                             FROM t010_pedido T10
                             JOIN t002_cliente T02 ON T02.T002_codigo = T10.T002_codigo
                            WHERE T10.T010_pedido = $codigoPedido";

		$dadosPedido = $this->query($sql);

		$strCabec = "";

		foreach ($dadosPedido as $campos => $valores) {
			$codigoPedido = $valores[''];
			$strCliente = $this->retornaFormatoCodigoNome($valores[''], $valores['']);
			$codigoPedido = $valores[''];
			$codigoPedido = $valores[''];

		}

	}

	private
	function chamaFuncaoJs(
		$funcao
	)
	{
		echo "<script>
                    $(function(){
                                    $funcao
                    });
              </script>";
	}

	public
	function enviaImpressora(
		$comando,
		$string = null,
		$nroLinha = null
	)
	{
//		echo "\n".$comando."\n\n";
//		echo $string."\n\n";
//		echo $nroLinha."\n\n";
		switch ($comando) {
			case 0: //Comando para Impressao de uma String
				$this->chamaFuncaoJs("Imp_ImprimeString('" . $string . "',$nroLinha)");
				break;
			case 1: //Comando para pula Linha
				$this->chamaFuncaoJs("Imp_CorteParcial()");
				break;
			case 2: //Corte Total Papel
				$this->chamaFuncaoJs("Imp_CorteTotal()");
				break;
			case 3: //Corte Total Papel
				$this->chamaFuncaoJs("ImprimeLabel()");
				break;
		}

	}

	public
	function semCaracterEspecial(
		$string
	)
	{

		$string = preg_replace(
			"/[^a-zA-Z0-9_.]/",
			"",
			strtr($string, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_")
		);

		$string = str_replace("_", " ", $string);

		return ($string);
	}

	/**
	 * Formata um numero em notacao de moeda, assim como a funcao money_format do PHP
	 * @author Rubens Takiguti Ribeiro
	 * @see http://php.net/manual/en/function.money-format.php
	 *
	 * @param string $formato Formato aceito por money_format
	 * @param float $valor Valor monetario
	 *
	 * @return string Valor formatado
	 */
	function my_money_format($formato, $valor)
	{

		// Se a funcao money_format esta disponivel: usa-la
		if (function_exists('money_format')) {
			return money_format($formato, $valor);
		}

		// Se nenhuma localidade foi definida, formatar com number_format
		if (setlocale(LC_MONETARY, 0) == 'C') {
			return number_format($valor, 2);
		}

		// Obter dados da localidade
		$locale = localeconv();

		// Extraindo opcoes do formato
		$regex = '/^' . // Inicio da Expressao
			'%' . // Caractere %
			'(?:' . // Inicio das Flags opcionais
			'\=([\w\040])' . // Flag =f
			'|' .
			'([\^])' . // Flag ^
			'|' .
			'(\+|\()' . // Flag + ou (
			'|' .
			'(!)' . // Flag !
			'|' .
			'(-)' . // Flag -
			')*' . // Fim das flags opcionais
			'(?:([\d]+)?)' . // W  Largura de campos
			'(?:#([\d]+))?' . // #n Precisao esquerda
			'(?:\.([\d]+))?' . // .p Precisao direita
			'([in%])' . // Caractere de conversao
			'$/'; // Fim da Expressao

		if (!preg_match($regex, $formato, $matches)) {
			trigger_error('Formato invalido: ' . $formato, E_USER_WARNING);

			return $valor;
		}

		// Recolhendo opcoes do formato
		$opcoes = array(
			'preenchimento'   => ($matches[1] !== '') ? $matches[1] : ' ',
			'nao_agrupar'     => ($matches[2] == '^'),
			'usar_sinal'      => ($matches[3] == '+'),
			'usar_parenteses' => ($matches[3] == '('),
			'ignorar_simbolo' => ($matches[4] == '!'),
			'alinhamento_esq' => ($matches[5] == '-'),
			'largura_campo'   => ($matches[6] !== '') ? (int)$matches[6] : 0,
			'precisao_esq'    => ($matches[7] !== '') ? (int)$matches[7] : false,
			'precisao_dir'    => ($matches[8] !== '') ? (int)$matches[8] : $locale['int_frac_digits'],
			'conversao'       => $matches[9],
		);

		// Sobrescrever $locale
		if ($opcoes['usar_sinal'] && $locale['n_sign_posn'] == 0) {
			$locale['n_sign_posn'] = 1;
		} elseif ($opcoes['usar_parenteses']) {
			$locale['n_sign_posn'] = 0;
		}
		if ($opcoes['precisao_dir']) {
			$locale['frac_digits'] = $opcoes['precisao_dir'];
		}
		if ($opcoes['nao_agrupar']) {
			$locale['mon_thousands_sep'] = '';
		}

		// Processar formatacao
		$tipo_sinal = $valor >= 0 ? 'p' : 'n';
		if ($opcoes['ignorar_simbolo']) {
			$simbolo = '';
		} else {
			$simbolo = $opcoes['conversao'] == 'n' ? $locale['currency_symbol']
				: $locale['int_curr_symbol'];
		}
		$numero = number_format(
			abs($valor),
			$locale['frac_digits'],
			$locale['mon_decimal_point'],
			$locale['mon_thousands_sep']
		);

		/*
		//TODO: dar suporte a todas as flags
			list($inteiro, $fracao) = explode($locale['mon_decimal_point'], $numero);
			$tam_inteiro = strlen($inteiro);
			if ($opcoes['precisao_esq'] && $tam_inteiro < $opcoes['precisao_esq']) {
				$alinhamento = $opcoes['alinhamento_esq'] ? STR_PAD_RIGHT : STR_PAD_LEFT;
				$numero = str_pad($inteiro, $opcoes['precisao_esq'] - $tam_inteiro, $opcoes['preenchimento'], $alinhamento).
						  $locale['mon_decimal_point'].
						  $fracao;
			}
		*/

		$sinal = $valor >= 0 ? $locale['positive_sign'] : $locale['negative_sign'];
		$simbolo_antes = $locale[$tipo_sinal . '_cs_precedes'];

		// Espaco entre o simbolo e o numero
		$espaco1 = $locale[$tipo_sinal . '_sep_by_space'] == 1 ? ' ' : '';

		// Espaco entre o simbolo e o sinal
		$espaco2 = $locale[$tipo_sinal . '_sep_by_space'] == 2 ? ' ' : '';

		$formatado = '';
		switch ($locale[$tipo_sinal . '_sign_posn']) {
			case 0:
				if ($simbolo_antes) {
					$formatado = '(' . $simbolo . $espaco1 . $numero . ')';
				} else {
					$formatado = '(' . $numero . $espaco1 . $simbolo . ')';
				}
				break;
			case 1:
				if ($simbolo_antes) {
					$formatado = $sinal . $espaco2 . $simbolo . $espaco1 . $numero;
				} else {
					$formatado = $sinal . $numero . $espaco1 . $simbolo;
				}
				break;
			case 2:
				if ($simbolo_antes) {
					$formatado = $simbolo . $espaco1 . $numero . $sinal;
				} else {
					$formatado = $numero . $espaco1 . $simbolo . $espaco2 . $sinal;
				}
				break;
			case 3:
				if ($simbolo_antes) {
					$formatado = $sinal . $espaco2 . $simbolo . $espaco1 . $numero;
				} else {
					$formatado = $numero . $espaco1 . $sinal . $espaco2 . $simbolo;
				}
				break;
			case 4:
				if ($simbolo_antes) {
					$formatado = $simbolo . $espaco2 . $sinal . $espaco1 . $numero;
				} else {
					$formatado = $numero . $espaco1 . $simbolo . $espaco2 . $sinal;
				}
				break;
		}

		// Se a string nao tem o tamanho minimo
		if ($opcoes['largura_campo'] > 0 && strlen($formatado) < $opcoes['largura_campo']) {
			$alinhamento = $opcoes['alinhamento_esq'] ? STR_PAD_RIGHT : STR_PAD_LEFT;
			$formatado = str_pad($formatado, $opcoes['largura_campo'], $opcoes['preenchimento'], $alinhamento);
		}

		return $formatado;
	}

}

?>
