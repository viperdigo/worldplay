# Tabela T002_Cliente
SELECT CONCAT(
    'INSERT INTO customers (id, name, address, zipcode, phone, email, balance, created_at, updated_at, status) VALUES (',
    T002_codigo, ',"', T002_nome, '", "', T002_endereco, '", "', T002_cep, '", "', T002_telefone, '", null, ',
    T002_saldo, ', "', sysdate(), '", "', sysdate(), '", "', CASE T002_status
                                                             WHEN 0
                                                               THEN 'blocked'
                                                             ELSE 'activated' END, '");')
FROM t002_cliente;

# Tabela T010_pedido
SELECT CONCAT('INSERT INTO orders (id, fk_customer, amount, note, created_at, updated_at) VALUES (', T010_pedido, ', ',
              T002_codigo, ', ', T010_valor, ', null, "', T010_data, '","',sysdate(),'");')
FROM t010_pedido;

# Tabela T011_pedido_detalhe
SELECT CONCAT(
    'INSERT INTO order_items (fk_order, fk_product, sold_value, quantity, subtotal, created_at, updated_at) VALUES (',
    T010_pedido, ', ', T003_codigo, ', ', T011_valor_venda, ', ', T011_qtde, ', ', T011_subtotal, ', "', sysdate(),
    '", "', sysdate(), '");')
FROM t011_pedido_detalhe;

# Tabela T004_fornecedor
SELECT CONCAT('INSERT INTO suppliers (id, name, document, phone, created_at, updated_at) VALUES (', T004_codigo, ', "',
              T004_razao_social, '", "', T004_cnpj, '", "', T004_fone, '", "', sysdate(), '", "', sysdate(), '");')
FROM t004_fornecedor;

# Tabela T003_produto
SELECT CONCAT(
    "INSERT INTO products (id, fk_supplier, description, cost_value, sale_value, storage, type, created_at, updated_at) VALUES (",
    T003_codigo, ",  null , '", T003_descricao, "', ", T003_vl_custo, ", ", T003_vl_unit, ", ",
    T003_qtd_estoque, ", '", CASE T003_tipo
                             WHEN 1
                               THEN "simple"
                             WHEN 2
                               THEN "compound"
                             ELSE "cabinet" END, "', '", sysdate(), "', '", sysdate(), "');")
FROM t003_produto;

# Tabela T013_componente
SELECT CONCAT('INSERT INTO products_components (fk_product, fk_component, quantity, created_at, updated_at) VALUES ( ',
              T003_codigo, ', ', T003_componente, ', ', T013_qtd_componente, ', "', sysdate(), '", "', sysdate(), '");')
FROM t013_componente;

# Tabela Manutencao
SELECT CONCAT(
    'INSERT INTO maintenance_products (id, fk_customer, fk_product, quantity, status, note, created_at, updated_at) VALUES (',
    t16.T016_codigo, ', ', t16.T002_codigo, ', ', t17.T003_codigo, ', ', t17.T017_qtde, ' ,"', CASE t16.T016_status
                                                                                               WHEN 1
                                                                                                 THEN 'entered'
                                                                                               WHEN 2
                                                                                                 THEN 'sent'
                                                                                               WHEN 3
                                                                                                 THEN 'received'
                                                                                               WHEN 4
                                                                                                 THEN 'returned'
                                                                                               ELSE 'canceled' END,
    '", "', t17.T017_obs, '", "', t16.T016_dt_entrada, '", "',sysdate(),'");')
FROM t016_manutencao t16
  JOIN t017_manutencao_detalhe t17 ON t16.T016_codigo = t17.T016_codigo;

#Tabela Manutencao Detalhe
SELECT
  CONCAT('INSERT INTO maintenance_status_log (fk_maintenance, created_at, fk_user, status) VALUES (', SE.T016_codigo,
         ', "', SE.Data, '", 2, "', CASE SE.Status
                                    WHEN 1
                                      THEN 'entered'
                                    WHEN 2
                                      THEN 'sent'
                                    WHEN 3
                                      THEN 'received'
                                    WHEN 4
                                      THEN 'returned'
                                    ELSE 'canceled' END, '");')
FROM (
       SELECT
         T016_codigo,
         T002_codigo,
         T016_dt_entrada AS "Data",
         1               AS "Status"
       FROM t016_manutencao
       UNION ALL
       SELECT
         T016_codigo,
         T002_codigo,
         T016_dt_envio AS "Data",
         2             AS "Status"
       FROM t016_manutencao
       UNION ALL
       SELECT
         T016_codigo,
         T002_codigo,
         T016_dt_recebimento AS "Data",
         3                   AS "Status"
       FROM t016_manutencao
       UNION ALL
       SELECT
         T016_codigo,
         T002_codigo,
         T016_dt_devolucao AS "Data",
         4                 AS "Status"
       FROM t016_manutencao
       UNION ALL
       SELECT
         T016_codigo,
         T002_codigo,
         T016_dt_cancelamento AS "Data",
         5                    AS "Status"
       FROM t016_manutencao) SE;
