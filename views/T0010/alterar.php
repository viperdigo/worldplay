<?php
//Instancia Class
$obj            =   new models_T0010()                     ;

$codigoFornecedor  =   $_REQUEST['codigoFornecedor']       ;
    
//Dados dos Campos
$dados          =   $obj->retornaDados($codigoFornecedor)  ;

if(!empty($_POST))
{
    //Retira mascara
    $campos         =   $obj->retiraMascaraArray($_POST)        ;
    
    $tabela         =   "t004_fornecedor"                       ;
    
    $delimitador    =   "T004_codigo    =".$codigoFornecedor    ;
    
    $alterar =   $obj->atualizar($tabela, $campos, $delimitador);
    if ($alterar)
    {
        header("Location: ".ROUTER."home");
        exit;
    }    
}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">
        <?php foreach($dados    as $campos  =>  $vls){?>
        <div class="grid_7">
            <p>Razão Social*</p>
            <input type="text" name="T004_razao_social"         class="validate[required] text-input"       value="<?php echo $vls['RazaoFornecedor']?>"/>
        </div>
        
        <div class="grid_3">
            <p>CNPJ</p>
            <input type="text" name="T004_cnpj"                 class="text-input cnpj"                         value="<?php echo $vls['CnpjFornecedor']?>"/>
        </div>
        
        <div class="grid_3">
            <p>Telefone</p>
            <input type="text" name="T004_fone"                 class="text-input"                      value="<?php echo $vls['FoneFornecedor']?>"/>
        </div>
        
        <div class="clear"></div>

        <div class="grid_2">
            <input type="submit" value="Alterar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        <?php }?>
    </div>        
</form>