<?php
//Instancia Class
$obj    =   new models_T0015();


if(!empty($_POST))
{
    $tabela =   "t007_parametro";
    
    $nome       =   $_POST['T007_nome'];
    $descricao  =   $_POST['T007_descricao'];
    $valor      =   $_POST['T007_valor'];
    
    $campos     =   array( "T007_nome"       => $nome
                         , "T007_descricao"  => $descricao
                         , "T007_valor"      => $valor);
    
    
    $inseri =   $obj->inserir($tabela, $campos);
    
    if ($inseri)
    {
        header("Location: ".ROUTER."home");
        exit;
    }
    
}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">
        <div class="grid_7">
            <p>Nome*</p>
            <input type="text" name="T007_nome"         class="validate[required] text-input"           />                  
        </div>
        
        <div class="grid_3">
            <p>Descrição</p>
            <input type="text" name="T007_descricao"     class="text-input"                              />                  
        </div>
        
        <div class="grid_2">
            <p>Valor</p>
            <input type="text" name="T007_valor"          class="text-input valor "                      />                  
        </div>
        
        <div class="clear"></div>

        <div class="grid_2">
            <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        
    </div>        
</form>