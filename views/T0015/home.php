<?php
//Instancia 
$obj        =   new models_T0015()  ;

//Filtros
if (!empty($_POST))
{
    $codigoParametro    =   $_POST['T007_codigo']   ;                  
    $nomeParametro      =   $_POST['T007_nome']     ;   
    
    $limit          =   $_POST['QtdeRegistros']     ;
    
    $dados          =   $obj->retornaDados($codigoParametro, $nomeParametro, $limit);
}else 
{
    //Sem Filtro
    $dados          =   $obj->retornaDados();
}

//Variavel para verificar se existem dados de retorno para tabela não aparecer
$QtdeDados  =   0                   ;
?>
<div id="barra-botoes">
    <a href="#"                         class="ui-state-default ui-corner-all botoes-links abrirFiltros">   <span class="ui-icon ui-icon-carat-2-n-s">  </span>Filtros</a>
    <a href="<?php echo ROUTER;?>novo"  class="ui-state-default ui-corner-all botoes-links">                <span class="ui-icon ui-icon-plus">         </span>Novo</a>
    <hr>    
</div>    
<div id="barra-filtros">
    <form action="" method="post">        
        <div class="conteudo_16">
            <!-- CAMPOS DA BARRA DE FILTROS -->
                <div class="grid_3">
                    <label>Dinâmico</label>
                    <input type="text"                      class="filtroDinamico"  />                  
                </div>
            
                <div class="grid_1">
                    <label>Código</label>
                    <input type="text" name="T007_codigo"                           />                  
                </div>
            
                <div class="grid_1">
                    <label>Registros</label>
                    <?php echo $obj->comboQtdeRegistros($limit);?>
                </div>
            
                <div class="grid_2">
                    <input type="submit" value="Filtrar" class="ui-button ui-widget ui-state-default ui-corner-all botao" role="button" aria-disabled="false">                
                </div>
        </div>        
    </form>
</div>  
<div class="conteudo_16">
    <table class="tablesorter" cellspacing="1" cellpadding="0" border="0"> 
        <thead> 
            <tr> 
                <th>Parametro           </th> 
                <th>Valor               </th> 
                <th>Descrição           </th> 
                <th>Ações               </th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php   foreach($dados    as $campos => $valores)
                    {   $QtdeDados++; ?>
                        <tr> 
                            <td class="id"><?php echo $obj->retornaFormatoCodigoNome($valores['CodigoParametro'],$valores['NomeParametro']);?> </td> 
                            <td><?php echo $valores['ValorParametro'];?></td> 
                            <td><?php echo $valores['DescricaoParametro'];?></td> 
                            <td class="acoes">
                                <ul class="ui-widget ui-helper-clearfix" id="icons">
                                    <li title="Alterar" class="ui-state-default ui-corner-all"><a href="<?php echo ROUTER?>alterar&codigoParametro=<?php echo $valores['CodigoParametro'];?>"><span   class='ui-icon ui-icon-pencil'></span></a></li>
                                    <li title="Excluir" class="ui-state-default ui-corner-all excluir"><a href="#"><span class="ui-icon ui-icon-closethick"></span></li>
                                </ul>
                            </td> 
                        </tr>
            <?php   }  ?>
                    <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->    
            <?php if ($QtdeDados==0){    ?>        
                    <tr>
                        <td colspan="6" style="text-align: center">Não Existem Dados.</td>
                    </tr>    
            <?php }?>
        </tbody> 
    </table> 
</div>   
