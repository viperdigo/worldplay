<?php 

$obj    =   new models_T0016(); 

if (!empty($_POST))
{
    $codigoCliente  =   $_POST['T002_codigo'];
    $dataInicio     =   $_POST['T010_data'];
    $dataFim        =   $_POST['T010_dataF'];
    
    $dadosRelatorio    =   $obj->retornaDados($codigoCliente, $dataInicio, $dataFim);   
    $dadosContaCliente =   $obj->retornaDadosContaCliente($codigoCliente, $dataInicio, $dataFim);   
    
    foreach($dadosContaCliente as $campos   =>  $valores)
    {
        $saldoCliente      =   $valores['SaldoCliente'];
        $recebidoCliente   =   $valores['RecebidoCliente'];
    }
}

$cabecImpressao     =   "&codigoCliente=$codigoCliente&dataInicio=$dataInicio&dataFim=$dataFim";

?>
<div id="barra-botoes">
    <a href="#"                                                     class="ui-state-default ui-corner-all botoes-links abrirFiltros">   <span class="ui-icon ui-icon-carat-2-n-s">  </span>Filtros</a>
    <a href="<?php echo ROUTER;?>js.pdfImpressao<?php echo $cabecImpressao;?>" target="_blank"   class="ui-state-default ui-corner-all botoes-links">                <span class="ui-icon ui-icon-print">        </span>Imprimir</a>
    <hr>    
</div>    
<div id="barra-filtros">
    <form action="" method="post" class="validar">        
        <div class="conteudo_16">
            <!-- CAMPOS DA BARRA DE FILTROS -->

            <div class="grid_3">
                <label>Cliente *</label>
                <?php echo $obj->inputCliente($codigoCliente,1);?>
            </div>              
            
            <div class="grid_2">
                <label>Data Início</label>
                <input type="text" name="T010_data"       class="data"   value="<?php echo $dataInicio;?>"  />                  
            </div>

            <div class="grid_2">
                <label>Data Fim</label>
                <input type="text" name="T010_dataF"       class="data"  value="<?php echo $dataFim;?>"   />                  
            </div>          

            <div class="grid_2">
                <input type="submit" value="Filtrar" class="ui-button ui-widget ui-state-default ui-corner-all botao" role="button" aria-disabled="false">                
            </div>

        </div>        
    </form>
</div>
<div class="conteudo_16">
    <table class="tablesorter" cellspacing="1" cellpadding="0" border="0"> 
        <thead> 
            <tr> 
                <th>Tipo        </th> 
                <th>Data        </th> 
                <th>Produto     </th>
                <th>Quantidade  </th>
                <th>Valor       </th> 
                <th>Subtotal(débito)</th> 
                <th>Pagto(crédito)</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php   foreach($dadosRelatorio    as $campos => $valores)
                    {   $QtdeDados++;
                        if ( $valores["Tipo"]==2) {
							$totalPedidos += $valores['Valor'];
							$nomeTipo = 'Pedido';
							$valorPagamento = '';
						} else if ( $valores["Tipo"]==3) {
							$totalPedidoFornecedor += $valores['Valor'];
							$nomeTipo = 'Pedido Fornecedor';
							$valorPagamento = '';
						}else {
							$totalPagtos += $valores['Valor'];
							$nomeTipo = 'Pagamento';
							$valorPagamento = $obj->formataMoeda($valores['Valor']);
						}
                 
                    if ($valores['Tipo']==1 || $valores['Tipo'] == 3) {
						$corLetra = "#FF0000";

					}else {
						$corLetra = "#0000FF";

					}
                                                            
                    ?>
                        <tr>
                            <td style="color: <?php echo $corLetra;?>"><?php echo $nomeTipo;?></td>
                            <td style="color: <?php echo $corLetra;?>"><?php echo $obj->formataDataView($valores['Data']);?></td> 
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="color: <?php echo $corLetra;?>"><?php echo $valorPagamento;?></td>
                        </tr>
                        <?php if($valores['Tipo']!=1){
                            
                            if($valores['Tipo'] == 2) {
                                $dadosDetalhePedido =   $obj->detalhePedido($valores['CodigoPedido']);
                            }else{
								$dadosDetalhePedido =   $obj->detalhePedidoFornecedor($valores['CodigoPedido']);
                            };
                        foreach($dadosDetalhePedido as $cps => $vls){
                        ?>
                        <tr> 
                            <td><?php echo $vls[''];?></td> 
                            <td><?php echo $vls[''];?></td> 
                            <td><?php echo $obj->retornaFormatoCodigoNome($vls['CodigoProduto'],$vls['DescricaoProduto']);?></td> 
                            <td><?php echo $vls['QtdeProduto'];?></td> 
                            <td><?php echo $obj->formataMoeda($vls['ValorProduto']);?></td> 
                            <td><?php echo $obj->formataMoeda($vls['SubtotalProduto']);?></td> 
                            <td></td> 
                        </tr>                       
                        <?php   }}} ?>
                    <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->    
            <?php if ($QtdeDados==0){    ?>        
                    <tr>
                        <td colspan="7" style="text-align: center">Não Existem Dados.</td>
                    </tr>    
            <?php }else{  ?>
                    <tr>
                        <td colspan="6" style="text-align: right; font-weight: bold; color: #0000FF">Total Pedidos (de acordo com filtro acima)&nbsp;&nbsp;&nbsp;</td>
                        <td style="text-align: left; font-weight: bold; color: #0000FF"><?php echo $obj->formataMoeda($totalPedidos);?></td>
                    </tr>                     
                    <tr>
                        <td colspan="6" style="text-align: right; font-weight: bold; color:#FF0000">Total Pagtos (de acordo com filtro acima)&nbsp;&nbsp;&nbsp;</td>
                        <td style="text-align: left; font-weight: bold; color: #FF0000"><?php echo $obj->formataMoeda($totalPagtos);?></td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: right; font-weight: bold; color:#FF0000">Total Pedido Forn. (de acordo com filtro acima)&nbsp;&nbsp;&nbsp;</td>
                        <td style="text-align: left; font-weight: bold; color: #FF0000"><?php echo $obj->formataMoeda($totalPedidoFornecedor);?></td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: right; font-weight: bold;">Saldo Atual &nbsp;&nbsp;&nbsp;</td>
                        
                        <?php if($saldoCliente<0){                        
                            $corLetra   =   '#FF0000';
                        }else{
                            $corLetra   =   '#0000FF';
                        }
                        ?>
                        
                        <td style="text-align: left; font-weight: bold;color: <?php echo $corLetra;?>;"><?php echo $obj->formataMoeda($saldoCliente);?></td>
                    </tr>                                       
            <?php }?>        
        </tbody> 
    </table> 
</div>   