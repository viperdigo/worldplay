<?php

class PDF extends FPDF {
	//Current column
	var $col = 0;
	//Ordinate of column start
	var $y0;

	public function select() {
		$codigoCliente = $_GET['codigoCliente'];
		$dataInicio    = $_GET['dataInicio'];
		$dataFim       = $_GET['dataFim'];

		$obj = new models_T0016();

		$dados = $obj->retornaDados( $codigoCliente, $dataInicio, $dataFim );

		return $dados;

	}

	public function select2() {
		$codigoCliente = $_GET['codigoCliente'];
		$dataInicio    = $_GET['dataInicio'];
		$dataFim       = $_GET['dataFim'];

		$obj = new models_T0016();

		$dadosContaCliente = $obj->retornaDadosContaCliente( $codigoCliente, $dataInicio, $dataFim );

		return $dadosContaCliente;

	}

	public function Header() {

		$obj = new models_T0016();

		$codigoCliente = $_GET['codigoCliente'];

		$dadosCliente = $obj->retornaCliente( $codigoCliente );

		foreach ( $dadosCliente as $campos => $valores ) {
			$strCliente = $obj->retornaFormatoCodigoNome( $valores['CodigoCliente'], $valores['NomeCliente'] );
		}

		$this->SetFont( 'Arial', 'B', 20 );

		$this->Cell( 190, 15, utf8_decode( "RELATÓRIO CLIENTE: $strCliente" ), 0, 1, "C" );

	}

	public function Footer() {
		$this->SetFont( 'Arial', 'B', 12 );
		$this->SetTextColor( 0, 0, 0 );
		$this->SetXY( 130, 280 );
		$this->Cell( 120, 15, utf8_decode( "Página: " . $this->PageNo() ), 0, 1, "C" );
	}

	public function SetCol( $col ) {
		//Set position at a given column
		$this->col = $col;
		$x         = 10 + $col * 65;
		$this->SetLeftMargin( $x );
		$this->SetX( $x );
	}

	public function AcceptPageBreak() {
		//Method accepting or not automatic page break
		if ( $this->col < 2 ) {
			//Go to next column
			$this->SetCol( $this->col + 1 );
			//Set ordinate to top
			$this->SetY( $this->y0 );

			//Keep on page
			return false;
		} else {
			//Go back to first column
			$this->SetCol( 0 );

			//Page break
			return true;
		}
	}

	public function ChapterBody() {

		$obj = new models_T0016();

		$this->imprimiCabecalho();

		$this->SetFont( 'Arial', '', 10 );

		$dadosContaCliente = $this->select2();

		foreach ( $dadosContaCliente as $campos => $valores ) {
			$saldoCliente    = $valores['SaldoCliente'];
			$recebidoCliente = $valores['RecebidoCliente'];
		}

		$dados       = $this->select();
		$i = 1;
		$totalPedidos = 0;
		$totalPedidoFornecedor = 0;
		$totalPagtos = 0;
		foreach ( $dados as $campos => $valores ) {
			$obsPagto = $valores['ObsCR'];

			if ( $valores["Tipo"] == 2 ) {
				$totalPedidos += $valores['Valor'];
				$this->SetTextColor( 0, 64, 255 );

			}else if ( $valores["Tipo"] == 3 ) {
				$totalPedidoFornecedor += $valores['Valor'];
				$this->SetTextColor( 223, 0, 41 );

			} else {
				$totalPagtos += $valores['Valor'];
				$this->SetTextColor( 223, 0, 41 );
			}

			if ( $valores['Tipo'] == 1 ) {
				$valor = $obj->formataMoeda( $valores['Valor'] );
			}  else {
				$valor = "";
			}

			if ( $valores['Tipo'] == 1 ) {
				$tipo = "Pagamento";
			} elseif($valores['Tipo'] == 3){
				$tipo = "Pedido Forn.";
			} else {
				$tipo = "Pedido";
			}

			$this->SetFont( 'Arial', 'B', 8 );
			$this->Cell( 20, 7, utf8_decode( $tipo ), 1, 0, "L" );
			$this->Cell( 20, 7, utf8_decode( $obj->formataDataView( $valores['Data'] ) ), 1, 0, "L" );
			if ( $valores['Tipo'] == 1 || $valores['ObsCR'] == '' ) {

				$this->Cell( 120, 7, utf8_decode( "Observação :" . $obsPagto ), 1, 0, "L" );
			} else {
				$this->Cell( 120, 7, "", 1, 0, "L" );
			}
			$this->Cell( 30, 7, utf8_decode( $valor ), 1, 1, "R" );

			$i = $this->checkNumberLinesAndAddPage($i);

			$this->SetTextColor( 0, 0, 0 );

			if ( $valores['Tipo'] != 1 ) {
				if($valores['Tipo'] == 2){
					$dadosDetalhePedido = $obj->detalhePedido( $valores['CodigoPedido'] );
				}else{
					$dadosDetalhePedido = $obj->detalhePedidoFornecedor( $valores['CodigoPedido'] );
				}

				foreach ( $dadosDetalhePedido as $cps => $vls ) {
					$this->Cell( 20, 7, "", 1, 0, "L" );
					$this->Cell( 20, 7, utf8_decode( $obj->formataDataView( $vls['Data'] ) ), 1, 0, "L" );
					$this->Cell( 50,
						7,
						utf8_decode( $obj->retornaFormatoCodigoNome( $vls['CodigoProduto'],
							$vls['DescricaoProduto'] ) ),
						1,
						0,
						"L" );
					$this->Cell( 15, 7, utf8_decode( $vls['QtdeProduto'] ), 1, 0, "C" );
					$this->Cell( 25, 7, utf8_decode( $obj->formataMoeda( $vls['ValorProduto'] ) ), 1, 0, "R" );
					$this->Cell( 30, 7, utf8_decode( $obj->formataMoeda( $vls['SubtotalProduto'] ) ), 1, 0, "R" );
					$this->Cell( 30, 7, '', 1, 1, "R" );

					$i = $this->checkNumberLinesAndAddPage($i);
				}

			}

		}

		$this->SetFont( 'Arial', 'B', 12 );

		$this->SetTextColor( 0, 0, 0 );
		$this->Cell( 160, 7, utf8_decode( "Saldo Atual   " ), 1, 0, "R" );

		if ( $saldoCliente < 0 ) {
			$this->SetTextColor( 223, 0, 41 );
		} else {
			$this->SetTextColor( 0, 64, 255 );
		}

		$this->Cell( 30, 7, utf8_decode( $obj->formataMoeda( $saldoCliente ) ), 1, 1, "R" );

	}

	public function checkNumberLinesAndAddPage( $numberLines ) {

		++ $numberLines;

		if ( $numberLines >= 36 ) {
			$this->AddPage();

			$this->imprimiCabecalho();

			$this->SetFont( 'Arial', '', 10 );

			$numberLines = 1;

			return $numberLines;

		} else {

			return $numberLines;

		}


	}

	public function imprimiCabecalho() {

		$this->SetTextColor( 0, 0, 0 );

		$this->SetFont( 'Arial', 'B', 10 );

		$this->Cell( 20, 7, utf8_decode( "Tipo" ), 1, 0, "L" );
		$this->Cell( 20, 7, utf8_decode( "Data" ), 1, 0, "L" );
		$this->Cell( 50, 7, utf8_decode( "Produto" ), 1, 0, "L" );
		$this->Cell( 15, 7, utf8_decode( "Qtde" ), 1, 0, "L" );
		$this->Cell( 25, 7, utf8_decode( "Valor Venda" ), 1, 0, "L" );
		$this->Cell( 30, 7, utf8_decode( "Subtotal(débito)" ), 1, 0, "L" );
		$this->Cell( 30, 7, utf8_decode( "Subtotal(crédto)" ), 1, 1, "L" );
	}

	public function PrintChapter( $num, $title, $file ) {
		//Add chapter
		$this->AddPage();
		$this->ChapterBody();

	}
}

$pdf = new PDF();
$pdf->SetAutoPageBreak( false );
$pdf->SetTitle( "Relatório Pedidos Detalhes" );
$pdf->PrintChapter( 1, 'Relatório Pedidos Detalhes', '' );
$pdf->Output();

?>