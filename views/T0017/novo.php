<?php
//Instancia Class
$obj    =   new models_T0017();


function imprimi($obj,  $codigoConta, $codigoCliente, $dataConta, $valorConta, $observacaoConta)
{

    $objCliente = new models_T0006();
    $clientes = $objCliente->retornaDados($codigoCliente);

    foreach($clientes as $key => $value)
    {
		$cliente = $obj->retornaFormatoCodigoNome($value['CodigoCliente'], $value['NomeCliente']);
    }

	$obj->enviaImpressora(0, "CONTA Nro. :$codigoConta", 1);
	$obj->enviaImpressora(0, "CLIENTE    :$cliente", 1);
	$obj->enviaImpressora(0, "DATA       :".$dataConta, 1);
	$obj->enviaImpressora(0, "VALOR      :".$valorConta, 1);
	$obj->enviaImpressora(0, "OBSERVACAO :".$observacaoConta, 1);
	$obj->enviaImpressora(0, "", 2);

	$obj->enviaImpressora(2);
}

if(!empty($_POST))
{
    $tabela =   "t012_contas_receber";
    
    $user           = $_SESSION['user']         ;
    $codigoCliente  = $_POST['T002_codigo']     ;
    $dataConta      = $_POST['T012_data']       ;
    $valorConta     = $_POST['T012_valor']      ;
    $observacaoConta= $_POST['T012_observacao'] ;
    
    $campos =   array("T002_codigo"     => $codigoCliente
                     ,"T012_data"       => $dataConta
                     ,"T012_valor"      => $valorConta
                     ,"t001_login"      => $user
                     ,"T012_observacao" => $observacaoConta);
    
    $inseri =   $obj->inserir($tabela, $campos);
    $codigoConta = $obj->lastInsertId();
    
    if ($inseri)
    {
        //Atualiza Saldo Cliente
        $tabela = "T002_cliente";
        $valorConta = $obj->formataValor($tabela, "T002_saldo", $valorConta);
        $saldoCliente   =   $obj->creditaContaCliente($codigoCliente, $valorConta);

		imprimi($obj,  $codigoConta, $codigoCliente, $dataConta, $valorConta, $observacaoConta);
		imprimi($obj,  $codigoConta, $codigoCliente, $dataConta, $valorConta, $observacaoConta);

        header("Location: ".ROUTER."home");
        exit;

    }
    
}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">
        <div class="grid_4">
            <p>Cliente*</p>
            <?php echo $obj->inputCliente("");?>
        </div>
        
        <div class="grid_2">
            <p>Data *</p>
            <input type="text" name="T012_data"     class="validate[required] text-input data"   value="<?php echo date("d/m/Y");?>"                           />                  
        </div>
        
        <div class="grid_2">
            <p>Valor *</p>
            <input type="text" name="T012_valor"    class="validate[required] text-input valor "                      />                  
        </div>
        
        <div class="grid_7">
            <p>Observação</p>
            <input type="text" name="T012_observacao"    />                  
        </div>
        
        <div class="grid_1">
            <input type="hidden" name="t001_login"    value="<?php echo $user;?>"/>
        </div>
        
        <div class="clear"></div>

        <div class="grid_2">
            <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        
    </div>        
</form>