<?php
//Instancia Classe
$obj = new models_T0017();

$codigoConta = $_REQUEST['codigoConta'];

$dadosCabec = $obj->retornaDadosContaImpressao($codigoConta);

foreach ($dadosCabec as $campos => $valores) {
	$cliente = $obj->retornaFormatoCodigoNome($valores['clienteCodigo'], $valores['clienteNome']);
	$dataConta = $obj->formataDataHoraView($valores['dataConta']);
	$valor = $valores['valorConta'];
	$observacao = $valores['obsConta'];
}

$arrRetorno = array("CLIENTE"       => $cliente
					, "VALOR"       => $valor
					, "OBSERVACAO"  => $observacao
					, "DATA"        => $dataConta);

echo json_encode($arrRetorno);

?>
