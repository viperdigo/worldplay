<?php
//Instancia 
$obj = new models_T0017();

//Filtros
if (!empty($_POST)) {
	$codigoContaR = $_POST['T012_codigo'];
	$codigoCliente = $_POST['T002_codigo'];
	$dataInicio = $_POST['T012_data'];
	$dataFim = $_POST['T012_dataF'];

	$limit = $_POST['QtdeRegistros'];

	$dados = $obj->retornaDados($codigoContaR, $codigoCliente, $dataInicio, $dataFim, $limit);
} else {
	//Sem Filtro
	$dados = $obj->retornaDados();
}

//Variavel para verificar se existem dados de retorno para tabela não aparecer
$QtdeDados = 0;
?>
<div id="barra-botoes">
    <a href="#" class="ui-state-default ui-corner-all botoes-links abrirFiltros"> <span
                class="ui-icon ui-icon-carat-2-n-s">  </span>Filtros</a>
    <a href="<?php echo ROUTER; ?>novo" class="ui-state-default ui-corner-all botoes-links"> <span
                class="ui-icon ui-icon-plus">         </span>Novo</a>
    <hr>
</div>
<div id="barra-filtros">
    <form action="" method="post">
        <div class="conteudo_16">
            <!-- CAMPOS DA BARRA DE FILTROS -->
            <div class="grid_3">
                <label>Dinâmico</label>
                <input type="text" class="filtroDinamico"/>
            </div>

            <div class="grid_1">
                <label>Código</label>
                <input type="text" name="T007_codigo"/>
            </div>

            <div class="grid_4">
                <label>Cliente</label>
				<?php echo $obj->inputCliente($codigoCliente); ?>
            </div>

            <div class="grid_2">
                <label>Data Início</label>
                <input type="text" name="T012_data" class="data" value="<?php echo $dataInicio; ?>"/>
            </div>

            <div class="grid_2">
                <label>Data Fim</label>
                <input type="text" name="T012_dataF" class="data" value="<?php echo $dataFim; ?>"/>
            </div>

            <div class="grid_1">
                <label>Registros</label>
				<?php echo $obj->comboQtdeRegistros($limit); ?>
            </div>

            <div class="grid_2">
                <input type="submit" value="Filtrar" class="ui-button ui-widget ui-state-default ui-corner-all botao"
                       role="button" aria-disabled="false">
            </div>
        </div>
    </form>
</div>
<div class="conteudo_16">
    <table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
        <thead>
        <tr>
            <th>Conta</th>
            <th>Cliente</th>
            <th>Data Pagamento</th>
            <th>Valor</th>
            <th>Observação</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
		<?php foreach ($dados as $campos => $valores) {
			$QtdeDados++; ?>
            <tr class="linhaAdd">
                <td class="id"><?php echo $obj->retornaFormatoCodigo($valores['CodigoConta']); ?></td>
                <td><?php echo $obj->retornaFormatoCodigoNome($valores['CodigoCliente'], $valores['NomeCliente']); ?></td>
                <td><?php echo $obj->formataDataView($valores['DataConta']); ?></td>
                <td class="valor"><?php echo $obj->formataMoeda($valores['ValorConta']); ?></td>
                <td><?php echo $valores['ObservacaoConta']; ?></td>
                <td class="acoes">
                    <ul class="ui-widget ui-helper-clearfix" id="icons">
                        <li title="Alterar" class="ui-state-default ui-corner-all"><a
                                    href="<?php echo ROUTER ?>alterar&codigoContaR=<?php echo $valores['CodigoConta']; ?>"><span
                                        class='ui-icon ui-icon-pencil'></span></a></li>
                        <li title="Excluir" class="ui-state-default ui-corner-all excluirConta" data-id="<?php echo $valores['CodigoConta']?>"><a href="#"><span
                                        class="ui-icon ui-icon-closethick"></span></li>
                        <li title="Imprimir" class="ui-state-default ui-corner-all btnImprimir"><a href="#"><span   class='ui-icon ui-icon-print'></span></a></li>
                    </ul>
                </td>
            </tr>
		<?php } ?>
        <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->
		<?php if ($QtdeDados == 0) { ?>
            <tr>
                <td colspan="6" style="text-align: center">Não Existem Dados.</td>
            </tr>
		<?php } ?>
        </tbody>
    </table>
</div>   
