<?php
//Instacia Classe
$obj = new models_T0017();

//Captura Parametro
$codigoContaR = intval($_POST['item']);

$tabela = "t012_contas_receber";

$dadosContaR = $obj->retornaDados($codigoContaR);

foreach ($dadosContaR as $cps => $vls) {
	$codigoCliente = $vls['CodigoCliente'];
	$valorConta = $vls['ValorConta'];
}

$obj->debitaContaCliente($codigoCliente, $valorConta);

$delimitador = "T012_codigo    =   $codigoContaR";

$exclui = $obj->excluir($tabela, $delimitador);

if ($exclui)
	echo 1;
else
	echo 0;

?>