<?php
//Instancia Class
$obj = new models_T0017();

$codigoContaR = $_REQUEST['codigoContaR'];

//Dados dos Campos
$dados = $obj->retornaDados($codigoContaR);

$dados2 = $obj->retornaDados($codigoContaR);
foreach ($dados2 as $campos => $vls) {
    $vlrOld = $vls['ValorConta'];
}

if (!empty($_POST)) {
    //Retira mascara
    $tabela = "t012_contas_receber";

    $user = $_SESSION['user'];
    $codigoCliente = $_POST['T002_codigo'];
    $dataConta = $_POST['T012_data'];
    $valorConta = $_POST['T012_valor'];
    $observacaoConta = $_POST['T012_observacao'];

    $campos = array(
        "T002_codigo" => $codigoCliente
    ,
        "T012_data" => $dataConta
    ,
        "T012_valor" => $valorConta
    ,
        "t001_login" => $user
    ,
        "T012_observacao" => $observacaoConta
    );

    $delimitador = "T012_codigo    =".$codigoContaR;

    $alterar = $obj->atualizar($tabela, $campos, $delimitador);
    if ($alterar) {

        $valorConta = str_replace("R$", "", $valorConta);
        $valorConta = trim($valorConta);
        $valorConta = str_replace(".", "", $valorConta);
        $valorConta = str_replace(",", ".", $valorConta);

        $vlrSaldoAtual = $valorConta - $vlrOld;

        $tabela = "T002_cliente";

        $vlrSaldoAtual = $obj->formataValor($tabela, "T002_saldo", $vlrSaldoAtual);

        $obj->creditaContaCliente($codigoCliente, $vlrSaldoAtual);

        header("Location: ".ROUTER."home");
        exit;
    }
}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER ?>home" class="ui-state-default ui-corner-all botoes-links"><span
            class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>
</div>
<form action="" method="post" class="validar">
    <div class="conteudo_16">
        <?php foreach ($dados as $campos => $vls) { ?>
            <div class="grid_4">
                <p>Cliente*</p>
                <?php echo $obj->inputCliente($vls['CodigoCliente']); ?>
            </div>

            <div class="grid_2">
                <p>Data *</p>
                <input type="text" name="T012_data" class="validate[required] text-input"
                       value="<?php echo $obj->formataDataView($vls['DataConta']); ?>"/>
            </div>

            <div class="grid_2">
                <p>Valor *</p>
                <input type="text" name="T012_valor" class="validate[required] text-input valor "
                       value="<?php echo $vls['ValorConta']; ?>"/>
            </div>

            <div class="grid_7">
                <p>Observação</p>
                <input type="text" name="T012_observacao" value="<?php echo $vls['ObservacaoConta']; ?>"/>
            </div>

            <div class="grid_1">
                <input type="hidden" name="t001_login" value="<?php echo $user; ?>"/>
            </div>

            <div class="clear"></div>

            <div class="grid_2">
                <input type="submit" value="Alterar" class="ui-button ui-widget ui-state-default ui-corner-all"
                       role="button" aria-disabled="false">
            </div>
        <?php } ?>
    </div>
</form>