<?php
//Instancia Class
$obj    =   new models_T0023();

if(!empty($_POST))
{
    $tabela =   "t015_devolucao";
    
    $data    =   date("d/m/Y h:i:s");
    
    $codigoCliente      =   $_POST['T002_codigo'];
        
    $campos =   array( "T002_codigo"    => $codigoCliente
                     , "T015_data"      => $data
                     , "T015_status"    => 1); //Status 1 = Pendente
    
    $iDevolucao    =   $obj->inserir($tabela, $campos);
    
    if ($iDevolucao)
    {        
        $codigoDevolucao   =  $obj->lastInsertId();
        
        $tabela =   "t019_devolucao_detalhe";
        
        $qtdeCampos =   count($_POST['T019_qtde']);
               
        for($i=0;$i<$qtdeCampos;$i++)
        {
            if (!empty($_POST['T003_codigo'][$i]))
            {
                $arrProduto = explode("-", $_POST['T003_codigo'][$i]);
                $codigoProduto  =   (int)$arrProduto[0];
                $qtdeProduto    =   $_POST['T019_qtde'][$i];  
                
                $obsDevolucao       =   $_POST['T019_observacao'][$i];  
                
                $campos =      array( "T015_codigo"         =>  $codigoDevolucao
                                    , "T003_codigo"         =>  $codigoProduto
                                    , "T019_qtde"           =>  $qtdeProduto
                                    , "T019_observacao"     =>  $obsDevolucao
                                );  

                $obj->inserir($tabela, $campos);                                    
            }                                                           
        }
        
        header("Location: ".ROUTER."home");
        exit;        
        
        
    }
                   
} 
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">

        <div class="grid_3">
            <p>Cliente *</p>
            <?php echo $obj->inputCliente("",1);?>
        </div>
        
        <div class="clear"></div>
        
        <div class="grid_7">
            <p>Produto*</p>
        </div>
        
        <div class="grid_2">
            <p>Quantidade</p>
        </div>
        
        <div class="grid_6">
            <p>Observação</p>
        </div>
        
        <div class="clear"></div>
        
        <div id="linhaPrincipal">
            
            <div class="linhaAdd">
        
                <div class="grid_7">
                    <input type="text" name="T003_codigo[]"       class="campoProdutos"   />                  
                </div>

                <div class="grid_2">
                    <input type="text" name="T019_qtde[]"                                 />                  
                </div>

                <div class="grid_6">
                    <input type="text" name="T019_observacao[]"   class="campoObservacao" />                  
                </div>

                <div class="clear"></div>
            
            </div>
        
        </div>

        <div class="grid_2">
            <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        
    </div>        
</form>