<?php

class PDF extends FPDF
{
    //Current column
    var $col = 0;
    //Ordinate of column start
    var $y0;

    public function select()
    {

        $codigoManutencao = $_GET['T016_codigo'];
        $codigoCliente = $_GET['T002_codigo'];
        $statusManutencao = $_GET['T016_status'];
        $dataEntInicio = $_GET['T016_dt_entrada'];
        $dataEntFim = $_GET['T016_dt_entradaF'];
        $dataRetInicio = $_GET['T016_dt_retirada'];
        $dataRetFim = $_GET['T016_dt_retiradaF'];
        $limit = $_GET['QtdeRegistros'];

        $obj = new models_T0030();

        $dados = $obj->retornaDados(
            $codigoManutencao,
            $codigoCliente,
            $dataEntInicio,
            $dataEntFim,
            $dataRetInicio,
            $dataRetFim,
            $statusManutencao,
            $limit
        );

        return $dados;

    }

    public function Header()
    {
        $this->SetFont('Arial', 'B', 20);

        $this->Cell(190, 15, utf8_decode("RELATÓRIO MANUTENÇÃO"), 0, 1, "C");

    }

    public function Footer()
    {
        $this->SetFont('Arial', 'B', 12);
        $this->SetXY(130, 280);
        $this->Cell(120, 15, utf8_decode("Página: " . $this->PageNo()), 0, 1, "C");
    }

    public function SetCol($col)
    {
        //Set position at a given column
        $this->col = $col;
        $x = 10 + $col * 65;
        $this->SetLeftMargin($x);
        $this->SetX($x);
    }

    public function AcceptPageBreak()
    {
        //Method accepting or not automatic page break
        if ($this->col < 2) {
            //Go to next column
            $this->SetCol($this->col + 1);
            //Set ordinate to top
            $this->SetY($this->y0);

            //Keep on page
            return false;
        } else {
            //Go back to first column
            $this->SetCol(0);

            //Page break
            return true;
        }
    }

    public function ChapterBody()
    {

        $obj = new models_T0030();
        $dados = $this->select();

        $this->SetFont('Arial', 'B', 8);

        $largCodigo = 15;
        $largCliente = 46;
        $largDatas = 16;
        $largStatus = 49;

        $this->Cell($largCodigo, 7, utf8_decode("Código"), 1, 0, "L");
        $this->Cell($largCliente, 7, utf8_decode("Cliente"), 1, 0, "L");
        $this->Cell($largDatas, 7, utf8_decode("Recebido"), 1, 0, "L");
        $this->Cell($largDatas, 7, utf8_decode("Enviado"), 1, 0, "L");
        $this->Cell($largDatas, 7, utf8_decode("Devolvido"), 1, 0, "L");
        $this->Cell($largDatas, 7, utf8_decode("Entregue"), 1, 0, "L");
        $this->Cell($largDatas, 7, utf8_decode("Cancelado"), 1, 0, "L");
        $this->Cell($largStatus, 7, utf8_decode("Status"), 1, 1, "L");

        $this->SetFont('Arial', '', 8);

        $i = 0;

        foreach ($dados as $item) {

            $strCliente = str_pad($obj->retornaFormatoCodigoNome($item['CodigoCliente'], $item['NomeCliente']), 30);

            $this->Cell($largCodigo, 7, utf8_decode($item['CodigoManutencao']), 1, 0, "L");
            $this->Cell($largCliente, 7, utf8_decode($strCliente), 1, 0, "L");
            $this->Cell($largDatas, 7, utf8_decode($item['DataEntManutencao']), 1, 0, "L");
            $this->Cell($largDatas, 7, utf8_decode($item['DataEnvioManutencao']), 1, 0, "L");
            $this->Cell($largDatas, 7, utf8_decode($item['DataRecManutencao']), 1, 0, "L");
            $this->Cell($largDatas, 7, utf8_decode($item['DataDevManutencao']), 1, 0, "L");
            $this->Cell($largDatas, 7, utf8_decode($item['DataCancManutencao']), 1, 0, "L");
            $this->Cell($largStatus, 7, utf8_decode($item['StatusManutencao']), 1, 1, "L");

            $i++;

            if ($i == 33) {
                $this->AddPage();
                $i = 0;
            }

        }
    }

    public function PrintChapter()
    {
        //Add chapter
        $this->AddPage();
        $this->ChapterBody();

    }
}

$pdf = new PDF();
$pdf->SetTitle("Relatório Pedidos Detalhes");
$pdf->PrintChapter(1, 'Relatório Pedidos Detalhes', '');
$pdf->Output();

?>