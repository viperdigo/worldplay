<?php
//Instancia 
$obj        =   new models_T0009()  ;

//Filtros
if (!empty($_POST))
{
    $codigoProduto  =   $_POST['T003_codigo']       ;                  
    $nomeProduto    =   $_POST['T003_descricao']    ;
    
    $tipo           =   $_POST['T003_tipo'];
    
    $limit          =   $_POST['QtdeRegistros']     ;
    
    $dados          =   $obj->retornaDados($codigoProduto, $nomeProduto,$tipo, $limit);
}else 
{
    //Sem Filtro
    $dados          =   $obj->retornaDados();
}

//Variavel para verificar se existem dados de retorno para tabela não aparecer
$QtdeDados  =   0                   ;
?>
<div id="barra-botoes">
    <a href="#"                         class="ui-state-default ui-corner-all botoes-links abrirFiltros">   <span class="ui-icon ui-icon-carat-2-n-s">  </span>Filtros</a>
    <a href="<?php echo ROUTER;?>novo"  class="ui-state-default ui-corner-all botoes-links">                <span class="ui-icon ui-icon-plus">         </span>Novo</a>
    <hr>    
</div>    
<div id="barra-filtros">
    <form action="" method="post">        
        <div class="conteudo_16">
            <!-- CAMPOS DA BARRA DE FILTROS -->
                        
            <div class="grid_3">
                <label>Dinâmico</label>
                <input type="text"                      class="filtroDinamico"  />                  
            </div>

            <div class="grid_1">
                <label>Código</label>
                <input type="text" name="T003_codigo"       value="<?php echo $codigoProduto;?>"/>                  
            </div>

            <div class="grid_3">
                <label>Descrição</label>
                <input type="text" name="T003_descricao"    value="<?php echo $nomeProduto;?>"/>                  
            </div>
            
            <div class="grid_3">
                <label>Tipo</label>
                <select name="T003_tipo">
                    <option value=""  <?php echo $tipo==""?"selected":"";?>>...</option>
                    <option value="1" <?php echo $tipo=="1"?"selected":"";?>>001-NORMAL</option>
                    <option value="2" <?php echo $tipo=="2"?"selected":"";?>>002-RECEITA</option>
                    <option value="3" <?php echo $tipo=="3"?"selected":"";?>>003-GABINETE</option>
                </select>
            </div>
            
            <div class="grid_2">
                <label>Registros</label>
                <?php echo $obj->comboQtdeRegistros($limit);?>
            </div>

            <div class="grid_2">
                <input type="submit" value="Filtrar" class="ui-button ui-widget ui-state-default ui-corner-all botao" role="button" aria-disabled="false">                
            </div>

        </div>        
    </form>
</div>  
<div class="conteudo_16">
    <table class="tablesorter" cellspacing="1" cellpadding="0" border="0"> 
        <thead> 
            <tr> 
                <th>Código              </th> 
                <th>Descrição           </th> 
                <th>Valor Custo         </th> 
                <th>Valor Venda         </th> 
                <th>Estoque             </th> 
                <th>Tipo                </th> 
                <th>Ações               </th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php   foreach($dados    as $campos => $valores)
                    {   $QtdeDados++; ?>
                        <tr> 
                            <td class="id"><?php echo $obj->retornaFormatoCodigo($valores['CodigoProduto']);?> </td> 
                            <td><?php echo $valores['DescricaoProduto'];?></td> 
                            <td><?php echo $valores['VlCustoProduto'];?></td> 
                            <td><?php echo $valores['VlVendaProduto'];?></td> 
                            <td><?php echo $valores['EstoqueProduto'];?></td> 
                            <td><?php echo $valores['TipoProduto'];?></td> 
                            <td class="acoes">
                                <ul class="ui-widget ui-helper-clearfix" id="icons">
                                    <?php if($valores['TipoProduto']=='NORMAL' || $valores['TipoProduto']=='GABINETE'){?>
                                    <li title="Alterar" class="ui-state-default ui-corner-all"><a href="<?php echo ROUTER?>alterar&codigoProduto=<?php echo $valores['CodigoProduto'];?>"><span   class='ui-icon ui-icon-pencil'></span></a></li>
                                    <?php }else{?>
                                    <li title="Alterar" class="ui-state-default ui-corner-all"><a href="<?php echo ROUTER?>alterarReceita&codigoProduto=<?php echo $valores['CodigoProduto'];?>"><span   class='ui-icon ui-icon-pencil'></span></a></li>
                                    <?php }?>
                                    <li title="Excluir" class="ui-state-default ui-corner-all excluir"><a href="#"><span class="ui-icon ui-icon-closethick"></span></li>
                                </ul>
                            </td> 
                        </tr>
            <?php   }  ?>
                    <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->    
            <?php if ($QtdeDados==0){    ?>        
                    <tr>
                        <td colspan="6" style="text-align: center">Não Existem Dados.</td>
                    </tr>    
            <?php }?>
        </tbody> 
    </table> 
</div>   
