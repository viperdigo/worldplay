<?php

$obj                    =   new models_T0009();

$produtoProcurado       =   $_GET['term'];
$produtoReceita         =   $_GET['receita'];
$produtoNormal          =   $_GET['normal'];

$rArray                 =   array();        //array retorno


//Para Código do Produto
$dados  = $obj->retornaDados($produtoProcurado, "", $produtoReceita, $produtoNormal);

foreach($dados as $campos   =>  $valores)
{
    $strProduto =   $obj->retornaFormatoCodigoNome($valores['CodigoProduto'], $valores['DescricaoProduto']);
    array_push($rArray, $strProduto);
}
    
//Para String
$dados = $obj->retornaDados("",$produtoProcurado, $produtoReceita, $produtoNormal);

foreach($dados as $campos   =>  $valores)
{
    $strProduto =   $obj->retornaFormatoCodigoNome($valores['CodigoProduto'], $valores['DescricaoProduto']);
    array_push($rArray, $strProduto);
}


echo json_encode($rArray);

?>