<?php
//Instancia Class
$obj = new models_T0009();

if (!empty($_POST)) {
    if ($_POST['T003_tipo'] == 1) {

        $tabela = "t003_produto";

        $_POST = $obj->retiraMascaraArray($_POST, $tabela);

        $inseri = $obj->inserir($tabela, $_POST);

        if ($inseri) {
            header("Location: " . ROUTER . "home");
            exit;
        }

    } else {
        $tabela = "t003_produto";

        $fornecedor = $_POST['T004_codigo'];
        $descricao = $_POST['T003_descricao'];
        $vlCusto = $_POST['T003_vl_custo'];
        $vlVenda = $_POST['T003_vl_unit'];
        $tipo = $_POST['T003_tipo'];

        $campos = array("T004_codigo" => $fornecedor
        , "T003_descricao" => $descricao
        , "T003_vl_custo" => $vlCusto
        , "T003_vl_unit" => $vlVenda
        , "T003_tipo" => $tipo
        );

        $obj->inserir($tabela, $campos);

        $codigoProduto = $obj->lastInsertId();

        //Captura Componentes do Produto Referencia              
        $produtoReferencia = $_POST['ProdutoReferencia'];
        $arrProdutoReferencia = explode("-", $produtoReferencia);
        $codigoProdutoReferencia = $arrProdutoReferencia[0];

        $tabela = "t013_componente";

        $componentes = $obj->retornaComponentes($codigoProdutoReferencia);

        //Inseri Componentes do Produto Referencia
        foreach ($componentes as $campos => $valores) {

            $codigoComponente = $valores['CodigoComponente'];
            $qtdComponente = $valores['QtdeComponente'];

            $campos = array("T003_codigo" => $codigoProduto
            , "T003_componente" => $codigoComponente
            , "T013_qtd_componente" => $qtdComponente
            );

            $obj->inserir($tabela, $campos);
        }

        //Insere Componentes dados pelo Usuário

        $qtdeCampos = count($_POST['T013_qtd_componente']);

        for ($i = 0; $i < $qtdeCampos; $i++) {
            if (!empty($_POST['T013_qtd_componente'][$i])) {
                $arrComponente = explode("-", $_POST['T003_componente'][$i]);
                $codigoComponente = $arrComponente[0];

                $campos = array("T003_codigo" => $codigoProduto
                , "T003_componente" => $codigoComponente
                , "T013_qtd_componente" => $_POST['T013_qtd_componente'][$i]
                );

                $obj->inserir($tabela, $campos);
            }
        }

        header("Location: " . ROUTER . "home");
        exit;

    }
}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER ?>home" class="ui-state-default ui-corner-all botoes-links"><span
            class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>
</div>
<form action="" method="post" class="validar">
    <div class="conteudo_16">

        <div class="grid_6">
            <div id="radio">
                <input type="radio" id="radio1" name="T003_tipo" checked="checked" value="1"/><label
                    for="radio1">Normal</label>
                <input type="radio" id="radio2" name="T003_tipo" value="2"/><label for="radio2">Receita</label>
                <input type="radio" id="radio3" name="T003_tipo" value="3"/><label for="radio3">Gabinete</label>
            </div>
        </div>

        <div class="clear"></div>

        <!-- Itens Normais -->

        <div id="normal">

            <div class="grid_3">
                <p>Fornecedor</p>
                <?php echo $obj->comboFornecedor("", 0); ?>
            </div>

            <div class="grid_7">
                <p>Descrição*</p>
                <input type="text" name="T003_descricao" class="validate[required] text-input campoDescricao"/>
            </div>

            <div class="grid_2">
                <p>Qtde. Estoque</p>
                <input type="text" name="T003_qtd_estoque" class="text-input"/>
            </div>

            <div class="grid_2">
                <p>Valor Custo</p>
                <input type="text" name="T003_vl_custo" class="text-input valor"/>
            </div>

            <div class="grid_2">
                <p>Valor Venda</p>
                <input type="text" name="T003_vl_unit" class="text-input valor"/>
            </div>

            <div class="clear"></div>

            <div class="grid_2">
                <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all"
                       role="button" aria-disabled="false">
            </div>

        </div>

        <!-- Itens Receita -->

        <div id="receita" style="display: none;">

            <div class="grid_7">
                <p>Produto Referência</p>
                <input type="text" name="ProdutoReferencia" class="campoProdutosReceita"/>
            </div>

            <div class="grid_2">
                <p class="branco"></p>
                <a id="btComponente" href="#" class="ui-state-default ui-corner-all botoes-links"><span
                        class="ui-icon ui-icon-note"></span>Componentes</a>
            </div>

            <div class="clear"></div>

            <div class="grid_4">
                <p>Fornecedor</p>
                <?php echo $obj->comboFornecedor("", 0); ?>
            </div>

            <div class="grid_7">
                <p>Descrição*</p>
                <input type="text" name="T003_descricao"
                       class="validate[required] text-input campoDescricao descReceita"/>
            </div>

            <div class="grid_2">
                <p>Valor Custo</p>
                <input type="text" name="T003_vl_custo" class="text-input valor"/>
            </div>

            <div class="grid_2">
                <p>Valor Venda</p>
                <input type="text" name="T003_vl_unit" class="text-input valor"/>
            </div>

            <div class="clear10"></div>

            <div class="grid_16">
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">Componentes</a></li>
                    </ul>
                    <div id="tabs-1">
                        <div class="conteudo_16">

                            <div class="grid_7">
                                <p>Produto *</p>
                            </div>

                            <div class="grid_2">
                                <p>Quantidade *</p>
                            </div>

                            <div id="linhaPrincipal">

                                <div class="linhaAdd">

                                    <div class="grid_7">
                                        <input type="text" name="T003_componente[]" class="campoProdutosNormal"/>
                                    </div>

                                    <div class="grid_2">
                                        <input type="text" name="T013_qtd_componente[]" class="campoQtd"/>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="grid_2">
                <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all"
                       role="button" aria-disabled="false">
            </div>

        </div>

    </div>
</form>