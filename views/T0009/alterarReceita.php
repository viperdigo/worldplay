<?php
//Instancia Class
$obj            =   new models_T0009()                  ;

$codigoProduto  =   $_REQUEST['codigoProduto']          ;

//Dados dos Campos
$dados              =   $obj->retornaDados($codigoProduto)  ;

$dadosComponentes   =   $obj->retornaComponentes($codigoProduto)  ;

if(!empty($_POST))
{
    $tabela         =   "t003_produto"              ;
    
    $codigoProduto  =   $_POST['T003_codigo']       ;
     
    $fornecedor     =   $_POST['T004_codigo']       ;
    $descricao      =   $_POST['T003_descricao']    ;
    $vlCusto        =   $_POST['T003_vl_custo']     ;
    $vlVenda        =   $_POST['T003_vl_unit']      ;

    $campos         =   array( "T004_codigo"    =>  $fornecedor
                             , "T003_descricao" =>  $descricao
                             , "T003_vl_custo"  =>  $vlCusto
                             , "T003_vl_unit"   =>  $vlVenda
                             );

    $delimitador        =   "T003_codigo    =".$codigoProduto;

    $obj->atualizar($tabela, $campos, $delimitador);

    //Captura Componentes do Produto Referencia              
    $produtoReferencia      =   $_POST['ProdutoReferencia'];
    $arrProdutoReferencia   =   explode("-", $produtoReferencia);
    $codigoProdutoReferencia=   $arrProdutoReferencia[0];

    $tabela         =   "t013_componente";

    $componentes    =   $obj->retornaComponentes($codigoProdutoReferencia);

    //Inseri Componentes do Produto Referencia
    foreach($componentes    as  $campos =>  $valores)
    {

        $codigoComponente   =   $valores['CodigoComponente'];
        $qtdComponente      =   $valores['QtdeComponente'];

        $campos =   array ( "T003_componente"       =>  $codigoComponente
                          , "T013_qtd_componente"   =>  $qtdComponente
                          );
        
        $delimitador        =   "T003_codigo    =".$codigoProduto;

        $obj->atualizar($tabela, $campos, $delimitador);
    }        

    //Insere Componentes dados pelo Usuário
    $qtdeCampos =   count($_POST['T013_qtd_componente']);

    for($i=0;$i<$qtdeCampos;$i++)
    {            
        if (!empty($_POST['T013_qtd_componente'][$i]))
        {                
            $arrComponente      =   explode("-", $_POST['T003_componente'][$i]);
            $codigoComponente   =   $arrComponente[0];

            $campos =      array( "T003_codigo"         =>  $codigoProduto
                                , "T003_componente"     =>  $codigoComponente
                                , "T013_qtd_componente" =>  $_POST['T013_qtd_componente'][$i]
                                );  

            $obj->inserir($tabela, $campos);                                    
        }                                                           
    }

    header("Location: ".ROUTER."home");
    exit;     

}

    
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">
        
        <?php foreach($dados    as  $campos =>  $valores){?>
        
        <!-- Itens Receita -->
        
        <div>

            <div class="grid_7">
                <p>Produto Referência</p>
                <input type="text" name="ProdutoReferencia"         class="campoProdutosReceita"                       />
            </div>

            <div class="grid_2">
                <p class="branco"></p>
                <a id="btComponente" href="#" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-note"></span>Componentes</a>
            </div>
            
            <div class="clear"></div>
            
            <div class="grid_4">
                <p>Fornecedor</p>
                <?php echo $obj->comboFornecedor($valores['CodigoFornecedor'],0);?>
            </div>        
                        
            <div class="grid_2">
                <p>Código Produto</p>
                <input type="text" name="T003_codigo"         class="validate[required] text-input"   value="<?php echo $valores['CodigoProduto'];?>"    readonly    id="codigoProduto" />                  
            </div>            

            <div class="grid_5">
                <p>Descrição*</p>
                <input type="text" name="T003_descricao"         class="validate[required] text-input campoDescricao descReceita"   value="<?php echo $valores['DescricaoProduto'];?>"         />                  
            </div>

            <div class="grid_2">
                <p>Valor Custo</p>
                <input type="text" name="T003_vl_custo"     class="text-input valor"      value="<?php echo $valores['VlCustoProduto'];?>"                        />                  
            </div>

            <div class="grid_2">
                <p>Valor Venda</p>
                <input type="text" name="T003_vl_unit"     class="text-input valor"      value="<?php echo $valores['VlVendaProduto'];?>"                        />                  
            </div> 
            
        <?php }?>
            
            <div class="clear10"></div>
            
            <div class="grid_16">
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">Componentes</a></li>
                    </ul>
                    <div id="tabs-1">
                        
                        <div class="conteudo_16">
                            
                            <div class="grid_7">
                                <p>Produto *</p>
                            </div>
                            
                            <div class="grid_2">
                                <p>Quantidade *</p>
                            </div>
                            
                            <div class="grid_2">
                                <p>Ações</p>
                            </div>
                            
                            <div class="grid_2">
                                <ul class="ui-widget ui-helper-clearfix" id="icons">
                                    <li id="adicionaLinha" title="Adicionar" class="ui-state-default ui-corner-all" style="margin: 0px;">
                                        <a href="#"><span class="ui-icon ui-icon-plus"></span>
                                    </li>
                                </ul>
                            </div>
                            
                            <div id="linhas">
                            
                                <?php foreach($dadosComponentes as $cpC =>  $vlC){?>                                

                                <div class="linhaAdd">

                                    <div class="grid_7">
                                        <input type="text" name="T003_componente[]"         class="campoProdutosNormal validate[required]"         value="<?php echo $obj->retornaFormatoCodigoNome($vlC['CodigoComponente'],$vlC['DescricaoProduto']);?>"  disabled />
                                    </div>

                                    <div class="grid_2">
                                        <input type="text" name="T013_qtd_componente[]"     class="campoQtd validate[required]"                    value="<?php echo $vlC['QtdeComponente'];?>" disabled/>
                                    </div>

                                    <div class="grid_2">
                                        <ul class="ui-widget ui-helper-clearfix" id="icons">
                                            <li title="Excluir" class="ui-state-default ui-corner-all excluirComponente" style="margin: 0px;"><a href="#"><span class="ui-icon ui-icon-closethick"></span></li>
                                        </ul>
                                    </div>

                                </div>

                                <?php }?>                                
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
            
            <div class="clear"></div>
            
            <div class="grid_2">
                <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
            </div>                         
    </div>        
</form>