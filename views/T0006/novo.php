<?php
//Instancia Class
$obj    =   new models_T0006();


if(!empty($_POST))
{
    $tabela =   "t002_cliente";
            
    $nome       =   $_POST['T002_nome'];
    $endereco   =   $_POST['T002_endereco'];
    $cep        =   $obj->retiraMascara($_POST['T002_cep']);
    $telefone   =   $obj->retiraMascara($_POST['T002_telefone']);
    $saldo      =   $obj->retornaValorNegativo($_POST['T002_saldo']);

    $campos     =   array( "T002_nome"      => $nome
                         , "T002_endereco"  => $endereco
                         , "T002_cep"       => $cep
                         , "T002_telefone"  => $telefone
                         , "T002_saldo"     => $saldo);    
    
    $inseri =   $obj->inserir($tabela, $campos);
    
    if ($inseri)
    {
        header("Location: ".ROUTER."home");
        exit;
    }
    
}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">
        <div class="grid_7">
            <p>Nome*</p>
            <input type="text" name="T002_nome"         class="validate[required] text-input"           />                  
        </div>
        
        <div class="grid_3">
            <p>Endereço</p>
            <input type="text" name="T002_endereco"     class="text-input"                              />                  
        </div>
        
        <div class="grid_2">
            <p>Cep</p>
            <input type="text" name="T002_cep"          class="text-input cep"                          />                  
        </div>
        
        <div class="grid_2">
            <p>Telefone</p>
            <input type="text" name="T002_telefone"     class="text-input "                              />                  
        </div>
        
        <div class="grid_2">
            <p>Saldo</p>
            <input type="text" name="T002_saldo"        class="text-input valor "          />                  
        </div>
        
        <div class="clear"></div>

        <div class="grid_2">
            <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        
    </div>        
</form>