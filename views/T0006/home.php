<?php
//Instancia 
$obj = new models_T0006();

//Filtros
if (!empty($_POST)) {
    $codigoCliente = $_POST['T002_codigo'];
    $nomeCliente = $_POST['T002_nome'];

    $limit = $_POST['QtdeRegistros'];

    $dados = $obj->retornaDados($codigoCliente, $nomeCliente, $limit);
} else {
    //Sem Filtro
    $dados = $obj->retornaDados();
}

//Variavel para verificar se existem dados de retorno para tabela não aparecer
$QtdeDados = 0;
?>
<div id="barra-botoes">
    <a href="#" class="ui-state-default ui-corner-all botoes-links abrirFiltros"> <span
            class="ui-icon ui-icon-carat-2-n-s">  </span>Filtros</a>
    <a href="<?php echo ROUTER; ?>novo" class="ui-state-default ui-corner-all botoes-links"> <span
            class="ui-icon ui-icon-plus">         </span>Novo</a>
    <hr>
</div>
<div id="barra-filtros">
    <form action="" method="post">
        <div class="conteudo_16">
            <!-- CAMPOS DA BARRA DE FILTROS -->
            <div class="grid_3">
                <label>Dinâmico</label>
                <input type="text" class="filtroDinamico"/>
            </div>
            <div class="grid_1">
                <label>Código</label>
                <input type="text" name="T002_codigo"/>
            </div>
            <div class="grid_3">
                <label>Nome</label>
                <input type="text" name="T002_nome"/>
            </div>
            <div class="grid_1">
                <label>Registros</label>
                <?php echo $obj->comboQtdeRegistros($limit); ?>
            </div>
            <div class="grid_2">
                <input type="submit" value="Filtrar" class="ui-button ui-widget ui-state-default ui-corner-all botao"
                       role="button" aria-disabled="false">
            </div>
        </div>
    </form>
</div>
<div class="conteudo_16">
    <table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
        <thead>
        <tr>
            <th>Cliente</th>
            <th>Endereço</th>
            <th>Cep</th>
            <th>Telefone</th>
            <th>Saldo Conta</th>
            <th>Status</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($dados as $campos => $valores) {
            $QtdeDados++;

            if ($valores['SaldoCliente'] >= 0) {
                $corLetra = "#0000FF";
            } else {
                $corLetra = "#FF0000";
            }

            ?>
            <tr>
                <td class="id"><?php echo $obj->retornaFormatoCodigoNome(
                        $valores['CodigoCliente'],
                        $valores['NomeCliente']
                    ); ?> </td>
                <td><?php echo $valores['EnderecoCliente']; ?></td>
                <td><?php echo $valores['CepCliente']; ?></td>
                <td><?php echo $valores['FoneCliente']; ?></td>
                <td><font color="<?php echo $corLetra; ?>"><?php echo $obj->formataMoeda($valores['SaldoCliente']); ?>
                </td>
                <?php

                if ($valores['StatusCliente'] == 1) {
                    $statusCliente = "Ativo";
                }else{
                    $statusCliente = "Inativo";
                }
                ?>

                <td align="center"><?php echo $statusCliente; ?><p class="statusCliente" style="display: none;"><?php echo $valores['StatusCliente'];?></p> </td>
                <td class="acoes">
                    <ul class="ui-widget ui-helper-clearfix" id="icons">
                        <li title="Alterar" class="ui-state-default ui-corner-all"><a
                                href="<?php echo ROUTER ?>alterar&codigoCliente=<?php echo $valores['CodigoCliente']; ?>"><span
                                    class='ui-icon ui-icon-pencil'></span></a></li>
                        <li title="Alterar Status" class="ui-state-default ui-corner-all status"><a href="#"><span
                                    class="ui-icon ui-icon-cancel"></span></li>
                        <li title="Excluir" class="ui-state-default ui-corner-all excluir"><a href="#"><span
                                    class="ui-icon ui-icon-closethick"></span></li>
                    </ul>
                </td>
            </tr>
        <?php } ?>
        <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->
        <?php if ($QtdeDados == 0) { ?>
            <tr>
                <td colspan="6" style="text-align: center">Não Existem Dados.</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>   
