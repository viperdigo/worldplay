<?php
//Instancia Classe
$obj            =   new models_T0011();

$tipo           =   $_REQUEST['tipo'];
$codigoPedido   =   $_REQUEST['codigoPedido'];

if($tipo==1)
{
    $dadosCabec =   $obj->retornaDadosPedidoImpressao($codigoPedido);
    
    foreach($dadosCabec as $campos  =>  $valores)
    {
        $cliente    =   $obj->retornaFormatoCodigoNome($valores['CodigoCliente'],$valores['NomeCliente']);
        $dataPedido =   $obj->formataDataHoraView($valores['DataPedido']);
    }
    
    $arrRetorno =   array( "CLIENTE"    =>  $cliente
                          ,"DATA"       =>  $dataPedido );
    
    echo json_encode($arrRetorno);
           
}else
{
    
    
    $dadosCabec =   $obj->retornaDadosPedidoImpressao($codigoPedido);
    
    $i  =   0;
    foreach($dadosCabec as $campos  =>  $valores)
    {
        $strProduto         =   $obj->preencheBranco($obj->retornaFormatoCodigo($i+1),3,0)                                                      //Campo ItemNro
                            .   " ".$obj->preencheBranco($valores['DescricaoProduto'], 24, 1)                                                   //Campo Descrição
                            .   $obj->preencheBranco($obj->retornaFormatoCodigo($valores['QtdeProduto']), 3, 0)                                 //Campo Qtde Produto
                            .   $obj->preencheBranco($valores['VlVendaProduto'],7,0)           //Campo Valor Unitário
                            .   $obj->preencheBranco($valores['SubtotalProduto'],9,0) ;
        
        $arrProdutos[$i]    =   $strProduto;
        
        $i++;
        
    } 
    
    echo json_encode($arrProdutos);
        
}
        


?>
