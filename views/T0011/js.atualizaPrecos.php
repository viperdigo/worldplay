<?php

$obj    =   new models_T0011();

$arrCliente     =   explode("-",$_REQUEST['codigoCliente']);
$codigoPedido   =   $_REQUEST['codigoPedido'];
$valorPedido    =   $_REQUEST['valorPedido'];
$arrCodigo      =   $_REQUEST['arrCodigo'];
$arrVlrVenda    =   $_REQUEST['arrVlrVda'];
$arrSubTotal    =   $_REQUEST['arrSubTotal'];

$codigoCliente  =   $arrCliente[0];

$nCampos    =   count($arrCodigo);

$tabela     =   "t011_pedido_detalhe";

for($i=0;$i<$nCampos;$i++)
{
    $arrSubTotal[$i]    =  str_replace(".",",", $arrSubTotal[$i]); 
    
    $campos =   array( "T011_valor_venda"   => $arrVlrVenda[$i]
                     , "T011_subtotal"      => $arrSubTotal[$i]
    );
    
    $delim  =   " T010_pedido = $codigoPedido
              AND T003_codigo = $arrCodigo[$i]";
    
    $obj->atualizar($tabela, $campos, $delim);
}

$subtotalPedido     =   $obj->recalculaSubtotalPedido($codigoPedido);

//Calcula Diferenca


$subtotalPedido =   $obj->formataValor("t010_pedido", "T010_valor", $subtotalPedido);

$valorPedido =   str_replace("R$ ", "", $valorPedido);
$valorPedido =   $obj->formataValor("t010_pedido", "T010_valor", $valorPedido);

$diff   =   $subtotalPedido - $valorPedido;

//echo "\nValor: ".$valorPedido;
//echo "\nSubtotal: ".$subtotalPedido;
//echo "\nDiff: ".$diff;

$obj->debitaContaCliente($codigoCliente, $diff);

$subtotalPedido =   $obj->formataMoeda($subtotalPedido);
echo str_replace("R$ ", "",$subtotalPedido);

?>