<?php
//Instancia Class
$obj            =   new models_T0011()                  ;

$codigoPedido  =   $_REQUEST['codigoPedido']          ;
    
//Dados dos Campos
$dados          =   $obj->retornaDados($codigoPedido)  ;

if(!empty($_POST))
{
    //Retira mascara
    $campos         =   $obj->retiraMascaraArray($_POST)        ;
    
    $tabela         =   "t010_pedido"                     ;
    
    $delimitador    =   "T010_codigo    =".$codigoPedido        ;
    
    $alterar =   $obj->atualizar($tabela, $campos, $delimitador);
    if ($alterar)
    {
        header("Location: ".ROUTER."home");
        exit;
    }    
}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">
        <?php foreach($dados    as $campos  =>  $vls){?>
        <div class="grid_7">
            <p>Data *</p>
            <input type="text" name="T010_data"    class="validate[required] text-input"       value="<?php echo $vls['DataPedido']?>"/>
        </div>
        
        <div class="grid_3">
            <p>Valor</p>
            <input type="text" name="T008_data_venc"     class="validate[required] text-input"      value="<?php echo $vls['DataVencConta']?>"/>
        </div>
        
        <div class="grid_3">
            <p>Valor *</p>
            <input type="text" name="T008_valor"         class="validate[required] text-input"      value="<?php echo $vls['ValorConta']?>"/>
        </div>
        
        <div class="clear"></div>

        <div class="grid_2">
            <input type="submit" value="Alterar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        <?php }?>
    </div>        
</form>