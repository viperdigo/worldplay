<?php
//Instancia Class
$obj    =   new models_T0011();

$hoje   =   date("d/m/Y H:i:s");

if(!empty($_POST))
{       
    
    $tabela =   "t010_pedido";
    
    $codigoCliente  =   $_POST['T002_codigo'];
        
    $campos =   array( "T002_codigo"    => $codigoCliente
                     , "T010_data"      => $hoje
                     , "T010_valor"     => $_POST['T010_valor']
                     , "T010_data"     => $_POST['T010_data']
    );
        
    $cps  =   $obj->retiraMascaraArray($campos,$tabela);
        
    $valorTotal =   $cps['T010_valor'];
    
    $iPedido =   $obj->inserir($tabela, $cps);
    
    if ($iPedido)
    {
        $codigoPedido   =  $obj->lastInsertId();
        
        #Busca Nome Cliente
        $dadosCliente   =   $obj->retornaDadosCliente($codigoCliente);
        foreach($dadosCliente as $cp => $vl)
        {
            $strCliente     =   $obj->retornaFormatoCodigoNome($vl['CodigoCliente'],$vl['NomeCliente']);
        }
                
        $tabela =   "t011_pedido_detalhe";
        
        $qtdeCampos =   count($_POST['T011_qtde']);
               
        for($i=0;$i<=$qtdeCampos;$i++)
        {
            if ((!empty($_POST['T003_codigo'][$i])) && (!empty($_POST['T011_qtde'][$i])) && ($_POST['T011_qtde'][$i]!=0))
            {
                $arrProduto = explode("-", $_POST['T003_codigo'][$i]);
                $codigoProduto  =   (int)$arrProduto[0];
                $qtdeProduto    =   $_POST['T011_qtde']        [$i];
                $subtotalProduto=   $_POST['T011_subtotal']    [$i];
                
                if($_POST['T011_valor_venda'] [$i]!="R$ 0,00")
                    $vlrVenda   =   $_POST['T011_valor_venda'][$i];
                else
                    $vlrVenda   =   $_POST['T003_vl_unit'][$i];
                             
                $campos =      array( "T010_pedido"         =>  $codigoPedido
                                    , "T002_codigo"         =>  $codigoCliente
                                    , "T003_codigo"         =>  $codigoProduto
                                    , "T011_valor_venda"    =>  $vlrVenda
                                    , "T011_qtde"           =>  $qtdeProduto
                                    , "T011_subtotal"       =>  $subtotalProduto
                                );  
                                                
                $cps  =   $obj->retiraMascaraArray($campos,$tabela);

                $obj->inserir($tabela, $cps);
                
                $obj->retornaFormatoCodigo($i);
                
                //Busca Descrição Produto
                $dadosProduto   =   $obj->retornaDadosProduto($codigoProduto);
                foreach($dadosProduto as $cp => $vl)
                {
                    $descricaoProduto   =   $vl['DescricaoProduto'];
                }
                                
                //Debita Estoque
                $tipoProduto    =  $obj->retornaTipoProduto($codigoProduto);
                //Se Tipo 1 - Produto Normal
                if($tipoProduto==1)
                {
                    
                    $obj->debitaEstoque($codigoProduto, $qtdeProduto);
                    
                }
                else if($tipoProduto==2) //Se Tipo 2 - Produto Receita
                {
                    
                    $componentesProduto =   $obj->retornaComponentes($codigoProduto);
                    
                    foreach($componentesProduto as $campos => $valores)
                    {
                        $codigoComponente   =   $valores['CodigoComponente'];
                        $qtdeTotal          =   $qtdeProduto * $valores['QtdeComponente'];                        
                        $obj->debitaEstoque($codigoComponente, $qtdeTotal);
                        
                    }  
                    
                }
                #Fim Debito Estoque
                                                
            }                                                           
        }  


        //Inicio Debita Cliente
        $valorTotal = $obj->formataValor($tabela, "T002_saldo", $valorTotal);
        $obj->debitaContaCliente($codigoCliente, $valorTotal);                                
        //Fim Debita Cliente
                
//        header("Location: ".ROUTER."home");
//        exit;
            
    }
    
    //Imprime comprovante
    
    
    $dadosCabec =   $obj->retornaDadosPedidoImpressao($codigoPedido);

    foreach($dadosCabec as $campos  =>  $valores)
    {
        $cliente    =   $obj->retornaFormatoCodigoNome($valores['CodigoCliente'],$valores['NomeCliente']);
        $dataPedido =   $obj->formataDataHoraView($valores['DataPedido']);
    }
    
    imprimi($obj,  $codigoPedido, $cliente, $dataPedido);
    imprimi($obj,  $codigoPedido, $cliente, $dataPedido);               
}

function imprimi($obj, $codigoPedido, $cliente, $dataPedido)
{
    $obj->enviaImpressora(0, "PEDIDO Nro.:$codigoPedido", 1);
    $obj->enviaImpressora(0, "CLIENTE    :$cliente", 1);
    $obj->enviaImpressora(0, "DATA       :".$dataPedido, 1);
    $obj->enviaImpressora(0, "", 2);

    $obj->enviaImpressora(0, "Nro Descricao               Qtd VlUnit Subtotal", 2);

    $dadosDet =   $obj->retornaDadosPedidoImpressao($codigoPedido);

    $i  =   0;
    foreach($dadosDet as $campos  =>  $valores)
    {
        $tabela =   "t011_pedido_detalhe";

		$valorTotal         =   0;
        $descricaoProduto   =   $valores['DescricaoProduto'];
        $qtdeProduto        =   $valores['QtdeProduto']     ;
        $vlrVenda           =   $valores['VlVendaProduto']  ;
        $subtotalProduto    =   $valores['SubtotalProduto'] ;

        $obj->enviaImpressora(  0
                              ,  $obj->preencheBranco($obj->retornaFormatoCodigo($i+1),3,0)                                     //Campo ItemNro
                                ." ".$obj->preencheBranco($obj->semCaracterEspecial($descricaoProduto), 24, 1)                                                 //Campo Descrição
                                .$obj->preencheBranco($obj->retornaFormatoCodigo($qtdeProduto), 3, 0)                           //Campo Qtde Produto
                                .$obj->preencheBranco(number_format($vlrVenda, 2, '.', ''),7,0)           //Campo Valor Unitário
                                .$obj->preencheBranco(number_format($subtotalProduto, 2, '.', ''),9,0)    //Campo SubTotal
                              , 1);

        $i++;
        $valorTotal += $subtotalProduto;

    }

    $valorTotal = $obj->formataValor($tabela, "T011_valor_venda", $valorTotal);

    $obj->enviaImpressora(0, "", 3);
    $obj->enviaImpressora(0, $obj->preencheBranco("TOTAL GERAL", 36, 0)." $valorTotal", 6);
    $obj->enviaImpressora(2);
}

?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar" id="formPedido">    
    <div class="conteudo_16">


        <div class="grid_2">
            <p>Data *</p>
            <input type="text" name="T010_data"     class="validate[required] text-input data"   value="<?php echo date("d/m/Y");?>"                           />
        </div>

        <div class="grid_4">
            <p>Cliente *</p>
            <?php echo $obj->inputCliente($codigoCliente,1);?>
        </div>

        
<!--        <div class="grid_2">
            <p>Data*</p>
            <input type="text" name="T010_data"    class="validate[required] data"   value="<?php echo $hoje;?>"         />                  
        </div>-->
                
        <div class="prefix_7 grid_2">
            <p>Total</p>
            <p class="labelText" id="lblTotal"></p>
            <input type="hidden" 
                   name="T010_valor"    
                   class="valor campoTotal"                                   
            />                  
        </div>        
        
        <div class="clear"></div>        
        
        <div class="grid_6">
            <p>Produto</p>
        </div>
        
        <div class="grid_2">
            <p>Vlr Unit.</p>
        </div>         
        
        <div class="grid_2">
            <p>Vlr Venda</p>
        </div>            
        
        <div class="grid_1">
            <p>Qtd</p>
        </div>            
        
        <div class="grid_2">
            <p>Subtotal</p>
        </div>
        
        <div class="clear"></div>   
                
        <div id="linhaPrincipal">
        
            <div class="linhaAdd">

                <div class="grid_6">
                    <input type="text" 
                           name="T003_codigo[]"         
                           class="campoProdutos validate[required]"                       
                    />                  
                </div>

                <div class="grid_2">
                    <p class="labelText lblVlrUnit"></p>
                    <input type="hidden" 
                           name="T003_vl_unit[]"        
                           class="valor campoVlrUnit"
                    />                  
                </div>            

                <div class="grid_2">
                    <input type="text" 
                           name="T011_valor_venda[]"    
                           class="valor campoVlrVenda validate[required]"                 
                    />                  
                </div>             

                <div class="grid_1">
                    <input type="text" 
                           name="T011_qtde[]"           
                           class=" valor campoQtde validate[required]"                    
                    />                  
                </div>             

                <div class="grid_2">
                    <p class="labelText lblSubtotal"></p>
                    <input type="hidden" 
                           name="T011_subtotal[]"    
                           class="valor campoSubtotal"    
                    />                  
                </div>
                
<!--                <div class="grid_1">
                    <ul class="ui-widget ui-helper-clearfix" id="icons">
                        <li title="Excluir Linha" 
                            class="ui-state-default ui-corner-all excluirLinha">
                            <a href="#">
                                <span   class='ui-icon ui-icon-cancel'></span>
                            </a>
                        </li>
                    </ul>                    
                </div>-->

                <div class="clear"></div>

            </div> 
            
        </div>    
                
        <div class="prefix_11 grid_2">
            <p>Total</p>
            <p class="labelText" id="lblTotal2"></p>
            <input type="hidden"                               
                   class="valor campoTotal"                                   
                />                  
        </div>        
        
        <div class="clear"></div>

        <div class="grid_2">
            <input type="submit" 
                   value="Gravar" 
                   id="botaoGravar" 
                   class="ui-button ui-widget ui-state-default ui-corner-all" 
                   role="button" 
                   aria-disabled="false"
            />
        </div>
        
    </div>        
</form>
