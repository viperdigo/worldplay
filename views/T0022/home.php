<?php
#   MANUTENÇÃO

//Instancia 
$obj        =   new models_T0022()  ;

//Filtros
if (!empty($_POST))
{
    $codigoTroca        =   $_POST['T014_codigo']       ;
    $codigoCliente      =   $_POST['T002_codigo']       ;                  
    $statusTroca        =   $_POST['T014_status']       ;
    
    $dataInicio         =   $_POST['T014_data']         ;
    $dataFim            =   $_POST['T014_dataF']        ;
    
    $limit              =   $_POST['QtdeRegistros']     ;
        
    $dados          =   $obj->retornaDados($codigoTroca,$codigoCliente,$dataInicio, $dataFim, $statusTroca, $limit);
    
}else 
{
    //Sem Filtro
    $dados          =   $obj->retornaDados();
}
//Variavel para verificar se existem dados de retorno para tabela não aparecer
$QtdeDados  =   0                   ;
?>
<div id="barra-botoes">
    <a href="#"                         class="ui-state-default ui-corner-all botoes-links abrirFiltros">  <span class="ui-icon ui-icon-carat-2-n-s">  </span>Filtros</a>
    <a href="<?php echo ROUTER;?>novo"  class="ui-state-default ui-corner-all botoes-links">                <span class="ui-icon ui-icon-plus">         </span>Novo</a>
    <hr>    
</div>    
<div id="barra-filtros">
    <form action="" method="post">        
        <div class="conteudo_16">
            <!-- CAMPOS DA BARRA DE FILTROS -->

            <div class="grid_1">
                <label>Código</label>
                <input type="text" name="T014_codigo"    value="<?php echo $codigoTroca;?>"                       />                  
            </div>

            <div class="grid_6">
                <label>Cliente</label>
                <?php echo $obj->inputCliente($codigoCliente);?>
            </div>
            
            <div class="grid_2">
                <label>Status</label>
                <select name="T014_status">
                    <option value="" ></option>
                    <option value="0">000-PENDENTE</option>
                    <option value="1">001-GARANTIA</option>
                    <option value="2">002-TROCA</option>
                    <option value="3">003-CANCELADO</option>
                </select>                 
            </div>
            
            <div class="grid_2">
                <label>Data Início</label>
                <input type="text" name="T014_data"       class="data"  value="<?php echo $dataInicio;?>"   />                  
            </div>

            <div class="grid_2">
                <label>Data Fim</label>
                <input type="text" name="T014_dataF"       class="data"  value="<?php echo $dataFim;?>"   />                  
            </div>
            
            <div class="grid_1">
                <label>Registros</label>
                <?php echo $obj->comboQtdeRegistros($limit);?>
            </div>            
            
            <div class="grid_2">
                <input type="submit" value="Filtrar" class="ui-button ui-widget ui-state-default ui-corner-all botao" role="button" aria-disabled="false">                
            </div>

        </div>        
    </form>
</div>  
<div class="conteudo_16">
    <table class="tablesorter" cellspacing="1" cellpadding="0" border="0"> 
        <thead> 
            <tr> 
                <th>Troca               </th> 
                <th>Cliente             </th> 
                <th>Data                </th> 
                <th>Status              </th> 
                <th>Ações               </th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php   foreach($dados    as $campos => $valores)
                    {   $QtdeDados++;
                        $arrStatus =   explode("-",$valores['StatusTroca']);
                        $status    =   (int)$arrStatus[0];
                    #criar parametro de data de vencimento para colorir o campo
                    
                    ?>
                        <tr> 
                            <td class="id" style="height: 30px;"><?php echo $obj->retornaFormatoCodigo($valores['CodigoTroca']);?> </td> 
                            <td><?php echo $obj->retornaFormatoCodigoNome($valores['CodigoCliente'],$valores['NomeCliente']);?></td> 
                            <td><?php echo $obj->formataDataView($valores['DataTroca']);?></td>                             
                            <td class="campoStatus"><?php echo $valores['StatusTroca'];?></td> 
                            <td class="acoes">                                
                                <?php if($status==0){?>
                                <ul class="ui-widget ui-helper-clearfix" id="icons">                                    
                                    <li title="Garantia"    class="ui-state-default ui-corner-all"><a class="btnGarantia"    href="#"><span   class='ui-icon ui-icon-star'></span></a></li>
                                    <!--<li title="Troca"       class="ui-state-default ui-corner-all"><a class="btnTroca"    href="#"><span   class='ui-icon ui-icon-transfer-e-w'></span></a></li>-->
                                    <li title="Cancelar"    class="ui-state-default ui-corner-all"><a class="btnCancelar"   href="#"><span   class='ui-icon ui-icon-cancel'></span></a></li>                                    
                                </ul>
                                <?php }?>                                
                            </td> 
                        </tr>
            <?php   }  ?>
                    <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->    
            <?php if ($QtdeDados==0){    ?>        
                    <tr>
                        <td colspan="5" style="text-align: center">Não Existem Dados.</td>
                    </tr>    
            <?php }?>
        </tbody> 
    </table> 
</div>   
