<?php
//Instancia Class
$obj    =   new models_T0022();

if(!empty($_POST))
{
    $tabela =   "t014_troca";
    
    $data    =   date("d/m/Y h:i:s");
    
    $codigoCliente      =   $_POST['T002_codigo'];
        
    $campos =   array( "T002_codigo"    => $codigoCliente
                     , "T014_data"      => $data
                     , "T014_status"    => 0); //Status 0 = Pendente
    
    $iTroca    =   $obj->inserir($tabela, $campos);
    
    if ($iTroca)
    {        
        $codigoTroca   =  $obj->lastInsertId();
        
        $tabela =   "t018_troca_detalhe";
        
        $qtdeCampos =   count($_POST['T018_qtde']);
               
        for($i=0;$i<$qtdeCampos;$i++)
        {
            if (!empty($_POST['T003_codigo'][$i]))
            {
                $arrProduto = explode("-", $_POST['T003_codigo'][$i]);
                $codigoProduto  =   (int)$arrProduto[0];
                $qtdeProduto    =   $_POST['T018_qtde'][$i];  
                
                $obsTroca       =   $_POST['T018_observacao'][$i];  
                
                //Produto Troca
//                $arrProdutoTroca = explode("-", $_POST['T003_codigo_troca'][$i]);
//                $codigoProdutoTroca  =   (int)$arrProdutoTroca[0];
//                $qtdeProdutoTroca    =   $_POST['T018_qtde'][$i];  
                
                $campos =      array( "T014_codigo"         =>  $codigoTroca
                                    , "T003_codigo"         =>  $codigoProduto
                                    , "T003_codigo_troca"   =>  $codigoProduto
                                    , "T018_qtde"           =>  $qtdeProduto
                                    , "T018_qtde_troca"     =>  $qtdeProduto
                                    , "T018_observacao"     =>  $obsTroca
                                );  

                $obj->inserir($tabela, $campos);                                    
            }                                                           
        }
        
        header("Location: ".ROUTER."home");
        exit;        
        
        
    }
                   
} 
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">

        <div class="grid_3">
            <p>Cliente *</p>
            <?php echo $obj->inputCliente("",1);?>
        </div>
        
        <div class="clear"></div>
        
        <div class="grid_7">
            <p>Produto Troca*</p>
        </div>
        
        <div class="grid_2">
            <p>Quantidade</p>
        </div>
        
<!--        <div class="grid_7">
            <p>Produto a ser Trocado *</p>
        </div>
        
        <div class="grid_2">
            <p>Quantidade</p>
        </div>-->
        
        <div class="grid_6">
            <p>Observação</p>
        </div>
        
        <div class="clear"></div>
        
        <div id="linhaPrincipal">
            
            <div class="linhaAdd">
        
                <div class="grid_7">
                    <input type="text" name="T003_codigo[]"       class="campoProdutos"   />                  
                </div>

                <div class="grid_2">
                    <input type="text" name="T018_qtde[]"                                 />                  
                </div>

                <div class="grid_6">
                    <input type="text" name="T018_observacao[]"   class="campoObservacao" />                  
                </div>

                <div class="clear"></div>
            
            </div>
        
        </div>

        <div class="grid_2">
            <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        
    </div>        
</form>