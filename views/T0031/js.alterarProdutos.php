<?php 
//Instacia Classe
$obj                =   new models_T0031();

$codigoPedido       =   $_REQUEST['codigoPedido'];

$produtosPedido     =   $obj->retornaProdutosPedido($codigoPedido);

$html   =   "";
$html   .=   "<form action='' method='post'>";

$html   .=   "<div class='conteudo_16'>";

$html   .=   "<div class='grid_1'>";
$html   .=   "<p>Código</p>";
$html   .=   "</div>";

$html   .=   "<div class='grid_4'>";
$html   .=   "<p>Produto</p>";
$html   .=   "</div>";

$html   .=   "<div class='grid_2'>";
$html   .=   "<p>Qtde.</p>";
$html   .=   "</div>";

$html   .=   "<div class='grid_2'>";
$html   .=   "<p>Vlr. Venda</p>";
$html   .=   "</div>";

$html   .=   "<div class='grid_2'>";
$html   .=   "<p>Vlr. Venda</p>";
$html   .=   "</div>";

$html   .=   "<div class='grid_2'>";
$html   .=   "<p>Subtotal</p>";
$html   .=   "</div>";

foreach($produtosPedido as $campos  =>  $valores)
{
    
    $html   .=  "<div class='linhaDialog'>";
    
    $html   .=  "<div class='grid_1'>";
    $html   .=  "<label class='produtoDialog'>".$valores['CodigoProduto']."</label>";
    $html   .=  "</div>";
    
    $html   .=  "<div class='grid_4'>";
    $html   .=  "<label>".$valores['DescricaoProduto']."</label>";
    $html   .=  "</div>";
    
    $html   .=  "<div class='grid_2'>";
    $html   .=  "<label class='qtdeDialog'>".$valores['QtdeProduto']."</label>";
    $html   .=  "</div>";
    
    $html   .=  "<div class='grid_2'>";
    $html   .=  "<label>".$obj->formataMoeda($valores['VlVendaProduto'])."</label>";
    $html   .=  "</div>";
    
    $html   .=  "<div class='grid_2'>";
    $html   .=  "<input type='text' name='T021_valor_venda' class='vlrVendaDialog'/>";
    $html   .=  "</div>";
    
    $html   .=  "<div class='grid_2'>";
    $html   .=  "<label class='subTotalDialog'>".$obj->formataMoeda($valores['SubtotalProduto'])."</label>";
    $html   .=  "</div>";        

    $html   .=  "<div class='clear'></div>";
    
    $html   .=  "</div>";    
    
}

$html   .=   "</div>";
$html   .=   "</form>";

echo $html;
?>