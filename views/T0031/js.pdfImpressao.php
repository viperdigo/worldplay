<?php
class PDF extends FPDF
{
    //Current column
    var $col=0;
    //Ordinate of column start
    var $y0;


    function Header()
    {        

    }
    
    function Footer()
    {        
        $this->SetFont('Arial','B',12);
        $this->SetXY(130, 280);
        $this->Cell(120, 15, utf8_decode("Página: ".$this->PageNo()), 0, 1, "C");                
    }

    function SetCol($col)
    {
        //Set position at a given column
        $this->col=$col;
        $x=10+$col*65;
        $this->SetLeftMargin($x);
        $this->SetX($x);
    }

    function AcceptPageBreak()
    {
        //Method accepting or not automatic page break
        if($this->col<2)
        {
            //Go to next column
            $this->SetCol($this->col+1);
            //Set ordinate to top
            $this->SetY($this->y0);
            //Keep on page
            return false;
        }
        else
        {
            //Go back to first column
            $this->SetCol(0);
            //Page break
            return true;
        }
    }

    function ChapterBody()
    {
        
        $obj    =   new models_T0031();
        
        $codigoPedido   =   $_REQUEST['codigoPedido'];
        
        $dadosPedido    =   $obj->retornaDadosPedidoImpressao($codigoPedido);
               
        #Detalhes
        
        $this->SetFont('Arial','B',8);
        
        $this->SetXY(0,2);
        $this->Cell(0, 0, utf8_decode("No"), 0, 0, "L");
        
        $this->SetXY(0.6,2);
        $this->Cell(0, 0, utf8_decode("Descrição"), 0, 0, "L");
        
        $this->SetXY(3.6,2);
        $this->Cell(0, 0, utf8_decode("Qt."), 0, 0, "L");
        
        $this->SetXY(4.3,2);
        $this->Cell(0, 0, utf8_decode("Vlr.Unit."), 0, 0, "L");
        
        $this->SetXY(6,2);
        $this->Cell(0, 0, utf8_decode("Subtotal"), 0, 0, "L");                                                                      
        
        $this->SetFont('Arial','',7);
        
        $i  =   1;
        $linha = 2.4;
        foreach($dadosPedido    as  $campos =>  $valores)
        {
            $codigoPedido   =   $valores['CodigoPedido'];
            $cliente        =   $obj->retornaFormatoCodigoNome($valores['CodigoCliente'], $valores['NomeCliente']);
            $data           =   $valores['DataPedido'];
            
            $this->SetXY(0,$linha);
            $this->Cell(0, 0, utf8_decode($i), 0, 0, "L");

            $this->SetXY(0.6,$linha);
            $this->Cell(0, 0, utf8_decode($valores['DescricaoProduto']), 0, 0, "L");

            $this->SetXY(3.6,$linha);
            $this->Cell(0, 0, utf8_decode($valores['QtdeProduto']), 0, 0, "L");

            $this->SetXY(4.3,$linha);
            $this->Cell(0, 0, utf8_decode($valores['VlVendaProduto']), 0, 0, "L");

            $this->SetXY(6,$linha);
            $this->Cell(0, 0, utf8_decode($valores['SubtotalProduto']), 0, 0, "L");                
            
            $linha = $linha + 0.4;
            $i++;
            
        }
        
        $this->SetFont('Arial','B',9);
        
        $this->SetXY(0,0.4);
        $this->Cell(0, 0, utf8_decode("Pedido: $codigoPedido"), 0, 0, "L");
        
        $this->SetXY(0,0.8);
        $this->Cell(0, 0, utf8_decode("Cliente: $cliente"), 0, 0, "L");
        
        $this->SetXY(0,1.2);
        $this->Cell(0, 0, utf8_decode("DATA: $data"), 0, 0, "L");        
       
        
    }

    function PrintChapter($num,$title,$file)
    {
        //Add chapter
        $this->AddPage();        
        $this->ChapterBody();

    }
}

$pdf      = new PDF("L","cm",array(20,7.8));
$pdf->SetTitle("Relatório Geral");
$pdf->PrintChapter(1,'Relatório Geral','');
$pdf->Output();

?>