<?php
//Instancia 
$obj = new models_T0028();

//Filtros
if (!empty($_POST)) {

    $tabela = "t003_produto";

    $i = 0;
    foreach ($_POST['T003_codigo'] as $value) {

        if (!empty($_POST['T003_qtd_estoque'][$i])) {

            $delimitador = "T003_codigo    =" . $value;

            $campos = array(
                'T003_qtd_estoque' => $_POST['T003_qtd_estoque'][$i] + $_POST['estoque_atual'][$i],
            );

            $alterar = $obj->atualizar($tabela, $campos, $delimitador);
        }
        $i++;
    }

}
//Sem Filtro
$dados = $obj->retornaDados();


//Variavel para verificar se existem dados de retorno para tabela não aparecer
$QtdeDados = 0;
?>

<div class="conteudo_16">
    <form action="" method="post">
        <table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
            <thead>
            <tr>
                <th>Código</th>
                <th>Descrição</th>
                <th>Estoque Atual</th>
                <th>Qtde</th>
            </tr>
            </thead>
            <tbody style="font-size: 20px;">
            <?php   foreach ($dados as $campos => $valores) {
                $QtdeDados++; ?>
                <tr style="width: 20px; height: 50px;">
                    <td class="id" style="width: 10px; text-align: center;"><input type="hidden" name="T003_codigo[]"
                                                                                   value="<?php echo $valores['CodigoProduto']; ?>"/><?php echo $obj->retornaFormatoCodigo($valores['CodigoProduto']); ?>
                    </td>
                    <td style="width: 400px;"><?php echo $valores['DescricaoProduto']; ?></td>
                    <td style="width: 100px; text-align: center;"><input type="hidden"
                                                                         value="<?php echo $valores['EstoqueProduto']; ?>"
                                                                         name="estoque_atual[]"/><?php echo $valores['EstoqueProduto']; ?>
                    </td>
                    <td style="width: 50px;"><input class="qtde" type="text" name="T003_qtd_estoque[]"
                                                    style="padding: 5px;font-size: 20px;width: 50px; height: 40px; margin: 10px;"/>
                    </td>
                </tr>
            <?php } ?>
            <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->
            <?php if ($QtdeDados == 0) { ?>
                <tr>
                    <td colspan="4" style="text-align: center">Não Existem Dados.</td>
                </tr>
            <?php } ?>
            </tbody>

        </table>

        <div class="clear"></div>

        <div class="grid_2">
            <input id="botao" type="submit" value="Gravar"/>
        </div>

    </form>
</div>   
