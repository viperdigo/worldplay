<?php
//Instancia Class
$obj    =   new models_T0012();

if(!empty($_POST))
{
    $tabela =   "t008_contas_pagar";
    
    $_POST  =   $obj->retiraMascaraArray($_POST,$tabela);
    
    $inseri =   $obj->inserir($tabela, $_POST);
    
    if ($inseri)
    {
        header("Location: ".ROUTER."home");
        exit;
    }
    
}  
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">

        <div class="grid_3">
            <p>Fornecedor*</p>
            <?php echo $obj->comboFornecedor("");?>
        </div>        
        
        <div class="grid_7">
            <p>Descrição*</p>
            <input type="text" name="T008_descricao"    class="validate[required] text-input"   />                  
        </div>
        
        <div class="grid_3">
            <p>Data Vencimento *</p>
            <input type="text" name="T008_data_venc"    class="validate[required] data"         />                  
        </div>
        
        <div class="grid_3">
            <p>Valor *</p>
            <input type="text" name="T008_valor"        class="validate[required] valor"        />                  
            <input type="hidden" name="T008_status"     value="1"        />                  
        </div>
        
        <div class="clear"></div>

        <div class="grid_2">
            <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        
    </div>        
</form>