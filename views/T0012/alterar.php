<?php
//Instancia Class
$obj            =   new models_T0012()                  ;

$codigoContaP  =   $_REQUEST['codigoConta']          ;

$user           =   $_SESSION['user'];

//Dados dos Campos
$dados          =   $obj->retornaDados($user, $codigoContaP)  ;

if(!empty($_POST))
{
    //Retira mascara
    $campos         =   $obj->retiraMascaraArray($_POST)        ;
    
    $tabela         =   "t008_contas_pagar"                     ;
    
    $delimitador    =   "T008_codigo    =".$codigoContaP        ;
    
    $alterar =   $obj->atualizar($tabela, $campos, $delimitador);
    if ($alterar)
    {
        header("Location: ".ROUTER."home");
        exit;
    }    
}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">
        <?php foreach($dados    as $campos  =>  $vls){?>
        <div class="grid_7">
            <p>Obervação *</p>
            <input type="text" name="T008_descricao"    class="validate[required] text-input"       value="<?php echo $vls['DescricaoConta']?>"/>
        </div>
        
        <div class="grid_3">
            <p>Data Vencimento *</p>
            <input type="text" name="T008_data_venc"     class="validate[required] text-input"      value="<?php echo $obj->formataDataView($vls['DataVencConta']);?>"/>
        </div>
        
        <div class="grid_3">
            <p>Valor *</p>
            <input type="text" name="T008_valor"         class="validate[required] valor"      value="<?php echo $vls['ValorConta']?>"/>
        </div>
        
        <div class="clear"></div>

        <div class="grid_2">
            <input type="submit" value="Alterar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        <?php }?>
    </div>        
</form>