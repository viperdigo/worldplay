<?php
# C O N T A S    A   P A G A R

//Instancia 
$obj        =   new models_T0012()  ;

$user       =   $_SESSION['user'];

//Filtros
if (!empty($_POST))
{
    $codigoFornecedor   =   $_POST['T004_codigo']       ;
    $codigoContaP       =   $_POST['T008_codigo']       ;                  
    $dataInicio         =   $_POST['T008_data_venc']    ;                  
    $dataFim            =   $_POST['T008_data_vencF']   ;
    $status             =   $_POST['T008_status']       ;
    
    $limit          =   $_POST['QtdeRegistros']         ;
    
    $dados          =   $obj->retornaDados($user,$codigoContaP,$codigoFornecedor, $dataInicio, $dataFim, $status, $limit);
}else 
{
    //Sem Filtro
    $dados          =   $obj->retornaDados($user);
}
//Variavel para verificar se existem dados de retorno para tabela não aparecer
$QtdeDados  =   0                   ;
?>
<div id="barra-botoes">
    <a href="#"                         class="ui-state-default ui-corner-all botoes-links abrirFiltros">   <span class="ui-icon ui-icon-carat-2-n-s">  </span>Filtros</a>
    <a href="<?php echo ROUTER;?>novo"  class="ui-state-default ui-corner-all botoes-links">                <span class="ui-icon ui-icon-plus">         </span>Novo</a>
    <hr>    
</div>    
<div id="barra-filtros">
    <form action="" method="post">        
        <div class="conteudo_16">
            <!-- CAMPOS DA BARRA DE FILTROS -->
                        
            <div class="grid_3">
                <label>Dinâmico</label>
                <input type="text"                      class="filtroDinamico"  />                  
            </div>

            <div class="grid_1">
                <label>Conta</label>
                <input type="text" name="T008_codigo"                           />                  
            </div>

            <div class="grid_3">
                <label>Fornecedor</label>
                <?php echo $obj->comboFornecedor("");?>
            </div>

            <div class="grid_2">
                <label>Data Início</label>
                <input type="text" name="T008_data_venc"       class="data"  value="<?php echo $dataInicio;?>"   />                  
            </div>

            <div class="grid_2">
                <label>Data Fim</label>
                <input type="text" name="T008_data_vencF"      class="data"  value="<?php echo $dataFim;?>"   />                  
            </div>
            
            <div class="grid_2">
                <label>Status</label>
                <select name="T008_status">
                    <option value="">...</option>
                    <option value="1" <?php echo $status=='1'?'selected':'';?>>001-PENDENTE</option>
                    <option value="2" <?php echo $status=='2'?'selected':'';?>>002-PAGO</option>
                    <option value="3" <?php echo $status=='3'?'selected':'';?>>003-CANCELADO</option>
                </select>
            </div>
            
            <div class="grid_1">
                <label>Registros</label>
                <?php echo $obj->comboQtdeRegistros($limit);?>
            </div>

            <div class="grid_2">
                <input type="submit" value="Filtrar" class="ui-button ui-widget ui-state-default ui-corner-all botao" role="button" aria-disabled="false">                
            </div>

        </div>        
    </form>
</div>  
<div class="conteudo_16">
    <table class="tablesorter" cellspacing="1" cellpadding="0" border="0"> 
        <thead> 
            <tr> 
                <th>Conta               </th> 
                <th>Fornecedor          </th> 
                <th>Data Vencimento     </th> 
                <th>Data Pagamento      </th> 
                <th>Valor               </th> 
                <th>Status              </th> 
                <th>Observação          </th> 
                <th>Ações               </th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php   foreach($dados    as $campos => $valores)
                    {   $QtdeDados++;
                    
                    #criar parametro de data de vencimento para colorir o campo
                    
                    ?>
                        <tr> 
                            <td class="id" style="height: 30px;"><?php echo $obj->retornaFormatoCodigo($valores['CodigoConta']);?> </td> 
                            <td><?php echo $obj->retornaFormatoCodigoNome($valores['CodigoFornecedor'],$valores['NomeFornecedor']);?></td> 
                            <td><?php echo $obj->formataDataView($valores['DataVencConta']);?></td>                             
                            <td class="campoDtPagto"><?php echo $obj->formataDataView($valores['DataPgtoConta']);?></td>                             
                            <td><?php echo $valores['ValorConta'];?></td> 
                            <td class="campoStatus"><?php echo $valores['StatusConta'];?></td> 
                            <td><?php echo $valores['DescricaoConta'];?></td> 
                            <td class="acoes" width="14%">
                                <?php if($valores['StatusConta']==1){?>
                                <ul class="ui-widget ui-helper-clearfix" id="icons">
                                    <!--<li title="Alterar" class="ui-state-default ui-corner-all"><a href="<?php echo ROUTER?>alterar&codigoConta=<?php echo $valores['CodigoConta'];?>"><span   class='ui-icon ui-icon-pencil'></span></a></li>-->
                                    <!--<li title="Excluir" class="ui-state-default ui-corner-all excluir"><a  href="#"><span class="ui-icon ui-icon-closethick"></span></li>-->
                                    <li title="Aprovar" class="ui-state-default ui-corner-all"><a class="btnAprovar" href="#"><span class="ui-icon ui-icon-check"></span></li>
                                    <li title="Cancelar" class="ui-state-default ui-corner-all"><a class="btnCancelar" href="#"><span class="ui-icon ui-icon-cancel"></span></li>
                                </ul>
                                <?php }?>
                            </td> 
                        </tr>
            <?php   }  ?>
                    <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->    
            <?php if ($QtdeDados==0){    ?>        
                    <tr>
                        <td colspan="8" style="text-align: center">Não Existem Dados.</td>
                    </tr>    
            <?php }?>
        </tbody> 
    </table> 
</div>   
