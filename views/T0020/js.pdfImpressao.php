<?php
class PDF extends FPDF
{
    //Current column
    var $col=0;
    //Ordinate of column start
    var $y0;

    function select()
    {
        $codigoProduto        =   $_GET['codigoProduto'];

        $obj    =   new models_T0020();

        $dados  =   $obj->retornaDados($codigoProduto);

        return $dados;

    }

    function Header()
    {        
        
        $this->SetFont('Arial','B',20);

        $this->Cell(190, 15, utf8_decode("ESTOQUE PRODUTOS"), 0, 1, "C");

    }
    
    function Footer()
    {        
        $this->SetFont('Arial','B',12);
        $this->SetXY(130, 280);
        $this->Cell(120, 15, utf8_decode("Página: ".$this->PageNo()), 0, 1, "C");                
    }

    function SetCol($col)
    {
        //Set position at a given column
        $this->col=$col;
        $x=10+$col*65;
        $this->SetLeftMargin($x);
        $this->SetX($x);
    }

    function AcceptPageBreak()
    {
        //Method accepting or not automatic page break
        if($this->col<2)
        {
            //Go to next column
            $this->SetCol($this->col+1);
            //Set ordinate to top
            $this->SetY($this->y0);
            //Keep on page
            return false;
        }
        else
        {
            //Go back to first column
            $this->SetCol(0);
            //Page break
            return true;
        }
    }

    function ChapterBody()
    {           
        
        $obj    =   new models_T0020();
        
        $this->SetFont('Arial','B',10);

        $this->Cell(70, 7, utf8_decode("Produto"), 1, 0, "L");
        $this->Cell(30, 7, utf8_decode("Estoque Atual"), 1, 0, "L");
        $this->Cell(30, 7, utf8_decode("Trocas"), 1, 0, "L");
        $this->Cell(30, 7, utf8_decode("Devoluções"), 1, 0, "L");
        $this->Cell(30, 7, utf8_decode("Esperado"), 1, 1, "L");
        
        $this->SetFont('Arial','',10);
        
        $dados  =   $this->select();
        
        $i  =   1;
        foreach($dados as $campos=>$valores)
        {
            
            if($i==32)
                $this->AddPage ();                    
            
            $this->Cell(70, 7, utf8_decode($obj->retornaFormatoCodigoNome($valores['CodigoProduto'],$valores['DescricaoProduto'])), 1, 0, "L");
            $this->Cell(30, 7, utf8_decode($valores['EstoqueAtual']), 1, 0, "L");
            $this->Cell(30, 7, utf8_decode($valores['Troca']), 1, 0, "L");
            $this->Cell(30, 7, utf8_decode($valores['Devolucao']), 1, 0, "L");
            $this->Cell(30, 7, utf8_decode($valores['EstoqueAtual']-$valores['Troca']+$valores['Devolucao']), 1, 1, "L");
            
        }
        
        $this->SetFont('Arial','B',12);
        
    }

    function PrintChapter($num,$title,$file)
    {
        //Add chapter
        $this->AddPage();        
        $this->ChapterBody();

    }
}

$pdf      = new PDF();
$pdf->SetTitle("ESTOQUE PRODUTOS");
$pdf->PrintChapter(1,'ESTOQUE PRODUTOS','');
$pdf->Output();

?>