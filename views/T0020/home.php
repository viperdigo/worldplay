<?php 

$obj    =   new models_T0020(); 

if (!empty($_POST))
{
    $strProduto     =   $_POST['T003_codigo'];
    
    $arrProduto     =   explode('-',$strProduto);
    $codigoProduto  =   $arrProduto[0];
    
    $dadosRelatorio =   $obj->retornaDados($codigoProduto);   
}
else
    $dadosRelatorio =   $obj->retornaDados();  

$cabecImpressao     =   "&codigoProduto=$codigoProduto";

?>
<div id="barra-botoes">
    <a href="#"                                                     class="ui-state-default ui-corner-all botoes-links abrirFiltros">   <span class="ui-icon ui-icon-carat-2-n-s">  </span>Filtros</a>
    <a href="<?php echo ROUTER;?>js.pdfImpressao<?php echo $cabecImpressao;?>" target="_blank"   class="ui-state-default ui-corner-all botoes-links">                <span class="ui-icon ui-icon-print">        </span>Imprimir</a>
    <hr>    
</div>    
<div id="barra-filtros">
    <form action="" method="post" class="validar">        
        <div class="conteudo_16">
            <!-- CAMPOS DA BARRA DE FILTROS -->           
            
            <div class="grid_5">
                <label>Produto</label>
                <input type="text" name="T003_codigo"       class="campoProdutos"   value="<?php echo $strProduto;?>"  />                  
            </div>

            <div class="grid_2">
                <input type="submit" value="Filtrar" class="ui-button ui-widget ui-state-default ui-corner-all botao" role="button" aria-disabled="false">                
            </div>

        </div>        
    </form>
</div>
<div class="conteudo_16">
    <table class="tablesorter" cellspacing="1" cellpadding="0" border="0"> 
        <thead> 
            <tr> 
                <th>Produto         </th> 
                <th>Estoque Atual   </th> 
                <th>Troca           </th> 
                <th>Devolução       </th> 
                <th>Estoque Esperado</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php   foreach($dadosRelatorio    as $campos => $valores)
                    {   $QtdeDados++;
                        $estoqueEsperado    =   $valores['EstoqueAtual'] - $valores['Troca'] + $valores['Devolucao'];
                    ?>
                        <tr> 
                            <td><?php echo $obj->retornaFormatoCodigoNome($valores['CodigoProduto'],$valores['DescricaoProduto']);?> </td> 
                            <td><?php echo $valores['EstoqueAtual'];?>  </td>
                            <td><?php echo $valores['Troca'];?>         </td>
                            <td><?php echo $valores['Devolucao'];?>     </td>
                            <td><?php echo $estoqueEsperado;?>          </td>
                        </tr>
            <?php   }  ?>
                    <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->    
            <?php if ($QtdeDados==0){    ?>        
                    <tr>
                        <td colspan="5" style="text-align: center">Não Existem Dados.</td>
                    </tr>    
            <?php }?>                           
        </tbody> 
    </table> 
</div>   