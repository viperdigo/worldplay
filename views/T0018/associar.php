<?php

$obj    =   new models_T0018();

$user   =   $_REQUEST['usuario'];


$dadosUser  =   $obj->retornaDadosUsuario($user); 
foreach($dadosUser as $cp => $vl)
{
    $login  =   $vl['LoginUsuario'];
    $nome   =   $vl['NomeUsuario'];
}

if (!empty($_POST))
{
    $tabela =   "t000_t001";
    
    $codigoEstrutura   =   $_POST['t000_codigo'];
    
    $campos =   array( "t000_codigo"    => $codigoEstrutura
                     , "t001_login"     => $login
    );   
    
    $obj->inserir($tabela, $campos);
    
} 

$dados      =   $obj->retornaAssociados($user);  

?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div> 
<form action="" method="post">
    <input type="hidden" value="<?php echo $login;?>" id="id"/>
    <div id="conteudo_16">

        <div class="clear10"></div>

        <div class="grid_2">
            <h2>Associar Menu ao Usuário: <?php echo "$nome ($login)";?></h2>
        </div>
        
        <div class="clear10"></div>
        
        <hr>
        
        <div class="clear10"></div>
        
        <div class="grid_2">
            <label>Selecione o menu:</label>  
            <?php echo $obj->comboEstrutura("");?>
        </div>
        
        <div class="grid_2">
            <input type="submit" value="Associar" class="ui-button ui-widget ui-state-default ui-corner-all botao" role="button" aria-disabled="false">                
        </div>
        
        <div class="clear10"></div>

        <table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
            <thead>              
            <tr>
                <th>Código Menu</th>
                <th>Nome Menu</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php   foreach($dados    as $campos => $valores)
                    {   $QtdeDados++;                                                           
                    ?>
                        <tr> 
                            <td class="id"><?php echo $obj->retornaFormatoCodigo($valores['CodigoEstrutura'],$valores['NomeEstrutura']);?> </td>                             
                            <td><?php echo $valores['NomeEstrutura'];?> </td>                             
                            <td class="acoes" width="5%">
                                <ul class="ui-widget ui-helper-clearfix alterarPedido" id="icons">
                                    <li title="Excluir" class="ui-state-default ui-corner-all excluirAssociar"><a href="#"><span class="ui-icon ui-icon-closethick"></span></li>
                                </ul>
                            </td>                             
                        </tr>
            <?php   }  ?>
                    <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->    
            <?php if ($QtdeDados==0){    ?>        
                    <tr>
                        <td colspan="6" style="text-align: center">Não Existem Dados.</td>
                    </tr>    
            <?php }?>                
            </tbody>
        </table>

    </div>
</form>