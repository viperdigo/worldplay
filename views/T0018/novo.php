<?php
//Instancia Class
$obj = new models_T0018();

if (!empty($_POST)) {
    $tabela = "t001_usuario";

    $login = $_POST['t001_login'];
    $nome = $_POST['T001_nome'];
    $senha = md5($_POST['t001_senha']);

    $campos = array("t001_login" => $login
    , "T001_nome" => $nome
    , "t001_senha" => $senha);

    $inseri = $obj->inserir($tabela, $campos);

    if ($inseri) {
        header("Location: " . ROUTER . "home");
        exit;
    }

}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER ?>home" class="ui-state-default ui-corner-all botoes-links"><span
            class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>
</div>
<form action="" method="post" class="validar">
    <div class="conteudo_16">
        <div class="grid_2">
            <p>Login*</p>
            <input type="text" name="t001_login" class="validate[required] text-input"/>
        </div>

        <div class="grid_4">
            <p>Nome*</p>
            <input type="text" name="T001_nome" class="validate[required] text-input"/>
        </div>

        <div class="grid_2">
            <p>Senha *</p>
            <input type="password" name="t001_senha" class="validate[required] text-input" id="password"/>
        </div>

        <div class="grid_2">
            <p>Senha *</p>
            <input type="password" name="t001_senhaConfirm" class="validate[required,equals[password]] text-input"/>
        </div>

        <div class="clear"></div>

        <div class="grid_2">
            <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button"
                   aria-disabled="false">
        </div>

    </div>
</form>