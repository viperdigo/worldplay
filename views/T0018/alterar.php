<?php
//Instancia Class
$obj            =   new models_T0018()                  ;

$usuario  =   $_REQUEST['usuario']                      ;
    
//Dados dos Campos
$dados          =   $obj->retornaDados($usuario)        ;

if(!empty($_POST))
{
    //Retira mascara
    $login  =   $_POST['t001_login'];
    $nome   =   $_POST['T001_nome'];
    $senha  =   md5($_POST['t001_senha']);
    
    $campos =   array( "t001_login" => $login
                     , "T001_nome"  => $nome
                     , "t001_senha" => $senha);
    
    $tabela         =   "t001_usuario"                          ;
    
    $delimitador    =   "t001_login    ='".$usuario."'"         ;
    
    $alterar =   $obj->atualizar($tabela, $campos, $delimitador);
    if ($alterar)
    {
        header("Location: ".ROUTER."home");
        exit;
    }    
}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">
        <?php foreach($dados    as $campos  =>  $vls){?>
        <div class="grid_7">
            <p>Login *</p>
            <input type="text"                      name="t001_login"           class="validate[required] text-input"                       value="<?php echo $vls['LoginUsuario']?>" readonly/>
        </div>
        
        <div class="grid_3">
            <p>Nome *</p>
            <input type="text"                      name="T001_nome"            class="validate[required] text-input "                      value="<?php echo $vls['NomeUsuario']?>"/>
        </div>
        
        <div class="grid_3">
            <p>Senha *</p>
            <input type="password" id="password"    name="t001_senha"           class="validate[required] text-input"                       />
        </div>       
        
        <div class="grid_3">
            <p>Confirma Senha *</p>
            <input type="password"                  name="t001_senhaConfirm"    class="validate[required,equals[password]] text-input"      />
        </div>       
        
        <div class="clear"></div>

        <div class="grid_2">
            <input type="submit" value="Alterar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        <?php }?>
    </div>        
</form>