<?php
class PDF extends FPDF
{
    //Current column
    var $col=0;
    //Ordinate of column start
    var $y0;

    function select()
    {
        $obj    =   new models_T0019();

        $dados  =   $obj->retornaDados();

        return $dados;

    }

    function Header()
    {        
        
        $this->SetFont('Arial','B',20);

        $this->Cell(120, 15, utf8_decode("RELATÓRIO GERAL"), 0, 1, "C");

    }
    
    function Footer()
    {        
        $this->SetFont('Arial','B',12);
        $this->SetXY(130, 280);
        $this->Cell(120, 15, utf8_decode("Página: ".$this->PageNo()), 0, 1, "C");                
    }

    function SetCol($col)
    {
        //Set position at a given column
        $this->col=$col;
        $x=10+$col*65;
        $this->SetLeftMargin($x);
        $this->SetX($x);
    }

    function AcceptPageBreak()
    {
        //Method accepting or not automatic page break
        if($this->col<2)
        {
            //Go to next column
            $this->SetCol($this->col+1);
            //Set ordinate to top
            $this->SetY($this->y0);
            //Keep on page
            return false;
        }
        else
        {
            //Go back to first column
            $this->SetCol(0);
            //Page break
            return true;
        }
    }

    function ChapterBody()
    {           
        
        $obj    =   new models_T0019();
        
        $this->SetFont('Arial','B',10);

        $this->Cell(80, 7, utf8_decode("Cliente"), 1, 0, "L");
        $this->Cell(50, 7, utf8_decode("Saldo"), 1, 0, "L");
        $this->Cell(50, 7, utf8_decode("Último Pagamento"), 1, 1, "L");
        
        $this->SetFont('Arial','',10);
        
        $dados  =   $this->select();
        
        $i  =   1;
        foreach($dados as $campos=>$valores)
        {
            $totalGeral +=   $valores['SaldoCliente'];
            
            if($i%34==0)
                $this->AddPage ();                    
            
            $this->Cell(80, 7, utf8_decode($obj->retornaFormatoCodigoNome($valores['CodigoCliente'],$valores['NomeCliente'])), 1, 0, "L");
            $this->Cell(50, 7, utf8_decode($obj->formataMoeda($valores['SaldoCliente'])), 1, 0, "L");
            $this->Cell(50, 7, utf8_decode($obj->formataDataView($valores['UltimoPagamento'])), 1, 1, "L");
            
            $i++;
        }
        
        $this->SetFont('Arial','B',12);
        
        $this->Cell(130, 7, utf8_decode("Total Geral   "), 1, 0, "R");        
        $this->Cell(50, 7, utf8_decode($obj->formataMoeda($totalGeral)), 1, 1, "L");        
        
    }

    function PrintChapter($num,$title,$file)
    {
        //Add chapter
        $this->AddPage();        
        $this->ChapterBody();

    }
}

$pdf      = new PDF();
$pdf->SetTitle("Relatório Geral");
$pdf->PrintChapter(1,'Relatório Geral','');
$pdf->Output();

?>