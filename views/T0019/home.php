<?php 

$obj    =   new models_T0019(); 

$dadosRelatorio =   $obj->retornaDados();   

?>
<div id="barra-botoes">
    <a href="#"                                                     class="ui-state-default ui-corner-all botoes-links abrirFiltros">   <span class="ui-icon ui-icon-carat-2-n-s">  </span>Filtros</a>
    <a href="<?php echo ROUTER;?>js.pdfImpressao<?php echo $cabecImpressao;?>" target="_blank"   class="ui-state-default ui-corner-all botoes-links">                <span class="ui-icon ui-icon-print">        </span>Imprimir</a>
    <hr>    
</div>    
<div id="barra-filtros">
<!--    <form action="" method="post" class="validar">        
        <div class="conteudo_16">
             CAMPOS DA BARRA DE FILTROS            
            
            <div class="grid_2">
                <label>Data Início</label>
                <input type="text" name="T010_data"       class="data"   value="<?php echo $dataInicio;?>"  />                  
            </div>

            <div class="grid_2">
                <label>Data Fim</label>
                <input type="text" name="T010_dataF"       class="data"  value="<?php echo $dataFim;?>"   />                  
            </div>

            <div class="grid_2">
                <input type="submit" value="Filtrar" class="ui-button ui-widget ui-state-default ui-corner-all botao" role="button" aria-disabled="false">                
            </div>

        </div>        
    </form>-->
</div>
<div class="conteudo_16">
    <table class="tablesorter" cellspacing="1" cellpadding="0" border="0"> 
        <thead> 
            <tr> 
                <th>Cliente         </th> 
                <th>Saldo           </th> 
                <th>Último Pagto    </th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php   foreach($dadosRelatorio    as $campos => $valores)
                    {   $QtdeDados++;
                        $totalGeral +=  $valores['SaldoCliente'];
                    
                    if ($valores['SaldoCliente']>=0)
                        $corLetra   =   "#0000FF";
                    else
                        $corLetra   =   "#FF0000";
                    
                    ?>
                        <tr> 
                            <td><?php echo $obj->retornaFormatoCodigoNome($valores['CodigoCliente'],$valores['NomeCliente']);?> </td> 
                            <td><font color="<?php echo $corLetra;?>"><?php echo $obj->formataMoeda($valores['SaldoCliente']);?></td> 
                            <td><?php echo $obj->formataDataView($valores['UltimoPagamento']);?></td> 
                        </tr>
            <?php   }  ?>
                    <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->    
            <?php if ($QtdeDados==0){    ?>        
                    <tr>
                        <td colspan="6" style="text-align: center">Não Existem Dados.</td>
                    </tr>    
            <?php }else{?>
                    <tr>
                        <td colspan="2" style="text-align: right; font-weight: bold;">Total Geral &nbsp;&nbsp;&nbsp;</td>
                        <td style="text-align: left; font-weight: bold;"><?php echo $obj->formataMoeda($totalGeral);?></td>
                    </tr>                     
            <?php }?>        
        </tbody> 
    </table> 
</div>   