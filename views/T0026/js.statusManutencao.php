<?php 
//Instacia Classe
$obj                =   new models_T0026();

$codigoManutencao   =   $_REQUEST['codigoManutencao'];   

$dadosManutencao    =   $obj->retornaDados($codigoManutencao);

foreach($dadosManutencao as $campos => $valores)
{
    $historicoManutencao    =   $valores['HistoricoManutencao'] ;
    $statusManutencao       =   $valores['StatusManutencao']    ;
}

if ($statusManutencao == 1)
    $proxStatusManutencao  =    "002-AGUARDANDO MANUTENÇÃO";
if ($statusManutencao == 2)
    $proxStatusManutencao  =    "003-AGUARDANDO CLIENTE";
if ($statusManutencao == 3)
    $proxStatusManutencao  =    "004-ENTREGUE CLIENTE";

$retorno    =   array(  "Status"        =>  $statusManutencao
                      , "Historico"     =>  $historicoManutencao
                      , "Proximo"       =>  $proxStatusManutencao);

echo json_encode($retorno);

?>