<?php 
//Instacia Classe
$obj                =   new models_T0026();

$codigoManutencao   =   $_REQUEST['codigoManutencao'];   

$dadosManutencao    =   $obj->retornaDadosManutencaoImpressao($codigoManutencao);

foreach($dadosManutencao as $cp =>  $vl)
{
    $strCliente =   $obj->retornaFormatoCodigoNome($vl['CodigoCliente'], $vl['NomeCliente']);
    $strProduto =   $obj->retornaFormatoCodigoNome($vl['CodigoProduto'], $vl['DescricaoProduto']);
}

$dataRetirada   =   date("d/m/Y h:i:s");

$arrManutencao  =   array(  "MANUTENCAO"        =>  $codigoManutencao 
                         ,  "CLIENTE"           =>  $strCliente
                         ,  "DATA"              =>  $dataRetirada
                         ,  "ITEM"              =>  $strProduto
                         ,  "LINHA"             =>  "__________________________________"
                         ,  "ASSINATURA"        =>  "       Assinatura Cliente         "
                         );

echo json_encode($arrManutencao);
?>