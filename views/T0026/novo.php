<?php
//Instancia Class
$obj    =   new models_T0026();

if(!empty($_POST)) 
{
    $tabela =   "t016_manutencao";
    
    $dataEntrada    =   date("d/m/Y h:i:s");
    
    $codigoCliente      =   $_POST['T002_codigo'];
        
    $campos =   array( "T002_codigo"        => $codigoCliente
                     , "T016_dt_entrada"    => $dataEntrada
                     , "T016_status"        => 1); //Status 1 - Aguardando Envio
    
    $iManutencao    =   $obj->inserir($tabela, $campos);
    
    if ($iManutencao)
    {        
        $codigoManutencao   =  $obj->lastInsertId();
        
        $tabela =   "t017_manutencao_detalhe";
        
        $arrProduto = explode("-", $_POST['T003_codigo']);
        $codigoProduto  =   (int)$arrProduto[0];
        $qtdeProduto    =   $_POST['T017_qtde'];  
        $obsProduto     =   $_POST['T017_observacao'];  

        $campos =      array( "T016_codigo"         =>  $codigoManutencao
                            , "T003_codigo"         =>  $codigoProduto
                            , "T017_qtde"           =>  $qtdeProduto
                            , "T017_obs"            =>  $obsProduto
                        );  

        $obj->inserir($tabela, $campos);                                    
        
        $dadosCliente   =   $obj->retornaDadosCliente($codigoCliente);
        foreach($dadosCliente   as  $cp =>  $vl)
        {
            $strCliente =   $obj->retornaFormatoCodigoNome($vl['CodigoCliente'], $vl['NomeCliente']);
        }
        
        $string =   "Manutenção: $codigoManutencao  \nCliente: $strCliente \nData: $dataEntrada  \nObs.: $obsProduto";
        echo "<textarea name='textTextArea' id='txtAreaPrtLbl' rows='5' cols='40' style='display:none;'>$string</textarea>";
        $obj->enviaImpressora(3);
        
        $obj->enviaImpressora(0, "MANUTENCAO Nro.:".$codigoManutencao, 1);
        $obj->enviaImpressora(0, "CLIENTE    :".$strCliente, 1);
        $obj->enviaImpressora(0, "DATA       :".$dataEntrada, 1);
        $obj->enviaImpressora(0, "OBS.:       :".$obsProduto, 1);
        $obj->enviaImpressora(0, "", 2); 
        $obj->enviaImpressora(0, "APRESENTAR ESTE CUPOM PARA RETIRADA DOS PRODUTOS DE MANUTENÇÃO. OBS: SEM ESTE NÃO SERÁ ENTREGUE!", 1); 
        
        $obj->enviaImpressora(2);


    }
                   
} 
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">

        <div class="grid_3">
            <p>Cliente *</p>
            <?php echo $obj->inputCliente("",1);?>
        </div>
        
        <div class="clear"></div>
        
        <div class="grid_7">
            <p>Produto *</p>
        </div>
        
        <div class="grid_2">
            <p>Quantidade</p>
        </div>
        
        <div class="grid_6">
            <p>Observação</p>
        </div>
        
        <div class="clear"></div>
        
        <div id="linhaPrincipal">
            
            <div class="linhaAdd">
        
                <div class="grid_7">
                    <input type="text" name="T003_codigo"       class="campoProdutos"   />                  
                </div>

                <div class="grid_2">
                    <input type="text" name="T017_qtde"     value="1"  readonly         />                  
                </div>

                <div class="grid_6">
                    <input type="text" name="T017_observacao"   />                  
                </div>

                <div class="clear"></div>
            
            </div>
        
        </div>

        <div class="grid_2">
            <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>
        
    </div>        
</form>