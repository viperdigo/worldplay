<?php
#   MANUTENÇÃO

//Instancia 
$obj = new models_T0026();
$filter = false;
//Filtros
if (!empty($_POST)) {

    $filter = true;

    $codigoManutencao = $_POST['T016_codigo'];
    $codigoCliente = $_POST['T002_codigo'];
    $statusManutencao = $_POST['T016_status'];

    $dataEntInicio = $_POST['T016_dt_entrada'];
    $dataEntFim = $_POST['T016_dt_entradaF'];

    $dataRetInicio = $_POST['T016_dt_retirada'];
    $dataRetFim = $_POST['T016_dt_retiradaF'];

    $limit = $_POST['QtdeRegistros'];

    $dados = $obj->retornaDados(
        $codigoManutencao,
        $codigoCliente,
        $dataEntInicio,
        $dataEntFim,
        $dataRetInicio,
        $dataRetFim,
        $statusManutencao,
        $limit
    );

} else {
    //Sem Filtro
    $dados = $obj->retornaDados();
}
//Variavel para verificar se existem dados de retorno para tabela não aparecer
$QtdeDados = 0;
?>
<div id="barra-botoes">
    <a href="#" class="ui-state-default ui-corner-all botoes-links abrirFiltros"> <span
            class="ui-icon ui-icon-carat-2-n-s">  </span>Filtros</a>
    <a href="<?php echo ROUTER; ?>novo" class="ui-state-default ui-corner-all botoes-links"> <span
            class="ui-icon ui-icon-plus">         </span>Novo</a>
    <hr>
</div>
<div id="barra-filtros">
    <form action="" method="post">
        <div class="conteudo_16">
            <!-- CAMPOS DA BARRA DE FILTROS -->

            <div class="grid_1">
                <label>Código</label>
                <input type="text" name="T016_codigo" value="<?php echo $codigoManutencao; ?>"/>
            </div>

            <div class="grid_6">
                <label>Cliente</label>
                <?php echo $obj->inputCliente($codigoCliente); ?>
            </div>

            <div class="grid_4">
                <label>Status</label>
                <select name="T016_status">
                    <option value=""></option>
                    <option value="1" <?php echo $statusManutencao == 1 ? "selected" : ""; ?>>001-AGUARDANDO ENVIO
                    </option>
                    <option value="2" <?php echo $statusManutencao == 2 ? "selected" : ""; ?>>002-AGUARDANDO
                        MANUTENÇÃO
                    </option>
                    <option value="3" <?php echo $statusManutencao == 3 ? "selected" : ""; ?>>003-AGUARDANDO CLIENTE
                    </option>
                    <option value="4" <?php echo $statusManutencao == 4 ? "selected" : ""; ?>>004-ENTREGUE CLIENTE
                    </option>
                    <option value="5" <?php echo $statusManutencao == 5 ? "selected" : ""; ?>>005-CANCELADO</option>
                </select>
            </div>

            <div class="clear"></div>

            <div class="grid_2">
                <label>Entrada Início</label>
                <input type="text" name="T016_dt_entrada" class="data" value="<?php echo $dataEntInicio; ?>"/>
            </div>

            <div class="grid_2">
                <label>Entrada Fim</label>
                <input type="text" name="T016_dt_entradaF" class="data" value="<?php echo $dataEntFim; ?>"/>
            </div>

            <div class="grid_2">
                <label>Retirada Início</label>
                <input type="text" name="T016_dt_retirada" class="data" value="<?php echo $dataRetInicio; ?>"/>
            </div>

            <div class="grid_2">
                <label>Retirada Fim</label>
                <input type="text" name="T016_dt_retiradaF" class="data" value="<?php echo $dataRetFim; ?>"/>
            </div>

            <div class="grid_3">
                <label>Registros</label>
                <?php echo $obj->comboQtdeRegistros($limit); ?>
            </div>

            <div class="grid_2">
                <input type="submit" value="Filtrar" class="ui-button ui-widget ui-state-default ui-corner-all botao"
                       role="button" aria-disabled="false">
            </div>

            <?php

            if ($filter) { ?>
                <div class="grid_2">
                    <input type="submit" value="Imprimir"
                           class="ui-button ui-widget ui-state-default ui-corner-all botaoImprimir"
                           role="button" aria-di    sabled="false">
                </div>
            <?php } ?>

        </div>
    </form>
</div>
<div class="conteudo_16">
    <table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
        <thead>
        <tr>
            <th>Código</th>
            <th>Cliente</th>
            <th width="7%">Entrada Em:</th>
            <th width="7%">Enviado Forn. Em:</th>
            <th width="7%">Devolução Forn. Em:</th>
            <th width="7%">Entregue Cliente Em:</th>
            <th width="7%">Cancelado Em:</th>
            <th>Status</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($dados as $campos => $valores) {
            $QtdeDados++;
            $arrStatus = explode("-", $valores['StatusManutencao']);
            $status = (int)$arrStatus[0];
            #criar parametro de data de vencimento para colorir o campo

            ?>
            <tr>
                <td class="id" style="height: 30px;"><?php echo $obj->retornaFormatoCodigo(
                        $valores['CodigoManutencao']
                    ); ?> </td>
                <td><?php echo $obj->retornaFormatoCodigoNome(
                        $valores['CodigoCliente'],
                        $valores['NomeCliente']
                    ); ?></td>
                <td class="campoDtEntrada"><?php echo $obj->formataDataView($valores['DataEntManutencao']); ?></td>
                <td class="campoDtEnvio"><?php echo $obj->formataDataView($valores['DataEnvioManutencao']); ?></td>
                <td class="campoDtRec"><?php echo $obj->formataDataView($valores['DataRecManutencao']); ?></td>
                <td class="campoDtDev"><?php echo $obj->formataDataView($valores['DataDevManutencao']); ?></td>
                <td class="campoDtCanc"><?php echo $obj->formataDataView($valores['DataCancManutencao']); ?></td>
                <td class="campoStatus"><?php echo $valores['StatusManutencao']; ?></td>
                <td class="acoes">

                    <ul class="ui-widget ui-helper-clearfix" id="icons">
                        <li title="Visualizar Itens" class="ui-state-default ui-corner-all"><a class="btnItens"
                                                                                               href="#"> <span
                                    class='ui-icon ui-icon-note'>           </span></a></li>
                        <?php if (($status != 4) && ($status != 5)) { ?>
                            <li title="Alterar Status" class="ui-state-default ui-corner-all liAlterar"><a
                                    class="btnAlterarStatus" href="#"> <span
                                        class='ui-icon ui-icon-transfer-e-w'>   </span></a></li>
                            <li title="Cancelar" class="ui-state-default ui-corner-all liCancelar"><a
                                    class="btnCancelar" href="#"> <span class='ui-icon ui-icon-cancel'>         </span></a>
                            </li>
                        <?php } ?>
                    </ul>

                </td>
            </tr>
        <?php } ?>
        <!-- COLSPAN É A QUANTIDADE DE COLUNAS EXISTENTES NA TABELA -->
        <?php if ($QtdeDados == 0) { ?>
            <tr>
                <td colspan="9" style="text-align: center">Não Existem Dados.</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>   
