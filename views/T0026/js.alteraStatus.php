<?php 
//Instacia Classe
$obj                =   new models_T0026();

$data               =   date("d/m/Y h:i:s")                         ;
$codigoManutencao   =   $_REQUEST['codigoManutencao']               ;   
$historicoManutencao=   $_REQUEST['historicoManutencao']            ;
$arrStatus          =   explode("-",$_REQUEST['statusManutencao'])  ;

$statusManutencao   =   (int)$arrStatus[0];

$tabela             =   "t016_manutencao";

if($statusManutencao    ==  1)
{
    $proxStatus =   2;
    $campos =   array(  "T016_status"             => $proxStatus      //Aguardando Manutenção
                     ,  "T016_dt_envio"           => $data
                     ,  "T016_historico"          => $historicoManutencao
                     );
}else if($statusManutencao  ==  2)
{
    $proxStatus =   3;
    $campos =   array(  "T016_status"             => $proxStatus      //Aguardando Cliente
                     ,  "T016_dt_recebimento"     => $data
                     ,  "T016_historico"          => $historicoManutencao
                     );        
}else if($statusManutencao  ==  3)
{
    $proxStatus =   4;
    $campos =   array(  "T016_status"             => $proxStatus      //Entregue Cliente
                     ,  "T016_dt_devolucao"       => $data
                     ,  "T016_historico"          => $historicoManutencao
                     );        
}

$delim  =   " T016_codigo   =   $codigoManutencao";

$aprova =   $obj->atualizar($tabela, $campos, $delim);

if($aprova)
    echo $proxStatus;    
else
    echo 0;

?>