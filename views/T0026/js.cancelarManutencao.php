<?php 
//Instacia Classe
$obj                =   new models_T0026();

$codigoManutencao   =   $_REQUEST['codigoManutencao'];   
$dataCancelamento   =   date("d/m/Y h:i:s");

$tabela             =   "t016_manutencao";

$campos =   array( "T016_status"         => 5                   //Status 5 = Cancelado
                 , "T016_dt_cancelamento"=> $dataCancelamento
                 ); 

$delim  =   "      T016_codigo            =   $codigoManutencao";

$cancela =   $obj->atualizar($tabela, $campos, $delim);

if($cancela)
    echo 1;
else
    echo 0;

?>