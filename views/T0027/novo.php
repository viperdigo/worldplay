<?php
//Instancia Class
$obj = new models_T0027();

if (!empty($_POST)) {
    if ($_POST['T003_tipo'] == 3) {

        $tabela = "t003_produto";

        $_POST = $obj->retiraMascaraArray($_POST, $tabela);

        $inseri = $obj->inserir($tabela, $_POST);

        if ($inseri) {
            header("Location: " . ROUTER . "home");
            exit;
        }

    }
}
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER ?>home" class="ui-state-default ui-corner-all botoes-links"><span
            class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>
</div>
<form action="" method="post" class="validar">
    <div class="conteudo_16">

        <input name="T003_tipo" type="hidden" value="3"/>

        <div class="clear"></div>

        <!-- Itens Normais -->

        <div id="normal">

<!--            <div class="grid_3">-->
<!--                <p>Fornecedor</p>-->
<!--                --><?php //echo $obj->comboFornecedor("", 0); ?>
<!--            </div>-->

            <div class="grid_7">
                <p>Descrição*</p>
                <input type="text" name="T003_descricao" class="validate[required] text-input campoDescricao"/>
            </div>

            <div class="grid_2">
                <p>Qtde. Estoque</p>
                <input type="text" name="T003_qtd_estoque" class="text-input"/>
            </div>

<!--            <div class="grid_2">-->
<!--                <p>Valor Custo</p>-->
<!--                <input type="text" name="T003_vl_custo" class="text-input valor"/>-->
<!--            </div>-->
<!---->
<!--            <div class="grid_2">-->
<!--                <p>Valor Venda</p>-->
<!--                <input type="text" name="T003_vl_unit" class="text-input valor"/>-->
<!--            </div>-->

            <div class="clear"></div>

            <div class="grid_2">
                <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all"
                       role="button" aria-disabled="false">
            </div>

        </div>

    </div>
</form>