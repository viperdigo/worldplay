<?php
//Instancia Class
$obj            =   new models_T0027()                  ;

$codigoProduto  =   $_REQUEST['codigoProduto']          ;

//Dados dos Campos
$dados          =   $obj->retornaDados($codigoProduto)  ;

if(!empty($_POST))
{
    //Retira mascara
    $tabela         =   "t003_produto"                              ;
    
    $_POST          =   $obj->retiraMascaraArray($_POST, $tabela)   ;
    
    $delimitador    =   "T003_codigo    =".$codigoProduto           ;
    
    $alterar =   $obj->atualizar($tabela, $_POST, $delimitador)    ;
    if ($alterar)
    {
        header("Location: ".ROUTER."home");
        exit;
    }      
}
    
?>
<div id="barra-botoes">
    <a href="<?php echo ROUTER?>home" class="ui-state-default ui-corner-all botoes-links"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>Voltar</a>
    <hr>    
</div>
<form action="" method="post" class="validar">    
    <div class="conteudo_16">
        
        <?php foreach($dados    as  $campos =>  $valores){?>

<!--            <div class="grid_3">-->
<!--                <p>Fornecedor</p>-->
<!--                --><?php //echo $obj->comboFornecedor($valores['CodigoFornecedor'],0);?>
<!--            </div>        -->

            <div class="grid_7">
                <p>Descrição*</p>
                <input type="text" name="T003_descricao"    value="<?php echo $valores['DescricaoProduto'];?>"  class="validate[required] text-input"    />                  
            </div>

            <div class="grid_2">
                <p>Qtde. Estoque</p>
                <input type="text" name="T003_qtd_estoque"  value="<?php echo $valores['EstoqueProduto'];?>"    class="text-input"                                      />                  
            </div>
<!---->
<!--            <div class="grid_2">-->
<!--                <p>Valor Custo</p>-->
<!--                <input type="text" name="T003_vl_custo"     value="--><?php //echo $valores['VlCustoProduto'];?><!--"    class="text-input valor"                                />                  -->
<!--            </div>-->
<!---->
<!--            <div class="grid_2">-->
<!--                <p>Valor Venda</p>-->
<!--                <input type="text" name="T003_vl_unit"      value="--><?php //echo $valores['VlVendaProduto'];?><!--"    class="text-input valor"                                />                  -->
<!--            </div>-->

            <div class="clear"></div>

            <div class="grid_2">
                <input type="submit" value="Gravar" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
            </div> 
            
        <?php }?>

    </div>        
</form>